$(document).ready(base);

function base(){
  solicitud_pendiente();
  $.ajax({
        url:"categorias_divisas",
        type:'Post',
        data:{},
        success:function(respuesta){
          document.getElementById('sel_divisa_1').innerHTML='';
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');def.innerText='Selecciona divisa';document.getElementById('sel_divisa_1').append(def);;
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_categoria;
              opt.value=categorias[i].id_categoria;
              document.getElementById('sel_divisa_1').append(opt);
            }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
  //
  $.ajax({
        url:"cajeros",
        type:'Post',
        data:{},
        success:function(respuesta){
          document.getElementById('sel_cajero').innerHTML='';
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');def.value="";def.innerText='Selecciona Cajero';document.getElementById('sel_cajero').append(def);
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre+' '+categorias[i].apellido;
              opt.value=categorias[i].cedula;
              document.getElementById('sel_cajero').append(opt);
            }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
  //
  document.getElementById('titulo_principal').innerText='BASE DIARIA DIVISAS';
  var base = document.getElementById('div_base')
  base.style.display='inline'
  document.getElementById('contenedor_principal_divisas').append(base);
}
function objetos_divisa(){
  //total_base_diaira=0;
  document.getElementById('contenido_tabla_base').innerHTML=''
  $.ajax({
        url:"consultar_objetos_divisas",
        type:'Post',
        data:{categoria:document.getElementById('sel_divisa_1').value},
        success:function(respuesta){
          if (respuesta=='false') {
            //console.log(respuesta[0]);
            //alert('no se puede guardar la relacion');
          } else {

            var objs=JSON.parse(respuesta);
            console.log(objs)
            var table = document.getElementById('contenido_tabla_base');
            for (var i = 0; i < objs.length; i++) {
              var tr=document.createElement('tr');
              var td1=document.createElement('td');
              var td2=document.createElement('td');
              var input=document.createElement('input');input.className='form-control col-sm-3';
              input.setAttribute('data-nombre',objs[i].precio_base);input.setAttribute('value','');
              input.setAttribute('data-stock',objs[i].cantidad_base);
              var txt='La maxima cantida de este precio en stock es: '+objs[i].cantidad_base
              input.setAttribute('title',txt);
              input.onchange=function(){
                //calcular_subtotal(this);
                if (parseFloat(this.dataset.stock) >= parseFloat(this.value)) {
                  var inputs_divisa=table.getElementsByTagName('input');
                  var total_cantidades=0;
                  for (var i = 0; i < inputs_divisa.length-3; i++) {
                    if (inputs_divisa[i].value!='') {
                      total_cantidades+=parseFloat(inputs_divisa[i].value);
                    }
                  }
                  document.getElementById('span_total_cantidades').innerText='TOTAL ENTREGA: '+new Intl.NumberFormat().format(total_cantidades);
                } else {
                    alert('la cantidad requerida excede el stock del inventario')
                    //document.getElementById(this.dataset.nombre).innerText='0'
                    this.value=0;
                }
              }
              td1.innerText=objs[i].precio_base;td2.append(input);
              tr.append(td1,td2);
              table.append(tr);
            }
            var trf=document.createElement('tr');
            var td4=document.createElement('td');var td5=document.createElement('td');
            //var lbl=document.createElement('label');lbl.innerText='Total: ';
            //td4.id='total_base_diaria';
            var input4=document.createElement('input');var input5=document.createElement('input');var input6=document.createElement('input');
            input4.id='venta1_gen';input5.id='venta2_gen';input6.id='venta3_gen';
            input4.placeholder='Precio Venta 1';input5.placeholder='Precio Venta 2';input6.placeholder='Precio Venta 3';
            //alert('sis')
            var btn_guardar=document.createElement('button');btn_guardar.className='btn btn-success';btn_guardar.innerText='Entregar Base Monetaria';
            var span_total=document.createElement('span');span_total.id='span_total_cantidades';span_total.className='badge badge-info';
            td4.append(input4,input5,input6);
            //trf.append(btn_guardar,lbl,td4);table.append(trf);
            trf.append(btn_guardar,span_total,td4);table.append(trf);
            btn_guardar.onclick=function(){
              guardar_base_diaria();
            }
          }
        },
        error:function(){

        },
      });
}
function calcular_subtotal(element){
  //alert(typeof(element.dataset.stock)+'--'+element.value)
  if (parseInt(element.dataset.stock) > parseInt(element.value)) {
    document.getElementById(element.dataset.nombre).innerText=(element.dataset.nombre*element.value).toFixed(2);;
    var subs=document.getElementsByName('td3_subtotal');
    console.log(subs);
    var total_base_diaria=0;
    for (var i = 0; i < subs.length; i++) {
      if (subs[i].innerText!="") {
        total_base_diaria+=parseFloat(subs[i].innerText);
      }
    }
    document.getElementById('total_base_diaria').innerText=total_base_diaria.toFixed(2);
  } else {
      alert('la cantidad requerida excede el stock del inventario')
      document.getElementById(element.dataset.nombre).innerText='0'
  }

  //console.log(element);
  //alert();
}
function guardar_base_diaria(){
  if (document.getElementById('sel_cajero').value=="") {
    alert('seleccione un cajero');
  } else {
    var elements=[];
    var tr=document.getElementById('contenido_tabla_base').getElementsByTagName('tr');
    //var input=document.getElementsByTagName('input');
    var hoy = new Date();
    for (var i = 0; i < tr.length-1; i++) {
      var obj=new Object();
      if (tr[i].cells[1].firstChild.value!='') {
        obj.precio_base=tr[i].cells[0].innerText;
        obj.cantidad=tr[i].cells[1].firstChild.value;
        obj.divisa=document.getElementById('sel_divisa_1').value;
        obj.cajero=document.getElementById('sel_cajero').value;
        obj.fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
        obj.time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds();
        obj.stock=tr[i].cells[1].firstChild.dataset.stock;
        obj.precio_venta1= document.getElementById('venta1_gen').value;
        obj.precio_venta2= document.getElementById('venta2_gen').value;
        obj.precio_venta3= document.getElementById('venta3_gen').value;
        elements.push(obj);
      }
    }
    console.log(elements);
    $.ajax({
      url:"base_diaria_entrega",
      type:'Post',
      data:{objs:elements},
      success:function(respuesta){
        if (respuesta=='true') {
          base()
          document.getElementById('contenido_tabla_base').innerHTML='';
          alert('Se guardo la base correctamente')
        } else {
          console.log(respuesta);
        }
      },
      error:function(respuesta){
        console.log(respuesta)
        alert('revisa los valores o verifica si ya se hizo un registro de base diaria para la fecha de hoy')
      },
    });
    return;
  }

}
function ver_bases_diarias(){
  $('#modal_base').modal('show');
  var hoy = new Date();
  var fecha = hoy.getFullYear()+'-'+(hoy.getMonth()+1)+'-'+hoy.getDate();
  //alert(fecha);
  $.ajax({
    url:"ver_bases_diarias",
    type:'Post',
    data:{fecha},
    success:function(respuesta){
      //console.log(respuesta);
      document.getElementById('modal_body_base').innerHTML=''
      var objs=JSON.parse(respuesta);
      var suma_cantidades_divisa=0;
      var divisa='';
      var cajero=objs[0].cedula;
      var cajeros=[];
      var temp=[];
      for (var i = 0; i < objs.length; i++) {
         if (objs[i].cedula==cajero) {
           temp.push(objs[i])
         } else {
          cajero=objs[i].cedula;
          cajeros.push(temp);
          temp=[];
          temp.push(objs[i])
         }
         if (i==objs.length-1) {
          cajeros.push(temp);
         }      
      }
      for (var i = 0; i < cajeros.length; i++) {
        divisa=cajeros[i][0].divisa
        suma_cantidades_divisa=0;
        for (var j = 0; j < cajeros[i].length; j++) {
          //alert(cajeros[i][j].divisa)
          if (divisa==cajeros[i][j].divisa) {
            suma_cantidades_divisa += parseFloat(cajeros[i][j].cantidad_inicial)
            //alert(suma_cantidades_divisa)            
          } else {
            //console.log(suma_cantidades_divisa+'-'+cajeros[i][j])
            var tr = document.createElement('tr');
            var td1 = document.createElement('td');td1.innerText=cajeros[i][j-1].fecha_base;
            var td2 = document.createElement('td');td2.innerText=cajeros[i][j-1].hora_transaccion;
            var td3 = document.createElement('td');td3.innerText=cajeros[i][j-1].nombre_categoria;
            var td4 = document.createElement('td');td4.innerText=cajeros[i][j-1].precio_base;
            //var td5 = document.createElement('td');td5.innerText=objs[i].cantidad_inicial;
            var td5 = document.createElement('td');td5.innerText=suma_cantidades_divisa;
            var td6 = document.createElement('td');td6.innerText=cajeros[i][j-1].nombre+' '+cajeros[i][j-1].apellido;
            //tr.append(td1,td2,td3,td4,td5,td6);
            tr.append(td1,td3,td5,td6);
            document.getElementById('modal_body_base').append(tr);
            suma_cantidades_divisa=0;
            divisa=cajeros[i][j].divisa
            suma_cantidades_divisa += parseFloat(cajeros[i][j].cantidad_inicial)
          }
          if (j==cajeros[i].length-1) {
            var tr = document.createElement('tr');
            var td1 = document.createElement('td');td1.innerText=cajeros[i][j].fecha_base;
            var td2 = document.createElement('td');td2.innerText=cajeros[i][j].hora_transaccion;
            var td3 = document.createElement('td');td3.innerText=cajeros[i][j].nombre_categoria;
            var td4 = document.createElement('td');td4.innerText=cajeros[i][j].precio_base;
            //var td5 = document.createElement('td');td5.innerText=objs[i].cantidad_inicial;
            var td5 = document.createElement('td');td5.innerText=suma_cantidades_divisa;
            var td6 = document.createElement('td');td6.innerText=cajeros[i][j].nombre+' '+cajeros[i][j].apellido;
            //tr.append(td1,td2,td3,td4,td5,td6);
            tr.append(td1,td3,td5,td6);
            document.getElementById('modal_body_base').append(tr);
          }  
        } 
      }
    },
    error:function(error){
      console.log(error);
      alert('sucedio un error')
    },
  });
}
function ingresos_egresos(){
  $('#modal_ingresos_egresos').modal('show');
  $.ajax({
        url:"categorias_divisas",
        type:'Post',
        data:{},
        success:function(respuesta){
          document.getElementById('sel_divisa_ingresos_egresos').innerHTML='';
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');def.innerText='Divisa';document.getElementById('sel_divisa_ingresos_egresos').append(def);;
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_categoria;
              opt.value=categorias[i].id_categoria;
              document.getElementById('sel_divisa_ingresos_egresos').append(opt);
            }
            document.getElementById('sel_divisa_ingresos_egresos').onchange=function(){
              consultar_precios_relacion();
            }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
  //
  $.ajax({
        url:"cajeros",
        type:'Post',
        data:{},
        success:function(respuesta){
          document.getElementById('sel_cajero_ingresos_egresos').innerHTML='';
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');def.value="";def.innerText='Cajero';document.getElementById('sel_cajero_ingresos_egresos').append(def);
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre+' '+categorias[i].apellido;
              opt.value=categorias[i].cedula;
              document.getElementById('sel_cajero_ingresos_egresos').append(opt);
            }
            document.getElementById('sel_cajero_ingresos_egresos').onchange=function(){
              consultar_precios_relacion();
            }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
}
function consultar_precios_relacion(){
  var div_1=document.getElementById('sel_divisa_ingresos_egresos').value;
  var div_2=1;
  var cedula=document.getElementById('sel_cajero_ingresos_egresos').value
  var hoy = new Date();
  var fecha = hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  $.ajax({
        url:"consultar_relacion",
        type:'Post',
        data:{div1:div_1,div2:div_2,cedula:cedula,fecha:fecha},
        success:function(respuesta){
          $('#sel_precios_ingresos_egresos').html('');
          $('#modal_compra').html('');
          if (respuesta=='false') {
          } else {
            var rel=JSON.parse(respuesta);
            console.log(rel);
            var opt0 = document.createElement('option');
            opt0.innerText='opciones compra';
            $('#sel_precios_ingresos_egresos').append(opt0);
            for (var i = 0; i < rel.length; i++) {
              var opt = document.createElement('option');
              opt.value=rel[i].precio_base;opt.innerText=rel[i].precio_base
              $('#sel_precios_ingresos_egresos').append(opt);
            }

          }
        },

        error:function(){

        },

    });
}
function format(input)
{
  var num = input.value.replace(/\,/g,'');
  if(!isNaN(num)){
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
    num = num.split('').reverse().join('').replace(/^[\,]/,'');
    input.value = num;
  }

  else{ alert('Solo se permiten numeros');
    input.value = input.value.replace(/[^\d\.]*/g,'');
  }
}
function guardar_ingreso_egreso(element){
  var obj=new Object();var hoy = new Date();
  obj.cajero=document.getElementById('sel_cajero_ingresos_egresos').value;
  obj.divisa=document.getElementById('sel_divisa_ingresos_egresos').value;
  obj.fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  obj.precio=document.getElementById('sel_precios_ingresos_egresos').value;
  obj.valor=document.getElementById('input_ingreso_egreso').value.replace(/[,]/g,'');
  obj.tipo=element.dataset.tipo;
  if (obj.cajero!=''||obj.divisa!=''||obj.valor!=''||obj.precio!='') {
    $.ajax({
          url:"insertar_ingresos_egresos",
          type:'Post',
          data:{obj:obj},
          success:function(respuesta){
            if (respuesta=='true') {
              alert('se guardo correctamente')
            } else {
              alert('NO se guardo');
            }
          },

          error:function(){
            alert('ocurrio un error')
          },

      });
  } else {
    alert('!revisa los valores¡');
  }

}
function solicitud_pendiente(){
  $.ajax({
        url:"solicitud_pendiente",
        type:'Post',
        data:{},
        success:function(respuesta){
          var solicitud=JSON.parse(respuesta)
          document.getElementById('span_solicitud').innerText=solicitud.length;
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
}
function ver_solicitud(){
  $.ajax({
        url:"solicitud_pendiente",
        type:'Post',
        data:{},
        success:function(respuesta){
          var solicitud=JSON.parse(respuesta)
          $('#modal_ver_solicitud').modal('show');
          $('#modal_body_solicitud').html('');
          for (var i = 0; i < solicitud.length; i++) {
            var tr=document.createElement('tr');
            for (var j = 0; j < 8; j++) {
              var td = document.createElement('td');
              switch (j) {
                case 0:
                  td.innerText=solicitud[i].fecha_sol
                  break;
                case 1:
                    td.innerText=solicitud[i].cajero_sol
                  break;
                case 2:
                    td.innerText=solicitud[i].nombre_categoria
                  break;
                case 3:
                    td.innerText=solicitud[i].precio
                  break;
                case 4:
                    td.innerText=solicitud[i].valor_sol
                  break;
                case 5:
                    td.innerText=solicitud[i].tipo_sol
                  break;
                case 6:
                    td.innerText=solicitud[i].estado_sol
                  break;
                case 7:
                    var btn1=document.createElement('button');btn1.className='btn btn-success';btn1.innerText='Aprobar';btn1.setAttribute('data-solicitud',JSON.stringify(solicitud[i]));btn1.setAttribute('data-estado','A');
                    var btn2=document.createElement('button');btn2.className='btn btn-danger';btn2.innerText='Rechazar';btn2.setAttribute('data-solicitud',JSON.stringify(solicitud[i]));btn2.setAttribute('data-estado','R');
                    btn1.onclick=function(){
                      //modificar_estado_solicitud(this);
                      //llenar_modal_solicitud(this);
                      aprobar_sol_auto(this)
                      //ingresos_egresos();
                    }
                    btn2.onclick=function(){
                      modificar_estado_solicitud(this);
                    }
                    td.append(btn1,btn2);
                  break;

              }
              tr.append(td);
              document.getElementById('modal_body_solicitud').append(tr);
            }
          }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
}
function modificar_estado_solicitud(element){
  var objeto=JSON.parse(element.dataset.solicitud);
  objeto.estado_sol=element.dataset.estado;
  $.ajax({
        url:"cambiar_estado_solicitud",
        type:'Post',
        data:{objeto},
        success:function(respuesta){
          if (respuesta=='true') {
            alert('se modifico el estado ')
          } else {

          }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
}
function llenar_modal_solicitud(element){
  var solicitud=JSON.parse(element.dataset.solicitud);
  $('#modal_impresion_solicitud').modal('show');
  $('#modal_body_impresion_solicitud2').html('');
  var label=document.createElement('label');
  label.innerText='CONTINENTAL CAMBIOS\nCAMBIO DE MONEDA\nMoney Exchange\nTERMINAL TERRESTRE\nLocal 47 Piso 2 Ipiales-Nariño\nTel:7731277\nCel:3175736096\nNit: 1.085.901.197-9\n-------------------\nFecha:'+solicitud.fecha_sol+' \nCajero:'+solicitud.cajero_sol+'\nDivisa:'+solicitud.nombre_categoria+'\nValor:'+solicitud.valor_sol+'\nTipo:'+solicitud.tipo_sol+'\nAPROBADO\n-------------------';
  $('#modal_body_impresion_solicitud2').append(label);
  function imprimir_reporte_solicitud(){
    $('#modal_body_principal_2').printThis();
  }
}
function imprimir_reporte_solicitud(){
  $('#modal_body_impresion_solicitud').printThis();
  //base();
}
function aprobar_sol_auto(element){
  //
  var temp =JSON.parse(element.dataset.solicitud);
  //console.log(temp);
  var obj = new Object();
  obj.cajero=temp.cajero_sol
  obj.divisa=temp.divisa_sol
  obj.valor=temp.valor_sol
  obj.tipo=temp.tipo_sol;
  obj.fecha=temp.fecha_sol;
  obj.precio=temp.precio;
  console.log(obj);
  $.ajax({
        url:"insertar_ingresos_egresos",
        type:'Post',
        data:{obj:obj},
        success:function(respuesta){
          if (respuesta=='true') {
            alert('se guardo correctamente');
            modificar_estado_solicitud(element);
            llenar_modal_solicitud(element);
            //base();
          } else {
            alert('NO se guardo');
            if (respuesta=='El valor solicitado exede la cantidad de este precio de divisa en el inventario') {
              alert(respuesta)
            } else {
              alert('Posiblemente no existe este precio para esta divisa en el inventario global')
            }

          }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
}
