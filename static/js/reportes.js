$(document).ready(report_div);
function report_div(){
  var inicio=document.getElementById('fecha_inicio').value,
      fin=document.getElementById('fecha_fin').value;
  document.getElementById('thead_tabla').innerHTML='';document.getElementById('body_tabla').innerHTML='';
  if ((inicio==''&&fin=='')||(inicio==''&&fin!='')||(inicio!=''&&fin=='')||(inicio!=''&&fin!=''&&inicio<=fin)) {
    //inicio=inicio.replace(/[/]/g,'-');fin=fin.replace(/[/]/g,'-');
    //alert(inicio+'<>'+fin)
    consultar_datos_div(inicio,fin)
  } else {
    alert('La fecha fin debe ser mayor a la fecha de inicio de buscqueda');
  }

}
//
function report_seg(){
  var inicio=document.getElementById('fecha_inicio').value,
      fin=document.getElementById('fecha_fin').value;
  document.getElementById('thead_tabla').innerHTML='';document.getElementById('body_tabla').innerHTML='';
  if ((inicio==''&&fin=='')||(inicio==''&&fin!='')||(inicio!=''&&fin=='')||(inicio!=''&&fin!=''&&inicio<=fin)) {
    //inicio=inicio.replace(/[/]/g,'-');fin=fin.replace(/[/]/g,'-');
    //alert(inicio+'<>'+fin)
    consultar_datos_seg(inicio,fin);
  } else {
    alert('La fecha fin debe ser mayor a la fecha de inicio de buscqueda');
  }
}
//
function report_gen(){
  var inicio=document.getElementById('fecha_inicio').value,
      fin=document.getElementById('fecha_fin').value;
  document.getElementById('thead_tabla').innerHTML='';document.getElementById('body_tabla').innerHTML='';
  if ((inicio==''&&fin=='')||(inicio==''&&fin!='')||(inicio!=''&&fin=='')||(inicio!=''&&fin!=''&&inicio<=fin)) {
    //inicio=inicio.replace(/[/]/g,'-');fin=fin.replace(/[/]/g,'-');
    //alert(inicio+'<>'+fin)
    consultar_datos_gen(inicio,fin)
  } else {
    alert('La fecha fin debe ser mayor a la fecha de inicio de buscqueda');
  }
}
//
function actualizar_fecha_minima(){
  var min=document.getElementById('fecha_inicio').value;
  document.getElementById('fecha_fin').setAttribute('min',min);
}
//
function consultar_datos_div(inicio,fin){
  //console.log(inicio+'--'+fin)
  if(inicio==''&&fin==''){
    let hoy = new Date();
    let mes =hoy.getMonth()
    if(mes <=9){
      mes+=1;
    }
    inicio=hoy.getFullYear()+'/'+mes+'/'+hoy.getDate();
    fin=hoy.getFullYear()+'/'+mes+'/'+hoy.getDate();
    //console.log(inicio+'--'+fin)
  }
  $.ajax({

    url:"Transacciones/consultar_transacciones_divisas",
    type:'Post',
    data:{inicio:inicio,fin:fin},
    success:function(respuesta){
      //
      //console.log(respuesta);
      var datos=JSON.parse(respuesta);
      var trh=document.createElement('tr');document.getElementById('thead_tabla').append(trh);
      for (var k = 0; k < 13; k++) {
        var td=document.createElement('th');
        trh.append(td);
        switch (k) {
          case 0:
            td.innerText='Fecha';
            break;
          case 1:
            td.innerText='Hora';
            break;
          case 2:
            td.innerText='Divisa 1';
            break;
          case 3:
            td.innerText='Divisa 2';
            break;
          case 4:
            td.innerText='Cantidad';
            break;
          case 5:
            td.innerText='Precio Base';
            break;
          case 6:
            td.innerText='Total';
            break;
          case 7:
            td.innerText='Tipo';
            break;
          case 8:
            td.innerText='Cajero';
            break;
          case 9:
            td.innerText='Nombre Caj';
            break;
          case 10:
            td.innerText='Cliente';
            break;
          case 11:
            td.innerText='Nombre Cli';
            break;
          case 12:
            td.innerText='Selecionar';
            break;

        }

      }
      for (var i = 0; i < datos.length; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < 13; j++) {
          var td = document.createElement('td');
          tr.append(td);
          switch (j) {
            case 0:
              td.innerText=datos[i]['fecha_div'];
              break;
            case 1:
                td.innerText=datos[i]['hora_div'];
                break;
            case 2:
              td.innerText=datos[i]['nombre_categoria'];
              break;
            case 3:
              //td.innerText=datos[i]['div2'];
              td.innerText='PESO';
              break;
            case 4:
              td.innerText=datos[i]['cantidad_div'];
              break;
            case 5:
              td.innerText=datos[i]['precio_uni_div'];
              break;
            case 6:
              td.innerText=datos[i]['total_tra_div'];
              break;
            case 7:
              td.innerText=datos[i]['tipo'];
              break;
            case 8:
              td.innerText=datos[i]['ced_cajero_div'];
              break;
            case 9:
              td.innerText=datos[i]['nombre_cajero_div'];
              break;
            case 10:
              td.innerText=datos[i]['ced_cliente_div'];
              break;
            case 11:
              td.innerText=datos[i]['nombre_cliente_div'];
              break;
            case 12:
              var btn=document.createElement('button');btn.setAttribute('data-objeto',JSON.stringify(datos[i]));btn.className='btn btn-info';btn.innerText='IMPRIMIR'
              var btn2=document.createElement('button');btn2.setAttribute('data-objeto',JSON.stringify(datos[i]));btn2.setAttribute('data-orden',(datos.length)-i);btn2.className='btn btn-danger';btn2.innerText='eliminar'
              var btn3=document.createElement('button');btn3.setAttribute('data-objeto',JSON.stringify(datos[i]));btn3.innerText='18'
              var btn4=document.createElement('button');btn4.setAttribute('data-objeto',JSON.stringify(datos[i]));btn4.innerText='536'
              btn.onclick=function(){
                imprimir_nuevamente(this);
              }
              btn2.onclick=function(){
                var r= confirm('esta seguro de eliminar');
                if (r==true) {
                  eliminar_transaccion_divisa(this);
                } else {

                }
              }
              btn3.onclick=function(){
                prueba_formulario_18_rep(this);
              }
              btn4.onclick=function(){
                formulario_2_rep(this)
              }
              td.append(btn,btn2,btn3,btn4);
              break;

          }
        }
        document.getElementById('body_tabla').append(tr);
      }
    },
    error:function(){

    },
  });
}
function consultar_datos_seg(inicio,fin){
  $.ajax({

    url:"Transacciones/consultar_transacciones_seguros",
    type:'Post',
    data:{inicio:inicio,fin:fin},
    success:function(respuesta){
      //
      //console.log(respuesta);
      var datos=JSON.parse(respuesta);
      var trh=document.createElement('tr');document.getElementById('thead_tabla').append(trh);
      for (var k = 0; k < 12; k++) {
        var td=document.createElement('th');
        trh.append(td);
        switch (k) {
          case 0:
            td.innerText='Fecha';
            break;
          case 1:
            td.innerText='Nombre Seg';
            break;
          case 2:
            td.innerText='Categoria';
            break;
          case 3:
            td.innerText='Placa';
            break;
          case 4:
            td.innerText='Precio Transaccion';
            break;
          case 5:
            td.innerText='Precio Base';
            break;
          case 6:
            td.innerText='Total';
            break;
          case 7:
            td.innerText='Cajero';
            break;
          case 8:
            td.innerText='Nombre Caj';
            break;
          case 9:
            td.innerText='Cliente';
            break;
          case 10:
            td.innerText='Nombre Cli';
            break;
          case 11:
            td.innerText='Acciones';
            break;

        }

      }
      for (var i = 0; i < datos.length; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < 12; j++) {
          var td = document.createElement('td');
          tr.append(td);
          switch (j) {
            case 0:
              td.innerText=datos[i]['fecha_seg'];
              break;
            case 1:
              td.innerText=datos[i]['nombre_seg'];
              break;
            case 2:
              td.innerText=datos[i]['categoria_seg'];
              break;
            case 3:
              td.innerText=datos[i]['placa'];
              break;
            case 4:
              td.innerText=datos[i]['precio_uni_seg'];
              break;
            case 5:
              td.innerText=datos[i]['precio_base_seg'];
              break;
            case 6:
              td.innerText=datos[i]['total_tra_seg'];
              break;
            case 7:
              td.innerText=datos[i]['ced_cajero_seg'];
              break;
            case 8:
              td.innerText=datos[i]['nombre_cajero_seg'];
              break;
            case 9:
              td.innerText=datos[i]['ced_cliente_seg'];
              break;
            case 10:
              td.innerText=datos[i]['nombre_cliente_seg'];
              break;
            case 11:
              //var check=document.createElement('input');check.setAttribute('type','checkbox');check.setAttribute('name','seleccionar');
              //td.append(check);
              var btn_eliminar_seg=document.createElement('button');btn_eliminar_seg.className='btn btn-danger';btn_eliminar_seg.innerText='eliminar';btn_eliminar_seg.setAttribute('data-objeto',JSON.stringify(datos[i]));
              btn_eliminar_seg.onclick=function(){
                var objeto_seguro=JSON.parse(this.dataset.objeto);
                var r = confirm('Confirmar para eliminar transaccion de seguros, se eliminara la transaccion y su respectivo registro en la contabilidad');
                if (r==true) {
                  $.ajax({

                    url:"Transacciones/eliminar_transaccion_seguros",
                    type:'Post',
                    data:{objeto:objeto_seguro},
                    success:function(respuesta){
                      if (respuesta=='true') {
                        alert('se elimino la transaccion de seguros correctamente');
                        report_seg();
                      } else {
                        //alert('se elimino la transaccion de seguros correctamente');
                        alert(respuesta);
                      }
                    },
                    error:function(respuesta){
                      alert(respuesta)
                    },
                  });
                }
              }
              td.append(btn_eliminar_seg);
              break;

          }
        }
        document.getElementById('body_tabla').append(tr);
      }
    },
    error:function(){

    },
  });
}
function consultar_datos_gen(inicio,fin){
  $.ajax({

    url:"Transacciones/consultar_transacciones_general",
    type:'Post',
    data:{inicio:inicio,fin:fin},
    success:function(respuesta){
      //
      //console.log(respuesta);
      var datos=JSON.parse(respuesta);
      var trh=document.createElement('tr');document.getElementById('thead_tabla').append(trh);
      for (var k = 0; k < 12; k++) {
        var td=document.createElement('th');
        trh.append(td);
        switch (k) {
          case 0:
            td.innerText='Fecha';
            break;
          case 1:
            td.innerText='Nombre';
            break;
          case 2:
            td.innerText='Categoria';
            break;
          case 3:
            td.innerText='Cantidad';
            break;
          case 4:
            td.innerText='Precio Transaccion';
            break;
          case 5:
            td.innerText='Precio Base';
            break;
          case 6:
            td.innerText='Total';
            break;
          case 7:
            td.innerText='Cajero';
            break;
          case 8:
            td.innerText='Nombre Caj';
            break;
          case 9:
            td.innerText='Cliente';
            break;
          case 10:
            td.innerText='Nombre Cli';
            break;
          case 11:
            td.innerText='Acciones';
            break;

        }

      }
      for (var i = 0; i < datos.length; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < 12; j++) {
          var td = document.createElement('td');
          tr.append(td);
          switch (j) {
            case 0:
              td.innerText=datos[i]['fecha_gen'];
              break;
            case 1:
              td.innerText=datos[i]['nombre_gen'];
              break;
            case 2:
              td.innerText=datos[i]['categoria_gen'];
              break;
            case 3:
              td.innerText=datos[i]['cantidad_gen'];
              break;
            case 4:
              td.innerText=datos[i]['precio_uni_gen'];
              break;
            case 5:
              td.innerText=datos[i]['precio_base_gen'];
              break;
            case 6:
              td.innerText=datos[i]['total_tra_gen'];
              break;
            case 7:
              td.innerText=datos[i]['ced_cajero_gen'];
              break;
            case 8:
              td.innerText=datos[i]['nombre_cajero_gen'];
              break;
            case 9:
              td.innerText=datos[i]['ced_cliente_gen'];
              break;
            case 10:
              td.innerText=datos[i]['nombre_cliente_gen'];
              break;
            case 11:
              var icon=document.createElement('i');icon.setAttribute('data-obj',JSON.stringify(datos[i]));icon.className='btn btn-danger';icon.innerText='Eliminar'
              icon.onclick=function(){
                var r = confirm('Confirmar para eliminar');
                if (r==true) {
                  $.ajax({

                    url:"Transacciones/revertir_venta_general",
                    type:'Post',
                    //data:{fecha:fecha,fecha2:fecha2},
                    data:{obj:JSON.parse(this.dataset.obj)},
                    success:function(respuesta){
                      if (respuesta=='true') {
                        alert('Se elimino la transaccion');
                        report_gen()
                      } else {

                      }
                    },
                    error:function(){

                    },
                  });
                }
              }
              td.append(icon);
              break;

          }
        }
        document.getElementById('body_tabla').append(tr);
      }
    },
    error:function(){

    },
  });
}
function report_global(){
  $('#modal_informe_global').modal('show');
}
function informe_global(){
  document.getElementById('modal_body_informe_global').style.display='inline';
  document.getElementById('modal_body_bancos').style.display='none';
}
function bancos(){
  document.getElementById('modal_body_informe_global').style.display='none';
  document.getElementById('modal_body_bancos').style.display='inline';
}
function format(input)
{
  var num = input.value.replace(/\,/g,'');
  if(!isNaN(num)){
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
    num = num.split('').reverse().join('').replace(/^[\,]/,'');
    input.value = num;
  }

  else{ alert('Solo se permiten numeros');
    input.value = input.value.replace(/[^\d\.]*/g,'');
  }
}
function generar_informe_global(){
  //var fecha=document.getElementById('fecha_informe').value;
  //var fecha2=document.getElementById('fecha_informe_2').value;
  $.ajax({

    url:"Transacciones/informe_global_divisas_pesos",
    type:'Post',
    //data:{fecha:fecha,fecha2:fecha2},
    data:{},
    success:function(respuesta){
      document.getElementById('contenedor_informe_divisas2').innerHTML=''
      document.getElementById('gran_total_global').innerHTML='';
      var datos1=JSON.parse(respuesta);
      var table = document.createElement('table');table.className='table table-hover';
      var thead=document.createElement('thead');thead.className='thead-dark';table.append(thead)
      var trh=document.createElement('tr');thead.append(trh);
      var th1=document.createElement('th');th1.innerText='PESOS: '+new Intl.NumberFormat().format(datos1[0].cantidad_base);trh.append(th1);
      document.getElementById('contenedor_informe_divisas2').append(table);
      //document.getElementById('gran_total_global').innerText=datos[0].cantidad_base;
    //
    $.ajax({

      url:"Transacciones/informe_global_divisas",
      type:'Post',
      //data:{fecha:fecha,fecha2:fecha2},
      data:{},
      success:function(respuesta){
        //
        //console.log(respuesta);

        var datos=JSON.parse(respuesta);
        document.getElementById('contenedor_informe_divisas').innerHTML=''

        //por si es por reportes
        // for (var i = 0; i < datos.length; i++) {
        //   var precio=0;
        //   var total_parcial=0;
        //   for (var j = 0; j < datos[i].length; j++) {
        //     //console.log(datos[i][j].precio_uni_div);
        //     if(datos[i][j].precio_uni_div==precio){
        //       //console.log()
        //       total_parcial+=datos[i][j].total_tra_div
        //     }else{
        //       console.log(precio+'--'+total_parcial);
        //       precio=datos[i][j].precio_uni_div;
        //
        //       total_parcial=datos[i][j].total_tra_div;
        //     }
        //   }
        //   //
        // }
        //fin por reportes

        //por INVENTARIO
        var nombre='';var total_div=0;var suma_cantidades=0;
        for (var i = 0; i < datos.length; i++) {

          if (nombre==datos[i].nombre_categoria) {
            var tr=document.createElement('tr');
            var td1 =document.createElement('td');
            var td2 =document.createElement('td');
            var td3 =document.createElement('td');
            tr.append(td1,td2,td3);
            total_div+=datos[i].cantidad_base*datos[i].precio_base;
            var subtotal=datos[i].cantidad_base*datos[i].precio_base;
            console.log(suma_cantidades);
            suma_cantidades+=parseFloat(datos[i].cantidad_base);
            td1.innerText=datos[i].cantidad_base;td2.innerText=datos[i].precio_base;td3.innerText=new Intl.NumberFormat().format(datos[i].cantidad_base*datos[i].precio_base);
            document.getElementById('tbody_'+nombre).append(tr);
            //console.log(total_div);
            if (i==datos.length-1) {
              var trf=document.createElement('tr');
              var td0=document.createElement('td');td0.innerHTML='CANTIDAD TOTAL: '+new Intl.NumberFormat().format(suma_cantidades);trf.append(td0);
              var td=document.createElement('td');td.innerHTML='TOTAL FINAL EN PESOS: '+new Intl.NumberFormat().format(total_div);trf.append(td);
              document.getElementById('tbody_'+nombre).append(trf);
              var total_global=document.getElementById('gran_total_global').innerText;
              if (total_global=='') {
                total_global=0;
              }
              document.getElementById('gran_total_global').innerText=parseFloat(total_global)+total_div;
            }
          } else {
            if (nombre!='') {
              var trf=document.createElement('tr');
              var td0=document.createElement('td');td0.innerHTML='CANTIDAD TOTAL: '+new Intl.NumberFormat().format(suma_cantidades);trf.append(td0);
              var td=document.createElement('td');td.innerHTML='TOTAL FINAL EN PESOS: '+new Intl.NumberFormat().format(total_div);trf.append(td);
              document.getElementById('tbody_'+nombre).append(trf);
              var total_global=document.getElementById('gran_total_global').innerText;
              if (total_global=='') {
                total_global=0;
              }
              document.getElementById('gran_total_global').innerText=parseFloat(total_global)+total_div;
            }
            //console.log(total_div);
            total_div=0
            suma_cantidades=0
            nombre=datos[i].nombre_categoria;
            var div=document.createElement('div');div.className='card';
            var div_header=document.createElement('div');div_header.className='card-header text-white text-center bg-success mb-3';
            var div_body=document.createElement('div');div_body.className='card-body';
            div.append(div_header,div_body);
            document.getElementById('contenedor_informe_divisas').append(div);
            div_header.innerText=datos[i].nombre_categoria;
            var table = document.createElement('table');table.className='table table-hover';
            var thead=document.createElement('thead');thead.className='thead-dark'
            var tbody=document.createElement('tbody');tbody.id='tbody_'+datos[i].nombre_categoria;
            var trh=document.createElement('tr');
            var th1=document.createElement('th');th1.innerText='CANTIDAD';
            var th2=document.createElement('th');th2.innerText='PRECIO';
            var th3=document.createElement('th');th3.innerText='TOTAL'
            trh.append(th1,th2,th3);thead.append(trh);table.append(thead,tbody)
            div_body.append(table);
            var tr=document.createElement('tr');
            var td1 =document.createElement('td');
            var td2 =document.createElement('td');
            var td3 =document.createElement('td');
            tr.append(td1,td2,td3);
            td1.innerText=datos[i].cantidad_base;td2.innerText=datos[i].precio_base;td3.innerText=new Intl.NumberFormat().format(datos[i].cantidad_base*datos[i].precio_base);
            tbody.append(tr);
            total_div+=datos[i].cantidad_base*datos[i].precio_base;
            suma_cantidades+=parseFloat(datos[i].cantidad_base);
            if (i==datos.length-1) {
              //alert('si')
              var trf=document.createElement('tr');
              var td0=document.createElement('td');td0.innerHTML='CANTIDAD TOTAL: '+new Intl.NumberFormat().format(suma_cantidades);trf.append(td0);
              var td=document.createElement('td');td.innerHTML='TOTAL FINAL EN PESOS: '+new Intl.NumberFormat().format(total_div);trf.append(td);
              document.getElementById('tbody_'+nombre).append(trf);
              var total_global=document.getElementById('gran_total_global').innerText;
              if (total_global=='') {
                total_global=0;
              }
              //document.getElementById('gran_total_global').innerText=new Intl.NumberFormat().format(parseFloat(total_global)+total_div);
              document.getElementById('gran_total_global').innerText=parseFloat(total_global)+total_div+parseFloat(datos1[0].cantidad_base);
            }else{
              //alert(datos.length+'-'+i)
            }
          }

        }
        // fin por inventario
      },
      error:function(){

      },
    });
    },
  });
  $.ajax({

    url:"Prestamos/cargar_prestamos",
    type:'Post',
    //data:{fecha:fecha,fecha2:fecha2},
    data:{},
    success:function(respuesta){
      var datos=JSON.parse(respuesta);
      document.getElementById('contenedor_informe_varios').innerHTML=''
      for (var i = 0; i < datos.length; i++) {
        var tr=document.createElement('tr');
        var td1=document.createElement('td');var td2=document.createElement('td');var td3=document.createElement('td');var td4=document.createElement('td');
        tr.append(td1,td2,td3,td4);
        td1.innerHTML=datos[i].fecha;td2.innerHTML=datos[i].nombre_prestamo;td3.innerHTML=datos[i].cantidad_prestamo;td4.innerHTML=datos[i].abono;
        document.getElementById('contenedor_informe_varios').append(tr);
      }

    },
    error:function(){

    },
  });
  $.ajax({

    url:"Transacciones/cargar_bancos",
    type:'Post',
    //data:{fecha:fecha,fecha2:fecha2},
    data:{},
    success:function(respuesta){
      console.log(respuesta)
      var datos=JSON.parse(respuesta);console.log(datos);
      document.getElementById('contenedor_informe_bancos').innerHTML=''
      document.getElementById('contenedor_informe_bancos').innerHTML=datos[0].valor_bancos
      var total_global=document.getElementById('gran_total_global').innerText;
      if (total_global=='') {
        total_global=0;
      }
      document.getElementById('gran_total_global').innerText=parseFloat(total_global)+parseFloat(datos[0].valor_bancos);

    },
    error:function(){

    },
  });
}
function actualizar_bancos(){
  valor=document.getElementById('consignaciones_bancos').value.replace(/[,]/g,'')
  $.ajax({
    url:"Transacciones/actualizar_bancos",
    type:'Post',
    //data:{fecha:fecha,fecha2:fecha2},
    data:{valor:valor},
    success:function(respuesta){
      if (respuesta=='true') {
        alert('se actualizo');
      } else {
        alert('NO! se actualizo');
      }
    },
    error:function(){

    },
  });
}
function report_cajeros(){
  $('#modal_informe_cajeros').modal('show');
  $.ajax({
    url:"Divisas/cajeros",
    type:"Post",
    data:{},
    success:function(respuesta){
      var cajeros=JSON.parse(respuesta);
      var sel=document.getElementById('cajeros_select_modal');
      sel.innerHTML='';
      var def=document.createElement('option');def.innerText='Seleccione Cajero';def.value="";
      sel.append(def);
      for (var i = 0; i < cajeros.length; i++) {
        var opt=document.createElement('option');
        opt.value=cajeros[i].cedula;
        opt.innerText=cajeros[i].nombre+' '+cajeros[i].apellido;
        sel.append(opt);
      }
      // sel.onchange=function(){
      //   mostar_informe_cajero(sel.value);
      // }
    },
  });
}
// function mostar_informe_cajero(){
//   var fecha=document.getElementById('cajeros_fecha_input').value
//   var cajero=document.getElementById('cajeros_select_modal').value
//   $.ajax({
//     url:"Divisas/informe_cajero",
//     type:"Post",
//     data:{cajero:cajero,fecha:fecha},
//     success:function(respuesta){
//       document.getElementById('contenedor_informe_cajeros').innerHTML=''
//       var informe=JSON.parse(respuesta);
//       console.log(informe);
//       var tem=0;
//       var compra=0;var venta=0;
//       for (var i = 0; i < informe.length; i++) {
//
//         if (tem!=informe[i].nombre_categoria) {
//           var tem_ant=tem
//           tem=informe[i].nombre_categoria;
//           var div = document.createElement('div');div.className='row';var br=document.createElement('hr');
//           var spna0=document.createElement('span');spna0.className='col-sm-2 badge badge-info';spna0.innerText='DIVISA:'
//           var spna1=document.createElement('span');spna1.className='col-sm-2 ';spna1.id=tem+'1';
//           var spna2=document.createElement('span');spna2.className='col-sm-2 badge badge-info';spna2.innerText='COMPRA:'
//           var spna3=document.createElement('span');spna3.className='col-sm-2 ';spna3.id=tem+'2'
//           var spna4=document.createElement('span');spna4.className='col-sm-2 badge badge-info';spna4.innerText='VENTA:'
//           var spna5=document.createElement('span');spna5.className='col-sm-2 ';spna5.id=tem+'3'
//           div.append(spna0,spna1,spna2,spna3,spna4,spna5);
//           document.getElementById('contenedor_informe_cajeros').append(div,br);
//           if (tem_ant!=0) {
//             //alert(compra+'--'+venta+'--'+tem_ant)
//             document.getElementById(tem_ant+'1').innerText=tem_ant;
//             document.getElementById(tem_ant+'2').innerText=compra;
//             document.getElementById(tem_ant+'3').innerText=venta;
//             if (informe[i].tipo=='compra') {
//               compra=parseFloat(informe[i].cantidad_div);
//               venta=0
//             } else {
//               venta=parseFloat(informe[i].cantidad_div);
//               compra=0
//             }
//             if (i==informe.length-1) {
//               //alert(compra+'--'+venta+'--'+tem)
//               spna1.innerText=tem;
//               spna3.innerText=compra;
//               spna5.innerText=venta;
//             }
//           } else {
//
//           }
//
//         } else {
//           if (informe[i].tipo=='compra') {
//             compra+=parseFloat(informe[i].cantidad_div);
//           } else {
//             venta+=parseFloat(informe[i].cantidad_div);
//           }
//           if (i==informe.length-1) {
//             //alert(compra+'--'+venta+'--'+tem)
//             spna1.innerText=tem;
//             spna3.innerText=compra;
//             spna5.innerText=venta;
//           }
//         }
//       }
//     },
//   });
// }
function mostar_informe_cajero(){
  var fecha=document.getElementById('cajeros_fecha_input').value
  var cajero=document.getElementById('cajeros_select_modal').value
  $.ajax({
    url:"Divisas/informe_cajero",
    type:"Post",
    data:{cajero:cajero,fecha:fecha},
    success:function(respuesta){
      var objetos=JSON.parse(respuesta);
      document.getElementById('contenedor_informe_cajeros').innerHTML=''
      var pesos_compra=0;var pesos_venta=0;
      for (var i = 0; i < objetos[0].length; i++) {
        if(objetos[0][i].tipo=='compra'){
          pesos_compra+=parseFloat(objetos[0][i].total_tra_div);
        }
        else{
          pesos_venta+=parseFloat(objetos[0][i].cantidad_div);
        }
      }
      var divisa='';
      var tot_parcial=0
      var suma_ingresos=0;
      var suma_egresos=0;
      for (var i = 0; i < objetos[1].length; i++) {
        var egreso_actualizacion=0;
        var ingreso_actualizacion=0;
        if(objetos[1][i].ingreso!=null){
          ingreso_actualizacion=objetos[1][i].ingreso;
        }
        if(objetos[1][i].egreso!=null){
          egreso_actualizacion=objetos[1][i].egreso;
        }
        
        if (divisa!=objetos[1][i].nombre_categoria&&objetos[1][i].nombre_categoria!='PESO') {
          if (divisa!='') {
            document.getElementById('head'+divisa).innerText=divisa+' TOTAL: '+tot_parcial;
            document.getElementById('sum_ing'+divisa).innerText=' SUMA INGRESOS: '+suma_ingresos;
            document.getElementById('sum_egr'+divisa).innerText=' SUMA EGRESOS: '+suma_egresos;
          }
          tot_parcial=0;
          divisa=objetos[1][i].nombre_categoria;
          suma_ingresos=0;
          suma_egresos=0;
          
          var table = document.createElement('table');table.className='table table-hover table-bordered';
          var thead = document.createElement('thead');thead.className='thead-dark';
          var tbody = document.createElement('tbody');tbody.id=divisa;
          var trh1 = document.createElement('tr');
          var th1=document.createElement('th');th1.colSpan="2";trh1.append(th1);th1.id='head'+divisa
          var th6=document.createElement('th');th6.innerText='SUMA Ingresos: ';trh1.append(th6);th6.id='sum_ing'+divisa;
          var th7=document.createElement('th');th7.innerText='SUMA Egresos';trh1.append(th7);th7.id='sum_egr'+divisa;
          var trh2 = document.createElement('tr');thead.append(trh1,trh2);
          var th2=document.createElement('th');th2.innerText='Precio';trh2.append(th2);
          var th3=document.createElement('th');th3.innerText='Cantidad';trh2.append(th3);
          var th4=document.createElement('th');th4.innerText='Ingresos';trh2.append(th4);
          var th5=document.createElement('th');th5.innerText='Egresos';trh2.append(th5);
          table.append(thead,tbody);
          var tr = document.createElement('tr');var td1=document.createElement('td');var td2=document.createElement('td');var td3=document.createElement('td');var td4=document.createElement('td');tr.append(td1,td2,td3,td4);tbody.append(tr);
          td1.innerText=objetos[1][i].precio_base;td2.innerText=objetos[1][i].cantidad;td3.innerText=ingreso_actualizacion;td4.innerText=egreso_actualizacion;
          document.getElementById('contenedor_informe_cajeros').append(table);
          tot_parcial+=parseFloat(objetos[1][i].cantidad)
          suma_ingresos+=parseFloat(ingreso_actualizacion);
          suma_egresos+=parseFloat(egreso_actualizacion);
          if (i==objetos[1].length-1) {
            document.getElementById('head'+divisa).innerText=divisa+' TOTAL: '+tot_parcial;
            document.getElementById('sum_ing'+divisa).innerText=' SUMA INGRESOS: '+suma_ingresos;
            document.getElementById('sum_egr'+divisa).innerText=' SUMA EGRESOS: '+suma_egresos;
          }
        } else {
          if (objetos[1][i].nombre_categoria=='PESO') {
            
            var table = document.createElement('table');table.className='table table-hover table-bordered';
            var thead = document.createElement('thead');thead.className='thead-dark';
            var trh1 = document.createElement('tr');
            var th1=document.createElement('th');th1.colSpan="2";trh1.append(th1);th1.id='peso'
            var th2=document.createElement('th');trh1.append(th2);th2.innerText='Ingresos: '+(parseFloat(ingreso_actualizacion));
            var th3=document.createElement('th');trh1.append(th3);th3.innerText='Egresos: '+(parseFloat(egreso_actualizacion));
            th1.innerText='PESO TOTAL: '+(parseFloat(objetos[1][i].cantidad)+pesos_venta-pesos_compra);
            thead.append(trh1);
            table.append(thead);
            document.getElementById('contenedor_informe_cajeros').append(table);
            //tot_parcial+=parseFloat(objetos[1][i].cantidad)
          } else {
            var tr = document.createElement('tr');var td1=document.createElement('td');var td2=document.createElement('td');var td3=document.createElement('td');var td4=document.createElement('td');tr.append(td1,td2,td3,td4);
            document.getElementById(divisa).append(tr);
            td1.innerText=objetos[1][i].precio_base;td2.innerText=objetos[1][i].cantidad;
            td3.innerText=ingreso_actualizacion;td4.innerText=egreso_actualizacion;
            tot_parcial+=parseFloat(objetos[1][i].cantidad)
            suma_ingresos+=parseFloat(ingreso_actualizacion);
            suma_egresos+=parseFloat(egreso_actualizacion);
            if (i==objetos[1].length-1) {
              document.getElementById('head'+divisa).innerText=divisa+' TOTAL: '+tot_parcial;
              document.getElementById('sum_ing'+divisa).innerText=divisa+' SUMA INGRESOS: '+suma_ingresos;
              document.getElementById('sum_egr'+divisa).innerText=divisa+' SUMA EGRESOS: '+suma_egresos;
            }
          }

        }
      }
    },
    error:function(){

    },
  });
}
function imprimir_nuevamente(element){
  $('#modal_footer_principal').html('');
  var obj_tr_div=JSON.parse(element.dataset.objeto);
  //console.log(obj_tr_div);
  $('#modal_principal').modal('show');
  $('#modal_body_principal').html('');
  var label=document.createElement('label');
  label.innerText='CONTINENTAL CAMBIOS\nCAMBIO DE MONEDA\nMoney Exchange\nTERMINAL TERRESTRE\nLocal 47 Piso 2 Ipiales-Nariño\nTel:7731277\nCel:3175736096\nNit: 1.085.901.197-9\n-------------------\nFecha:'+obj_tr_div['fecha_div']+' Hora:'+obj_tr_div['hora_div']+'\nCajero:'+obj_tr_div['nombre_cajero_div']+'\nTiquete N°:'+Date.now()+'\nCliente:'+obj_tr_div['nombre_cliente_div']+'\nOperación:'+obj_tr_div['tipo']+' Divisa: '+obj_tr_div['nombre_categoria']+'\n-------------------\nMonto:'+obj_tr_div['cantidad_div']+'\nCotizacion:'+obj_tr_div['precio_uni_div']+'\n-------------------\nPago Cliente:'+obj_tr_div['total_tra_div']+'\n'+obj_tr_div['nombre_cliente_div']+'\nVERIFIQUE SU DINERO\n ANTES DE SALIR DE VENTANILLA\nNO SE ACEPTAN RECLAMOS \nDESPUÉS DE RETIRARSE\n GRACIAS POR PEFERIRNOS';
  $('#modal_body_principal').append(label);
  var btn_imprimir=document.createElement('button');btn_imprimir.className='btn btn-success';btn_imprimir.innerText='Imprimir';
  $('#modal_footer_principal').append(btn_imprimir);
  btn_imprimir.onclick=function(){
    $('#modal_body_principal_2').printThis();
  }
}
function eliminar_transaccion_divisa (element){
  //alert(element.dataset.orden);
  $.ajax({
    url:"Transacciones/eliminar_transaccion_divisa",
    type:'Post',
    //data:{fecha:fecha,fecha2:fecha2},
    data:{orden:element.dataset.orden,objeto:JSON.parse(element.dataset.objeto)},
    success:function(respuesta){
      if (respuesta=='true') {
        alert('ne elimino');

      } else {
        alert('Se elimino');
        report_div()
      }
    },
    error:function(){

    },
  });
}
function imprimir_informe_global(){
  $('#h4_divisas, #contenedor_informe_divisas, #todo_varios, #todo_bancos, #todo_gran_total, #gran_total_global').printThis();
}
function report_cajeros_general(){
  $('#modal_informe_cajeros_general').modal('show');
  $.ajax({
    url:"Divisas/cajeros",
    type:"Post",
    data:{},
    success:function(respuesta){
      var cajeros=JSON.parse(respuesta);
      var sel=document.getElementById('cajeros_select_modal_general');
      sel.innerHTML='';
      var def=document.createElement('option');def.innerText='Seleccione Cajero';def.value="";
      sel.append(def);
      for (var i = 0; i < cajeros.length; i++) {
        var opt=document.createElement('option');
        opt.value=cajeros[i].cedula;
        opt.innerText=cajeros[i].nombre+' '+cajeros[i].apellido;
        sel.append(opt);
      }
    },
  });
}
function mostar_informe_cajero_general(){
  var obj=new Object()
  var hoy = new Date();
  //obj.fecha = hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  //obj.cedula=document.getElementById('cedula_cajero_login').innerText;
  //obj.cedula = 87718975;
  //obj.fecha=document.getElementById('cajeros_fecha_input_general').value;
  obj.cajero=document.getElementById('cajeros_select_modal_general').value;
  $.ajax({
    url:"Divisas/cajeros_general_transacciones",
    type:"Post",
    data:{obj:obj},
    success:function(respuesta){
      document.getElementById('contenedor_informe_cajeros_general').innerHTML=''
      var objts=JSON.parse(respuesta)
      var total=0
      for (var i = 0; i < objts.length; i++) {
        total+=parseInt(objts[i].total_tra_gen);
        var tr=document.createElement('tr');
        var td1=document.createElement('td');td1.innerText=objts[i].nombre_gen
        var td2=document.createElement('td');td2.innerText=objts[i].nombre_general
        var td3=document.createElement('td');td3.innerText=objts[i].precio_uni_gen
        var td4=document.createElement('td');td4.innerText=objts[i].cantidad_gen
        var td5=document.createElement('td');td5.innerText=objts[i].total_tra_gen
        tr.append(td1,td2,td3,td4,td5);
        document.getElementById('contenedor_informe_cajeros_general').append(tr);
      }
      var trf=document.createElement('tr');
      var tdf=document.createElement('td');tdf.colSpan='5';tdf.innerText='TOTAL: '+total;
      trf.append(tdf);
      document.getElementById('contenedor_informe_cajeros_general').append(trf);
    },
  });
}
function report_cajeros_seguros(){
  $('#modal_informe_cajeros_seguros').modal('show');
  $.ajax({
    url:"Divisas/cajeros",
    type:"Post",
    data:{},
    success:function(respuesta){
      var cajeros=JSON.parse(respuesta);
      var sel=document.getElementById('cajeros_select_modal_seguros');
      sel.innerHTML='';
      var def=document.createElement('option');def.innerText='Seleccione Cajero';def.value="";
      sel.append(def);
      for (var i = 0; i < cajeros.length; i++) {
        var opt=document.createElement('option');
        opt.value=cajeros[i].cedula;
        opt.innerText=cajeros[i].nombre+' '+cajeros[i].apellido;
        sel.append(opt);
      }
    },
  });
}
function mostar_informe_cajero_seguros(){
  var obj=new Object()
  var hoy = new Date();
  //obj.fecha = hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  //obj.cedula=document.getElementById('cedula_cajero_login').innerText;
  //obj.cedula = 87718975;
  //obj.fecha=document.getElementById('cajeros_fecha_input_general').value;
  obj.cajero=document.getElementById('cajeros_select_modal_seguros').value;
  $.ajax({
    url:"Divisas/cajeros_seguros_transacciones",
    type:"Post",
    data:{obj:obj},
    success:function(respuesta){
      document.getElementById('contenedor_informe_cajeros_seguros').innerHTML=''
      var objts=JSON.parse(respuesta)
      //console.log(objts)
      var total=0
      for (var i = 0; i < objts.length; i++) {
        total+=parseInt(objts[i].total_tra_seg);
        var tr=document.createElement('tr');
        var td1=document.createElement('td');td1.innerText=objts[i].nombre_seg
        var td2=document.createElement('td');td2.innerText=objts[i].nombre_seguros
        var td3=document.createElement('td');td3.innerText=objts[i].precio_uni_seg
        var td4=document.createElement('td');td4.innerText=objts[i].placa
        var td5=document.createElement('td');td5.innerText=objts[i].total_tra_seg
        var td6=document.createElement('td');td6.innerText=objts[i].estado
        tr.append(td1,td2,td3,td4,td5,td6);
        document.getElementById('contenedor_informe_cajeros_seguros').append(tr);
      }
      var trf=document.createElement('tr');
      var tdf=document.createElement('td');tdf.colSpan='5';tdf.innerText='TOTAL: '+total;
      trf.append(tdf);
      document.getElementById('contenedor_informe_cajeros_seguros').append(trf);
    },
  });
}
function prueba_formulario_18_rep(element){
  //console.log(element.dataset.info);
  var factura = JSON.parse(element.dataset.objeto);
  $('#modal_formulario_18_rep').modal('show');
  document.getElementById('fecha_hoy_18_rep').innerText=factura.fecha_div;
  document.getElementById('identificacion_cliente_18_rep').innerText=factura.ced_cliente_div;
  document.getElementById('nombre_cliente_18_rep').innerText=factura.nombre_cliente_div;
  document.getElementById('tasa_cambio_18_rep').innerText=factura.precio_uni_div;
  document.getElementById('monto_negociado_18_rep').innerText=factura.cantidad_div;
  document.getElementById('moneda_negociada_18_rep').innerText=factura.nombre_categoria;
  document.getElementById('total_18_rep').innerText=factura.total_tra_div;
  document.getElementById('input_efectivo_18_rep').value=factura.total_tra_div;

}
function imprimir_formulario_18_rep(){
  $('#modal_body_formulario_18_rep').printThis();
}
function formulario_2_rep(element){
  var factura = JSON.parse(element.dataset.objeto);
  $('#modal_formulario_2_rep').modal('show');
  //var factura = JSON.parse(element.dataset.info);
  var nombre=factura.nombre_cliente_div.split(' ');
  //console.log(nombre);
  document.getElementById('fecha_hoy_536_rep').innerText=factura.fecha_div;
  document.getElementById('cedula_cliente_536_rep').innerText=factura.ced_cliente_div;
  if (nombre.length==2) {
    document.getElementById('primer_apellido_536_rep').innerText=nombre[1];
    document.getElementById('segundo_appellido_536_rep').innerText='';
    document.getElementById('segundo_nombre_536_rep').innerText='';
  } else {
    if (nombre.length==3) {
      document.getElementById('primer_apellido_536_rep').innerText=nombre[nombre.length-2];
      document.getElementById('segundo_appellido_536_rep').innerText=nombre[nombre.length-1];
      document.getElementById('segundo_nombre_536_rep').innerText='';
      //
    } else {
      document.getElementById('primer_apellido_536_rep').innerText=nombre[nombre.length-2];
      document.getElementById('segundo_appellido_536_rep').innerText=nombre[nombre.length-1];
      var otros_n=''
      for (var i = 1; i < nombre.length-2; i++) {
        otros_n+=nombre[i]+' '
      }
      document.getElementById('segundo_nombre_536_rep').innerText=otros_n;
    }
  }
  document.getElementById('primer_nombre_536_rep').innerText=nombre[0];
  document.getElementById('monto_negociado_536_rep').innerText=factura.cantidad_div;
  document.getElementById('moneda_negociada_536_rep').innerText=factura.nombre_categoria;
  document.getElementById('valor_pesos_536_rep').innerText=factura.total_tra_div;
  var hoy_año=new Date();
  document.getElementById('año_536_rep').innerText=hoy_año.getFullYear();
}
function imprimir_formulario_536_rep(){
  $('#modal_body_formulario_2_rep').printThis();

}
function transferencia_divisas_cajero(){
  var hoy =new Date();
  //var fecha_hoy=''
  if (hoy.getMonth()+1==1||hoy.getMonth()+1==2||hoy.getMonth()+1==3||hoy.getMonth()+1==4||hoy.getMonth()+1==5||hoy.getMonth()+1==6||hoy.getMonth()+1==7||hoy.getMonth()+1==8||hoy.getMonth()+1==9) {
    var fecha_hoy=hoy.getFullYear()+'-0'+(hoy.getMonth()+1)+'-';//+hoy.getDate();
  } else {
    var fecha_hoy=hoy.getFullYear()+'-'+(hoy.getMonth()+1)+'-';//+hoy.getDate();
  }
  if (hoy.getDate()==1||hoy.getDate()==2||hoy.getDate()==3||hoy.getDate()==4||hoy.getDate()==5||hoy.getDate()==6||hoy.getDate()==7||hoy.getDate()==8||hoy.getDate()==9) {
    fecha_hoy+='0'+hoy.getDate();
  } else {
    fecha_hoy+=hoy.getDate();
  }
  var tables=document.getElementById('contenedor_informe_cajeros').getElementsByTagName('table')
  var pesos=/[0-9]+/.exec(tables[0].innerText);
  //console.log(pesos[0]);
  var fecha=document.getElementById('cajeros_fecha_input').value
  var cajero=document.getElementById('cajeros_select_modal').value
  //console.log(fecha+'**'+fecha_hoy)
  if (fecha_hoy!=fecha) {
    alert('NO SE PUEDE HACER LA TRANSFERENCIA PARA FECHA DIFERENTES A HOY')
    return;
  }
  var r = confirm('Confirmar para realizar transferencia');
  if (r==false) {
    return;
  }
  $.ajax({
    url:"Divisas/informe_cajero",
    type:"Post",
    data:{cajero:cajero,fecha:fecha},
    success:function(respuesta){
      var objs=[];
      var datos=JSON.parse(respuesta);
      var obj=new Object();
      for (var i = 0; i < datos[1].length; i++) {
        var obj2=new Object();
        if (datos[1][i].divisa!=1) {
          obj2.cantidad=datos[1][i].cantidad;
        } else {
          obj2.cantidad=pesos[0];
        }
        obj2.divisa=datos[1][i].divisa;
        obj2.nombre_divisa=datos[1][i].nombre_categoria;
        obj2.precio=datos[1][i].precio_base;
        objs.push(obj2);
      }
      $.ajax({
        url:"Divisas/transacion_inventario_global",
        type:"Post",
        data:{objs:objs},
        success:function(respuesta){
          if (respuesta=='true') {
            alert('se modifico el inventario global')
            reiniciar_base_cajero();
          } else {
            if (respuesta=='false') {
              alert('no se modifico')
            } else {
              var opt=JSON.parse(respuesta);
              alert('no se encontro la divisa '+opt.nombre_divisa+' con un precio base de '+opt.precio);
            }

          }
        },
      });
      //console.log(datos[1]);
    },
  });
}
function reiniciar_base_cajero(){
  var cajero = document.getElementById('cajeros_select_modal').value
  $.ajax({
    url:"Divisas/reiniciar_base_cajero",
    type:"Post",
    data:{cajero:cajero},
    success:function(respuesta){
      if (respuesta=='true') {
        alert('se reinicia cajero')
      } else {
        if (respuesta=='false') {
          alert('No se reinicio')
        } else {
          var opt=JSON.parse(respuesta);
          //alert('no se encontro la divisa '+opt.nombre_divisa+' con un precio base de '+opt.precio);
          alert(opt);
        }

      }
    },
  });
}
function report_general_dia_anterior(){
  var hoy = new Date();
  var fecha = hoy.getFullYear()+'-'+(hoy.getMonth()+1)+'-'+hoy.getDate();
  $.ajax({

    url:"Transacciones/consultar_transacciones_divisas_dia",
    type:'Post',
    data:{hoy:fecha},
    success:function(respuesta){
      //
      $('#modal_informe_diario').modal('show');
      //console.log(respuesta);
      var datos=JSON.parse(respuesta);
      //console.log(datos)
      var suma_ventas=0;
      var suma_compras=0;
      var divisa='';
      console.log(datos);      
      if (datos[0]!='false') {
        
        var tabla1=document.getElementById('tabla_informe_dia_compra');
        tabla1.innerHTML='';
        divisa=datos[0][0].nombre_categoria;
        //console.log(datos[0])
        for(var i = 0;i<datos[0].length;i++){
          //console.log(datos[0][i])
          //alert(divisa)
          if (i<datos[0].length-1 && divisa==datos[0][i].nombre_categoria) {
            suma_compras += parseFloat(datos[0][i].cantidad_div)
          } else {
            //alert(divisa)
            let trc=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
            trc.append(td1,td2);
            td1.innerText=divisa;td2.innerText=suma_compras
            tabla1.append(trc);
            divisa=datos[0][i].nombre_categoria;
            suma_compras=0;
            if (i<datos[0].length-1 && datos[0][i+1].nombre_categoria!=divisa ) {
              suma_compras += parseFloat(datos[0][i].cantidad_div)                
            }
          }
          if (i==datos[0].length-1) {
            var ultimo =tabla1.lastChild;
            //console.log(ultimo.lastChild.innerText)
            //console.log(datos[0][i].cantidad_div)
            if (ultimo.firstChild.innerText==datos[0][i].nombre_categoria) {
              //let trv=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
              //trv.append(td1,td2);
              //td1.innerText=divisa;td2.innerText=suma_ventas
              //tabla2.append(trv);
              ultimo.lastChild.innerText=parseFloat(ultimo.lastChild.innerText)+parseFloat(datos[0][i].cantidad_div);
            }else{
              suma_compras += parseFloat(datos[0][i].cantidad_div)
              let trc=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
              trc.append(td1,td2);
              td1.innerText=divisa;td2.innerText=suma_compras
              tabla1.append(trc);
            }
          }
        }        
      }
      if (datos[1]!='false') {
        var tabla2=document.getElementById('tabla_informe_dia_venta');
        tabla2.innerHTML='';
        divisa=datos[1][0].nombre_categoria
        for(var i = 0;i<datos[1].length;i++){
          if (i<datos[1].length-1 && divisa==datos[1][i].nombre_categoria) {
            
              suma_ventas += parseFloat(datos[1][i].total_tra_div)
                        
          } else {
              //console.log(datos[1][i])         
              let trv=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
              trv.append(td1,td2);
              td1.innerText=divisa;td2.innerText=suma_ventas
              tabla2.append(trv);
              divisa=datos[1][i].nombre_categoria;
              suma_ventas=0;
              //alert(divisa)
              if (i<datos[1].length-1 && datos[1][i+1].nombre_categoria!=divisa) {
                suma_ventas += parseFloat(datos[1][i].total_tra_div)                
              }            
            
          }
          if (i==datos[1].length-1) {
            var ultimo =tabla2.lastChild;
            //console.log(ultimo.lastChild.innerText)
            //console.log(datos[1][i].total_tra_div)
            if (ultimo.firstChild.innerText==datos[1][i].nombre_categoria) {
              //let trv=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
              //trv.append(td1,td2);
              //td1.innerText=divisa;td2.innerText=suma_ventas
              //tabla2.append(trv);
              ultimo.lastChild.innerText=parseFloat(ultimo.lastChild.innerText)+parseFloat(datos[1][i].total_tra_div);
            }else{
              suma_ventas += parseFloat(datos[1][i].total_tra_div)
              let trv=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
              trv.append(td1,td2);
              td1.innerText=divisa;td2.innerText=suma_ventas
              tabla2.append(trv);
            }
          }
        }
      }
      
      
    },
    error:function(){

    },
  });
}
function report_general_dia(){
  var hoy = new Date();
  var fecha = hoy.getFullYear()+'-'+(hoy.getMonth()+1)+'-'+hoy.getDate();
  $.ajax({

    url:"Transacciones/consultar_transacciones_divisas_dia",
    type:'Post',
    data:{hoy:fecha,tipo:'compra'},
    success:function(respuesta){
      $('#modal_informe_diario').modal('show');
      var datos_compras=JSON.parse(respuesta);      
      var tabla1=document.getElementById('tabla_informe_dia_compra');
      tabla1.innerHTML='';
      for (let i = 0; i < datos_compras.length; i++) {
        var suma_compras=0;
        if (datos_compras[i]!='false') {
          //console.log(datos[i]+'--')
          for (let j = 0; j < datos_compras[i].length; j++) {
            suma_compras += parseFloat(datos_compras[i][j].cantidad_div)             
          }
          let trc=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
          trc.append(td1,td2);
          td1.innerText=datos_compras[i][0].nombre_categoria;td2.innerText=suma_compras
          tabla1.append(trc);
           
        }
      }
      
    },
    error:function(error){
      alert('ocurrio un error en informe generla de ventas y compras')
      console.log(error)
    },
  });
  $.ajax({

    url:"Transacciones/consultar_transacciones_divisas_dia",
    type:'Post',
    data:{hoy:fecha,tipo:'venta'},
    success:function(respuesta){
      var datos_ventas=JSON.parse(respuesta);      
      var tabla2=document.getElementById('tabla_informe_dia_venta');
      tabla2.innerHTML='';
      for (let i = 0; i < datos_ventas.length; i++) {
        var suma_ventas=0;
        if (datos_ventas[i]!='false') {
          //console.log(datos[i]+'--')
          for (let j = 0; j < datos_ventas[i].length; j++) {
            suma_ventas += parseFloat(datos_ventas[i][j].total_tra_div)             
          }
          let trv=document.createElement('tr');let td1=document.createElement('td');let td2=document.createElement('td');
          trv.append(td1,td2);
          td1.innerText=datos_ventas[i][0].nombre_categoria;td2.innerText=suma_ventas
          tabla2.append(trv);
           
        }
      }
    },
    error:function(error){
      alert('ocurrio un error en informe generla de ventas y compras')
      console.log(error)
    },
  });
}