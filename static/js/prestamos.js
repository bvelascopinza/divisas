$(document).ready(prestamo);
function prestamo(){
  document.getElementById('titulo_principal').innerText='PRESTAMOS';
  var prestamo_view = document.getElementById('div_prestamos')
  prestamo_view.style.display='inline';
  document.getElementById('contenedor_principal_divisas').append(prestamo_view);
  cargar_divisas();
  cargar_prestamos();
}

function cargar_divisas(){
  $.ajax({
        url:"categorias_divisas",
        type:'Post',
        data:{},
        beforeSend:function(){
          //
        },
        success:function(respuesta){
          var categorias=JSON.parse(respuesta);
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_categoria;
              opt.value=categorias[i].id_categoria;
              //opt.value=JSON.stringify(categorias[i]);
              opt.setAttribute('data-nombre',categorias[i].nombre_categoria);
              if (categorias[i].nombre_categoria=='PESO') {
                document.getElementById('sel_categoria_origen').append(opt);
              }
            }

        },

        error:function(){

        },

    });
}
function cargar_prestamos(){
  var total_global=0;
  $.ajax({
        url:"cargar_prestamos",
        type:'Post',
        data:{},
        beforeSend:function(){
          //
        },
        success:function(respuesta){
          document.getElementById('body_tabla').innerHTML='';
          var objs=JSON.parse(respuesta);
          console.log(objs)
            for (var i = 0; i < objs.length; i++) {
              var tr=document.createElement('tr');
              var td1=document.createElement('th');var td2=document.createElement('th');var td3=document.createElement('th');var td4=document.createElement('th');var td5=document.createElement('th');var td6=document.createElement('th');  var td7=document.createElement('th');
              var btn1=document.createElement('button');var btn2=document.createElement('button');
              var inpt_abono=document.createElement('input');inpt_abono.className='form-control';inpt_abono.value=objs[i].abono;inpt_abono.setAttribute('data-info',JSON.stringify(objs[i]));
              inpt_abono.onkeypress=function(){
                actualizar_abono(this,event);
              }
              var estado=document.createElement('span');
              if (objs[i].estado=='A') {
                estado.className='badge badge-success';
              } else {
                estado.className='badge badge-danger';
              }
              estado.innerText=objs[i].estado;
              btn1.className='btn btn-info fas fa-pencil-alt';
              td1.innerText=objs[i].fecha;
              td2.innerText=objs[i].nombre_prestamo;
              td3.innerText=objs[i].cedula_prestamo;
              td4.append(estado);
              td5.innerText=new Intl.NumberFormat().format(objs[i].cantidad_prestamo-objs[i].abono);
              total_global=total_global+(objs[i].cantidad_prestamo-objs[i].abono);
              //td6.innerText=objs[i].abono;
              td6.append(inpt_abono);
              td7.append(btn1)
              tr.append(td1,td2,td3,td4,td5,td6,td7);
              btn1.setAttribute('data-info',JSON.stringify(objs[i]));
              document.getElementById('body_tabla').append(tr);
              btn1.onclick=function(){
                actualizar_estado_prestamo(this);
              }
            }
            document.getElementById('total_prestamos').innerText=new Intl.NumberFormat().format(total_global);
        },

        error:function(){

        },

    });
}
function insertar_prestamo(){
  //var sel=JSON.parse(document.getElementById('sel_categoria_origen').value);
  var hoy = new Date();
  if (document.getElementById('input_nombre').value==""||document.getElementById('input_cedula').value==""||document.getElementById('input_cantidad').value==""||document.getElementById('sel_categoria_origen').value=="") {
    alert('no pueden existir valores vacios para porder registrar el prestamo')
  } else {
    var objeto=new Object();
    objeto.fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
    objeto.nombre=document.getElementById('input_nombre').value;
    objeto.cedula=document.getElementById('input_cedula').value;
    objeto.cantidad=document.getElementById('input_cantidad').value;
    objeto.divisa=document.getElementById('sel_categoria_origen').value
    objeto.abono=0;
    $.ajax({
          url:"insertar_prestamo",
          type:'Post',
          data:{objeto:objeto},
          beforeSend:function(){
            //
          },
          success:function(respuesta){
            if (respuesta=='true') {
              alert('el prestamo se guardo correctamente');
              cargar_prestamos();
            } else {
              alert('NO se guardo');
            }
          },

          error:function(){
            alert('algo fallo en el prestamo');
          },

      });
  }

}
function actualizar_estado_prestamo(elemento){
  var obj=JSON.parse(elemento.dataset.info);
  console.log(obj);
  if (obj.estado=='A') {
    obj.estado='C'
  } else {
    obj.estado='A'
  }
  $.ajax({
        url:"actualizar_prestamo",
        type:'Post',
        data:{objeto:obj},
        beforeSend:function(){
          //
        },
        success:function(respuesta){
          if (respuesta=='true') {
            cargar_prestamos();
            alert('Actualizado...');
          } else {
            alert('NO se actualizo');
          }
        },

        error:function(){
          alert('algo fallo en actualizar');
        },

    });
}
function actualizar_abono(element,event){
  console.log(element.dataset.info);
  var codigo = event.which || event.keyCode;
  var obj=JSON.parse(element.dataset.info);
  obj.abono=element.value;
  if(codigo===13){
    var r = confirm('Confirmar para actualizar abono');
    if (r==true) {
      $.ajax({
            url:"actualizar_abono",
            type:'Post',
            data:{objeto:obj},
            beforeSend:function(){
              //
            },
            success:function(respuesta){
              if (respuesta=='true') {
                cargar_prestamos();
                alert('Actualizado...');
              } else {
                alert('NO se actualizo');
              }
            },

            error:function(){
              alert('algo fallo en actualizar');
            },

        });
    }
  }
}
