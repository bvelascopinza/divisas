$(document).ready(caja);
var tot_general=0;
function caja(){




  document.getElementById('titulo_principal').innerText='CAJA Y GESTION DE PROCESOS';
  var caja = document.getElementById('div_caja')
  caja.style.display='inline'
  document.getElementById('contenedor_principal_divisas').append(caja);


}
function enter_press(event){
  tot_general=0;
  var codigo = event.which || event.keyCode;
  if(codigo===13){
    var cedula=document.getElementById('input_cedula').value
    consultar_cedula(cedula)
  }
}
function consultar_cedula(cedula){
  $.ajax({
        url:$("#url").val()+"Divisas/cliente_unico",
        type:'Post',
        data:{cedula:cedula},
        beforeSend:function(){
        	//
        },
        success:function(respuesta){
          console.log(respuesta)
          var btn_guardar=document.createElement('button');
          btn_guardar.className='btn btn-primary';
          btn_guardar.innerText='Guardar Cliente';
          btn_guardar.onclick=function(){
            guardar_nuevo_cliente();
          };
          if (respuesta=='false') {
            alert('No existe el cliente ingresado');
            document.getElementById('div_complemento_caja').innerHTML='';
            for (var i = 0; i < 5; i++) {
              var div1=document.createElement('div');
              div1.className="col-sm-6";
              var div2=document.createElement('div');
              div2.className="col-sm-6";
              var div=document.createElement('div');
              div.className="row";
              var label=document.createElement('span');
              label.className='badge badge-info'
              var input=document.createElement('input');
              switch (i) {
                case 0:
                  label.innerText='Documento Identidad'
                  input.type='number';
                  input.id='doc_identidad';
                  input.value=document.getElementById('input_cedula').value;
                  input.setAttribute('placeholder','e: 1083702030');
                  break;
                case 1:
                  label.innerText='Nombre'
                  input.type="text"
                  input.id='nombre';
                  input.setAttribute('placeholder','ej: benito juarez');
                  break;
                case 2:
                  label.innerText='Teléfono'
                  input.type='number'
                  input.id='telefono';
                  input.setAttribute('placeholder','ej:7200000');
                  break;
                case 3:
                  label.innerText='Pais Origen'
                  input.id='pais';
                  input.setAttribute('placeholder','ej: Colombia');
                  break;
                case 4:
                  label.innerText='correo'
                  input.type="email";
                  input.id='email';
                  input.setAttribute('placeholder','ej: CORREO@gmail.com');
                  break;
              }
              div1.append(label);
              div2.append(input)
              div.append(div1,div2);
              //document.getElementById('div_complemento_caja').append(label1,input1,label2,input2,label3,input3,label4,input4);
              document.getElementById('div_complemento_caja').append(div);
            }
            document.getElementById('div_complemento_caja').append(btn_guardar);
          } else {
            document.getElementById('div_divisas').style.display='inline'
            document.getElementById('div_complemento_caja').innerHTML=''
            var usuario=JSON.parse(respuesta);
            console.log(usuario)
            var h4=document.createElement('h4');
            var label = document.createElement('span');
            label.className='badge badge-info';
            label.innerText=usuario[0].nombre;
            //opt.value=categorias[0].id_categoria;
            h4.append(label);
            document.getElementById('cliente_nombre').innerHTML='';
            document.getElementById('cliente_nombre').append(h4);
          }


        },

        error:function(){

        },

    });
}
function cargar_segunda_categoria(){
  tot_general=0;
  document.getElementById('div_complemento_caja').innerHTML='';
  document.getElementById('contenedor_botones').innerHTML=''
  var valor = document.getElementById('sel_principal').value
  document.getElementById('sel_categoria_origen').innerHTML='';
  document.getElementById('sel_categoria_destino').innerHTML='';
  //alert(valor);
  if (valor==1) {
    //alert('DIVISAS')
    $.ajax({
          url:$("#url").val()+"Divisas/categorias_divisas",
          type:'Post',
          data:{},
          beforeSend:function(){
          	//
          },
          success:function(respuesta){
              var categorias=JSON.parse(respuesta);
              var def=document.createElement('option');
              def.innerText='Seleccione divisa 1';
              document.getElementById('sel_categoria_origen').append(def);
              //var opt = document.createElement('option');

              for (var i = 0; i < categorias.length; i++) {
                var opt = document.createElement('option');
                opt.innerText=categorias[i].nombre_categoria;
                opt.value=categorias[i].id_categoria;
                opt.setAttribute('data-nombre',categorias[i].nombre_categoria);
                if (categorias[i].nombre_categoria!='PESO') {
                  document.getElementById('sel_categoria_origen').append(opt);
                }
              }
              document.getElementById('sel_categoria_origen').onchange=function(){
                //alert('sas')
                consultar_categoria_menos();
                if (document.getElementById('input_cantidad').value!='') {
                  var div_botones=document.getElementById('contenedor_botones');
                  div_botones.style.display='inline';
                  div_botones.innerHTML='';
                  var btn1= document.createElement('button');
                  var btn2= document.createElement('button');
                  var icon1= document.createElement('i');
                  var icon2= document.createElement('i');
                  btn1.className='btn btn-info';btn2.className='btn btn-success';
                  icon1.className='fas fa-money-check-alt';icon2.className='fas fa-money-check-alt';
                  btn1.append(icon1);btn2.append(icon2);
                  icon1.innerText='COMPRA';icon2.innerText='VENTA';
                  var valor = document.getElementById('sel_principal').value
                   if (valor==1) {
                     var div_1 = document.getElementById('sel_categoria_origen').value;
                     var div_2 = document.getElementById('sel_categoria_destino').value;
                     div_botones.append(btn1,btn2)
                     btn1.onclick=function(){
                       consultar_datos_compra(div_1,div_2);
                     }
                     btn2.onclick=function(){
                       consultar_datos_venta(div_1,div_2);
                     }
                   } else {
                     if (valor==2) {
                       div_botones.append(btn2)
                       btn2.onclick=function(){
                         //alert(div_2);
                         consultar_datos_seguros();
                       }
                     } else {
                       if (valor==3) {
                         div_botones.append(btn2)
                         btn2.onclick=function(){
                           //alert(div_2);
                           consultar_datos_general();
                         }
                       } else {

                       }
                     }
                   }
                }
                //
              }

          },

          error:function(){

          },

      });
  } else {
    if (valor==2) {
      //alert('seguros')
      document.getElementById('sel_categoria_origen').innerHTML='';
      var def=document.createElement('option');
      def.innerText='Seleccione sub-categoria';
      document.getElementById('sel_categoria_origen').append(def);
      $.ajax({
            url:$("#url").val()+"Divisas/categorias_seguros",
            type:'Post',
            data:{},
            beforeSend:function(){
            	//
            },
            success:function(respuesta){
                var categorias=JSON.parse(respuesta);
                //var opt = document.createElement('option');

                for (var i = 0; i < categorias.length; i++) {
                  var opt = document.createElement('option');
                  opt.innerText=categorias[i].nombre_seguros;
                  opt.value=categorias[i].id_seguros;
                  //opt.setAttribute('data-ninfo',JSON.stringify(categorias[i]));
                  document.getElementById('sel_categoria_origen').append(opt);
                }
                document.getElementById('sel_categoria_origen').onchange=function(){
                  //alert('sas')
                  consultar_seguros();
                }

            },

            error:function(){

            },

        });
    } else {
      if (valor==3) {
        document.getElementById('sel_categoria_origen').innerHTML='';
        var def=document.createElement('option');
        def.innerText='Seleccione sub-categoria';
        document.getElementById('sel_categoria_origen').append(def);
        $.ajax({
              url:$("#url").val()+"Divisas/categorias_general",
              type:'Post',
              data:{},
              beforeSend:function(){
              	//
              },
              success:function(respuesta){
                  var categorias=JSON.parse(respuesta);
                  //var opt = document.createElement('option');

                  for (var i = 0; i < categorias.length; i++) {
                    var opt = document.createElement('option');
                    opt.innerText=categorias[i].nombre_general;
                    opt.value=categorias[i].id_general;
                    opt.setAttribute('data-nombre',categorias[i].nombre_general);
                    document.getElementById('sel_categoria_origen').append(opt);
                  }
                  document.getElementById('sel_categoria_origen').onchange=function(){
                    //alert('sas')
                    consultar_general();
                  }

              },

              error:function(){

              },

          });
      } else {

      }
    }
  }
}


function consultar_categoria_menos(){
  document.getElementById('sel_categoria_destino').innerHTML=''
  $.ajax({
        url:$("#url").val()+"Divisas/categorias_menos",
        type:'Post',
        data:{option:document.getElementById('sel_categoria_origen').value},
        beforeSend:function(){
        	//
        },
        success:function(respuesta){
            var categorias=JSON.parse(respuesta);
            //var def=document.createElement('option');
            //def.innerText='selecciona divisa 2'
            document.getElementById('sel_categoria_destino').innerHTML=''
            //document.getElementById('sel_categoria_destino').append(def);
            //var opt = document.createElement('option');
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_categoria;
              opt.setAttribute('data-nombre',categorias[i].nombre_categoria);
              opt.value=categorias[i].id_categoria;
              if (categorias[i].nombre_categoria=='PESO') {
                document.getElementById('sel_categoria_destino').append(opt);
              }
            }


            //text.innerHTML=respuesta;
            //alert('se guardaron datos principales del auto')

        },

        error:function(){
            //$("#texto").html("pailas");
            //text.innerHTML='error';
        },

    });
}
//
function consultar_seguros(){
  document.getElementById('sel_categoria_destino').innerHTML=''
  $.ajax({
        url:$("#url").val()+"Divisas/seguros_objetos",
        type:'Post',
        data:{categoria:document.getElementById('sel_categoria_origen').value},
        beforeSend:function(){
          //
        },
        success:function(respuesta){
          //console.log(respuesta);
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');
            def.innerText='selecciona un seguro'
            document.getElementById('sel_categoria_destino').innerHTML=''
            document.getElementById('sel_categoria_destino').append(def);
            //var opt = document.createElement('option');
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');

              opt.innerText=categorias[i].nombre_objeto_seg+' -> '+categorias[i].precio_obj_venta;
              //opt.setAttribute('data-info',JSON.stringify(categorias[i]));
              opt.value=JSON.stringify(categorias[i]);
              //console.log(opt);


              document.getElementById('sel_categoria_destino').append(opt);
            }


            //text.innerHTML=respuesta;
            //alert('se guardaron datos principales del auto')

        },

        error:function(){
            //$("#texto").html("pailas");
            //text.innerHTML='error';
        },

    });
}
//
function consultar_general(){
  document.getElementById('sel_categoria_destino').innerHTML=''
  $.ajax({
        url:$("#url").val()+"Divisas/general_objetos",
        type:'Post',
        data:{option:document.getElementById('sel_categoria_origen').value},
        beforeSend:function(){
          //
        },
        success:function(respuesta){
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');
            def.innerText='selecciona un seguro'
            document.getElementById('sel_categoria_destino').innerHTML=''
            document.getElementById('sel_categoria_destino').append(def);
            //var opt = document.createElement('option');
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_objeto_gen+' -> '+categorias[i].precio_obj_gen_venta;
              opt.setAttribute('data-nombre',categorias[i].nombre_objeto_gen);
              //opt.value=categorias[i].id_objeto_gen;
              opt.value=JSON.stringify(categorias[i]);


              document.getElementById('sel_categoria_destino').append(opt);
            }


            //text.innerHTML=respuesta;
            //alert('se guardaron datos principales del auto')

        },

        error:function(){
            //$("#texto").html("pailas");
            //text.innerHTML='error';
        },

    });
}
////
function guardar_nuevo_cliente(){
  var cliente=new Object();
  cliente.doc=document.getElementById('doc_identidad').value;
  cliente.nombre=document.getElementById('nombre').value;
  cliente.telefono=document.getElementById('telefono').value;
  cliente.pais=document.getElementById('pais').value;
  cliente.email=document.getElementById('email').value;
  $.ajax({
        url:$("#url").val()+"Divisas/nuevo_cliente",
        type:'Post',
        data:{objeto:cliente},
        success:function(respuesta){
          if (respuesta=='false') {
            alert('el usuario ya existe ');
          } else {
            //document.getElementById('alert').append('se guardo el cliente');
            alert('se guardo el nuevo cliente');
            consultar_cedula(cliente.doc);
          }
        },

        error:function(){

        },

    });
}
//
function mostrar_botones_necesarios(event){
  var codigo = event.which || event.keyCode;
  if(codigo===13){
     var div_botones=document.getElementById('contenedor_botones');
     div_botones.style.display='inline';
     div_botones.innerHTML='';
     var btn1= document.createElement('button');
     var btn2= document.createElement('button');
     var icon1= document.createElement('i');
     var icon2= document.createElement('i');
     btn1.className='btn btn-info';btn2.className='btn btn-success';
     icon1.className='fas fa-money-check-alt';icon2.className='fas fa-money-check-alt';
     btn1.append(icon1);btn2.append(icon2);
     icon1.innerText='COMPRA';icon2.innerText='VENTA';
     var valor = document.getElementById('sel_principal').value
      if (valor==1) {
        var div_1 = document.getElementById('sel_categoria_origen').value;
        var div_2 = document.getElementById('sel_categoria_destino').value;
        div_botones.append(btn1,btn2)
        btn1.onclick=function(){
          consultar_datos_compra(div_1,div_2);
        }
        btn2.onclick=function(){
          consultar_datos_venta(div_1,div_2);
        }
      } else {
        if (valor==2) {
          div_botones.append(btn2)
          btn2.onclick=function(){
            //alert(div_2);
            consultar_datos_seguros();
          }
        } else {
          if (valor==3) {
            div_botones.append(btn2)
            btn2.onclick=function(){
              //alert(div_2);
              consultar_datos_general();
            }
          } else {

          }
        }
      }
  }

}
//
function consultar_datos_compra(div_1,div_2){
  $('#modal_footer_principal').html('');
  var hoy = new Date()
  var fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  var time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds();
  var cedula=document.getElementById('cedula_cajero_login').innerText;
  $.ajax({
        url:$("#url").val()+"Divisas/consultar_relacion",
        type:'Post',
        data:{div1:div_1,div2:div_2,fecha:fecha,cedula:cedula},
        success:function(respuesta){
          //alert(respuesta)
          if (respuesta=='') {
            alert('revise al relacion');
          } else {
            //alert('se guardo correctamente');
            var rel=JSON.parse(respuesta);

            console.log(rel);
            var sel = document.createElement('select');
            var opt0= document.createElement('option');opt0.innerText='Seleccione una opcion:';opt0.value='';sel.append(opt0);
            for (var i = 0; i < rel.length; i++) {
              var opt = document.createElement('option');
              opt.value=rel[i].precio_base;opt.innerText=rel[i].precio_base

              // var btn1 = document.createElement('button');
              // btn1.setAttribute('data-dismiss',"modal");
              // btn1.className='btn btn-success';
              // btn1.innerText='precio uno\n'+rel[i].precio_base;
              // btn1.onclick=function(){
              //   calcular_cambio_compra(rel[i].precio_base);
              // }
              //opt.append(btn1);
              sel.append(opt);
            }
            sel.onchange=function(){
              //alert(sel.value)
              calcular_cambio_compra(sel.value);
            }
            var span_pesos_maximos=document.createElement('span');span_pesos_maximos.className='badge badge-success';
            var cajero_max=document.getElementById('cedula_cajero_login').innerText
            var hoy_max = new Date();
            var fecha_max =hoy_max.getFullYear()+'/'+(hoy_max.getMonth()+1)+'/'+hoy_max.getDate();
            $.ajax({
              url:"informe_cajero",
              type:"Post",
              data:{cajero:cajero_max,fecha:fecha_max},
              success:function(respuesta){
                var objetos=JSON.parse(respuesta);
                var pesos_compra=0;var pesos_venta=0;
                for (var i = 0; i < objetos[0].length; i++) {
                  if(objetos[0][i].tipo=='compra'){
                    pesos_compra+=parseFloat(objetos[0][i].total_tra_div);
                  }
                  else{
                    pesos_venta+=parseFloat(objetos[0][i].cantidad_div);
                  }
                }
                for (var i = 0; i < objetos[1].length; i++) {
                  if (objetos[1][i].nombre_categoria=='PESO') {
                    span_pesos_maximos.innerText='MAXIMO PESOS: '+(parseFloat(objetos[1][i].cantidad)+pesos_venta-pesos_compra)
                  }
                }

              },
              error:function(){

              },
            });
            var div_nuevo=document.createElement('div');div_nuevo.className='col-sm-5';
            var label = document.createElement('span');label.innerText='VAlOR DIFERENTE';label.className='badge badge-info'
            var input1 = document.createElement('input');input1.placeholder='PRECIO COMPRA BASE';
            var input2 = document.createElement('input');input2.placeholder='VENTA 1';
            var input3 = document.createElement('input');input3.placeholder='VENTA 2';
            var input4 = document.createElement('input');input4.placeholder='VENTA 3';
            var btn_new=document.createElement('button');btn_new.className='btn btn-success';btn_new.innerText='Guardar nueva relacion'
            div_nuevo.append(label,input1,input2,input3,input4,btn_new);
            btn_new.onclick=function(){
              var inpts=document.getElementById('modal_body_principal').getElementsByTagName('input');
              //console.log(inpts);
              var objeto=new Object();
              objeto.base=inpts[0].value;              
              objeto.v1=inpts[1].value;
              objeto.v2=inpts[2].value;
              objeto.v3=inpts[3].value;
              objeto.time=time;
              objeto.fecha=fecha;
              objeto.cajero=document.getElementById('cedula_cajero_login').innerText;
              //objeto.cant_base=document.getElementById('input_cantidad').value.replace(/[,]/g, "");
              objeto.cant_base=0;
              objeto.destino=document.getElementById('sel_categoria_destino').value;
              objeto.origen=document.getElementById('sel_categoria_origen').value;
              if (parseFloat(objeto.v1)<=parseFloat(objeto.base) ||parseFloat(objeto.v2)<=parseFloat(objeto.base) ||parseFloat(objeto.v3)<=parseFloat(objeto.base) ) {
                alert('!VALIDA los valores ingresados en precios de venta, asegurate de que sean mayores que el precio de compra¡')
                return;
              }
              //console.log(parseFloat(objeto.v1))
              if(isNaN(parseFloat(objeto.v1)) || isNaN(parseFloat(objeto.v2)) || isNaN(parseFloat(objeto.v3))){
                alert('!VALIDA los valores ingresados en precios de venta, asegurate de que sean mayores que el precio de compra¡')
                return;
              }              
              $.ajax({
                url:"inv_agregar_relacion2",
                type:'Post',
                data:{objeto:objeto},
                success:function(respuesta){
                  //
                  if (respuesta=='true') {
                    alert('se guardo correctamente');
                  } else {

                  }
                },
                error:function(){

                },
              });
              calcular_cambio_compra(objeto.base);
            }
            // var btn2 = document.createElement('button');
            // btn2.setAttribute('data-dismiss',"modal");
            // btn2.className='btn btn-success';
            // btn2.innerText='precio dos\n'+rel[0].precio_compra2;
            // btn2.onclick=function(){
            //   calcular_cambio_compra(rel[0].precio_compra2);
            // }
            // var btn3 = document.createElement('button');
            // btn3.setAttribute('data-dismiss',"modal");
            // btn3.className='btn btn-success';
            // btn3.innerText='precio tres\n'+rel[0].precio_compra3;
            // btn3.onclick=function(){
            //   calcular_cambio_compra(rel[0].precio_compra3);
            // }
            $('#modal_principal').modal('show');
            $('#modal_body_principal').html('');
            document.getElementById('modal_title').innerHTML='';
            document.getElementById('modal_title').innerText='Compra de Divisas';
            $('#modal_body_principal').append(sel,div_nuevo,span_pesos_maximos);
          }
        },

        error:function(){

        },

    });
}
///
function consultar_datos_venta(div_1,div_2){
  $('#modal_footer_principal').html('');
  var hoy = new Date()
  var fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  var time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds();
  var cedula=document.getElementById('cedula_cajero_login').innerText;
  $.ajax({
        url:$("#url").val()+"Divisas/consultar_relacion2",
        type:'Post',
        data:{div1:div_1,div2:div_2,fecha:fecha,cedula:cedula},
        success:function(respuesta){
          //alert(respuesta)
          if (respuesta=='false') {
            alert('revise al relacion');
          } else {
            //alert('se guardo correctamente');
            var rel=JSON.parse(respuesta);

            console.log(rel);
            var btn1 = document.createElement('button');
            btn1.setAttribute('data-dismiss',"modal");
            btn1.className='btn btn-success';
            btn1.innerText='precio uno\n'+rel[0].precio_venta1;
            btn1.onclick=function(){
              calcular_cambio_venta(rel[0].precio_venta1);
            }
            var btn2 = document.createElement('button');
            btn2.setAttribute('data-dismiss',"modal");
            btn2.className='btn btn-success';
            btn2.innerText='precio dos\n'+rel[0].precio_venta2;
            btn2.onclick=function(){
              calcular_cambio_venta(rel[0].precio_venta2);
            }
            var btn3 = document.createElement('button');
            btn3.setAttribute('data-dismiss',"modal");
            btn3.className='btn btn-success';
            btn3.innerText='precio tres\n'+rel[0].precio_venta3;
            btn3.onclick=function(){
              calcular_cambio_venta(rel[0].precio_venta3);
            }
            var input_dif=document.createElement('input');input_dif.placeholder='OTRO mayor a: '+rel[0].precio_base;input_dif.value='';
            //input_dif.onkeyup=format(this);
            //input_dif.onchange=format(this);
            input_dif.onkeyup=function(event){
              var inputs=document.getElementById('modal_body_principal').getElementsByTagName('input');
              format(inputs[0])
              var tecla = event.which || event.keyCode;
              if (tecla===13) {
                //console.log(inputs[0].value.replace(/[,]/g, ""))
                if (inputs[0].value!=''&& parseFloat(inputs[0].value.replace(/[,]/g, ""))>=parseFloat(rel[0].precio_base)) {
                  //console.log(inputs[0].value);
                  calcular_cambio_venta(inputs[0].value.replace(/[,]/g, ""));
                } else {
                  alert('debe ingresar un valor mayor al precio base')
                }
              }
            }
            $('#modal_principal').modal('show');
            $('#modal_body_principal').html('');
            document.getElementById('modal_title').innerHTML='';
            document.getElementById('modal_title').innerText='Venta de Divisas';
            var span_divisa_maximos=document.createElement('span');span_divisa_maximos.className='badge badge-success';
            $.ajax({
              url:"divisa_maximos",
              type:'Post',
              data:{div1:div_1,div2:div_2,fecha:fecha,cedula:cedula},
              success:function(respuesta){
                //var max=JSON.parse(respuesta);
                span_divisa_maximos.innerText='MAXIMO DIVISA: '+respuesta
              },
              error:function(){

              },
            });
            $('#modal_body_principal').html('');
            $('#modal_body_principal').append(btn1,btn2,btn3,input_dif,span_divisa_maximos);
          }
        },

        error:function(respuesta){
          console.log(respuesta);
        },

    });
}
//
function calcular_cambio_compra(compra){
  $.ajax({
    url:$("#url").val()+"Divisas/divisa_unica",
    type:'Post',
    data:{valor:document.getElementById('sel_categoria_origen').value},
    success:function(respuesta){
      var div1_palabra=JSON.parse(respuesta);
      var complemento = document.getElementById('div_complemento_caja')
      complemento.innerHTML=''
      //
      var trh=document.createElement('tr');
      var th1=document.createElement('th');var th2=document.createElement('th');var th3=document.createElement('th');var th4=document.createElement('th');var th5=document.createElement('th');
      th1.innerText='Divisa 1';th2.innerText='Divisa 2';th3.innerText='Cantidad';th4.innerText='Precio unitario';th5.innerText='Total';
      trh.append(th1,th2,th3,th4,th5);
      complemento.append(trh);
      //
      var cantidad = document.getElementById('input_cantidad').value.replace(/[,]/g, "");
      var div_r = document.createElement('tr');
      //div_r.className='row';
      for (var i = 0; i < 5; i++) {
        var td = document.createElement('td');

        var label =document.createElement('h5');
        td.append(label);
        div_r.append(td);
        //div.className='col-sm-3';

        //label.className='badge badge-info';
        switch (i) {
          case 0:
              //label.innerText=document.getElementById('sel_categoria_origen').value;
              label.innerText=div1_palabra[0].nombre_categoria;
              label.setAttribute('data-divisa',document.getElementById('sel_categoria_origen').value);

            break;
          case 1:
            //label.innerText=document.getElementById('sel_categoria_destino').value;
            label.innerText='PESO';
            break;
          case 2:
            label.innerText=cantidad;
            break;
          case 3:
            label.innerText=compra;
            break;
          case 4:
            //label.innerText=(cantidad*compra).toFixed(2);
            label.innerText=new Intl.NumberFormat().format((cantidad*compra).toFixed(2));
            break;
        }
      }
      complemento.append(div_r);
      var btn_transaccion_divisas=document.createElement('button');btn_transaccion_divisas.className='btn btn-primary';btn_transaccion_divisas.innerText="Generar transacion"
      var input=document.createElement('input');input.placeholder='Cantidad que entrega el cliente'
      input.onkeyup=function(event){
        var enter=event.which || event.keyCode;
        if (enter==13) {
          alert('El cambio que se entrega es: '+(this.value-cantidad).toFixed(2))
        }

      }
      complemento.append(btn_transaccion_divisas,input);
      btn_transaccion_divisas.onclick=function(){
        btn_transaccion_divisas.disabled=true;
        var obj_tr_div=new Object()
        var tds=complemento.getElementsByTagName('td');
        var hoy = new Date();
        obj_tr_div.fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
        obj_tr_div.time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds();
        //obj_tr_div.div1=tds[0].innerText;
        obj_tr_div.div1=tds[0].firstChild.dataset.divisa;
        //obj_tr_div.div2=tds[1].innerText;
        obj_tr_div.div2=1;
        obj_tr_div.cantidad=tds[2].innerText;
        //obj_tr_div.cantidad=parseInt(tds[2].innerText.replace(/\./g, ''));
        console.log(obj_tr_div.cantidad)
        obj_tr_div.precio_uni=tds[3].innerText;
        //obj_tr_div.total=tds[4].innerText;
        console.log('antes-> ',tds[4].innerText)
        //obj_tr_div.total=parseFloat(tds[4].innerText.replace(/,/g, '').replace(/\./g, ''));
        obj_tr_div.total=parseFloat(tds[4].innerText.replace(/\./g, '').replace(/,/g, '.'));
        console.log('despues-> ',obj_tr_div.total)
        obj_tr_div.cedula_cajero=document.getElementById('cedula_cajero_login').innerText;
        obj_tr_div.nombre_cajero=document.getElementById('nombre_cajero_login').innerText;
        obj_tr_div.cedula_cliente=document.getElementById('input_cedula').value;
        obj_tr_div.nombre_cliente=document.getElementById('cliente_nombre').innerText;
        guardar_transaccion_divisa(obj_tr_div,'compra');
        $('#modal_principal').modal('show');
        $('#modal_body_principal').html('');
        var label=document.createElement('label');
        label.innerText='CONTINENTAL CAMBIOS\nCAMBIO DE MONEDA\nMoney Exchange\nTERMINAL TERRESTRE\nLocal 47 Piso 2 Ipiales-Nariño\nTel:7731277\nCel:3175736096\nNit: 1.085.901.197-9\n-------------------\nFecha:'+obj_tr_div['fecha']+' Hora:'+obj_tr_div['time']+'\nCajero:'+obj_tr_div['nombre_cajero']+'\nTiquete N°:'+Date.now()+'\nCliente:'+obj_tr_div['nombre_cliente']+'\nOperación:Compra Divisa: '+tds[0].innerText+'\n-------------------\nMonto:'+obj_tr_div['cantidad']+'\nCotizacion:'+obj_tr_div['precio_uni']+'\n-------------------\nPago Cliente:'+obj_tr_div['total']+'\n'+obj_tr_div['nombre_cliente']+'\nVERIFIQUE SU DINERO\n ANTES DE SALIR DE VENTANILLA\nNO SE ACEPTAN RECLAMOS \nDESPUÉS DE RETIRARSE\n GRACIAS POR PREFERIRNOS';
        $('#modal_body_principal').append(label);
        var btn_imprimir=document.createElement('button');btn_imprimir.className='btn btn-success';btn_imprimir.innerText='Imprimir';
        var btn_imprimir_18=document.createElement('button');btn_imprimir_18.className='btn btn-info';btn_imprimir_18.innerText='Imprimir formulario 18';btn_imprimir_18.setAttribute('data-info',JSON.stringify(obj_tr_div));btn_imprimir_18.setAttribute('data-opcion','compra');btn_imprimir_18.setAttribute('data-divisa',tds[0].innerText);
        var btn_imprimir_2=document.createElement('button');btn_imprimir_2.className='btn btn-info';btn_imprimir_2.innerText='Imprimir formulario 536';btn_imprimir_2.setAttribute('data-info',JSON.stringify(obj_tr_div));btn_imprimir_2.setAttribute('data-opcion','venta');btn_imprimir_2.setAttribute('data-divisa',tds[0].innerText);
        $('#modal_footer_principal').html('');
        $('#modal_footer_principal').append(btn_imprimir,btn_imprimir_18,btn_imprimir_2);
        btn_imprimir.onclick=function(){
          $('#modal_body_principal_2').printThis();
        }
        btn_imprimir_18.onclick=function(){
          prueba_formulario_18(this);

        }
        btn_imprimir_2.onclick=function(){
          formulario_2(this);

        }
      }
    },
  });

}
//
function calcular_cambio_venta(venta){
  //alert(venta);
  $.ajax({
    url:$("#url").val()+"Divisas/divisa_unica",
    type:'Post',
    data:{valor:document.getElementById('sel_categoria_origen').value},
    success:function(respuesta){
      var div1_palabra=JSON.parse(respuesta);
      var complemento = document.getElementById('div_complemento_caja')
      complemento.innerHTML=''
      //
      var trh=document.createElement('tr');
      var th1=document.createElement('th');var th2=document.createElement('th');var th3=document.createElement('th');var th4=document.createElement('th');var th5=document.createElement('th');
      th1.innerText='Divisa 1';th2.innerText='Divisa 2';th3.innerText='Cantidad';th4.innerText='Precio unitario';th5.innerText='Total';
      trh.append(th1,th2,th3,th4,th5);
      complemento.append(trh);
      //
      var cantidad = document.getElementById('input_cantidad').value.replace(/[,]/g, "");
      var div_r = document.createElement('tr');
      //div_r.className='row';
      for (var i = 0; i < 5; i++) {
        var td = document.createElement('td');

        var label =document.createElement('h5');
        td.append(label);
        div_r.append(td);
        //div.className='col-sm-3';

        //label.className='badge badge-info';
        switch (i) {
          case 0:
              //label.innerText=document.getElementById('sel_categoria_origen').value;
              label.innerText=div1_palabra[0].nombre_categoria;
              label.setAttribute('data-divisa',document.getElementById('sel_categoria_origen').value);

            break;
          case 1:
            //label.innerText=document.getElementById('sel_categoria_destino').value;}
            label.innerText='PESO';
            break;
          case 2:
            label.innerText=cantidad;
            break;
          case 3:
            label.innerText=venta;
            break;
          case 4:
            //label.innerText=(cantidad/venta).toFixed(2);
            label.innerText=new Intl.NumberFormat().format((cantidad/venta.replace(/[,]/g, "")).toFixed(2));;
            break;
        }
      }
      complemento.append(div_r);
      var btn_transaccion_divisas=document.createElement('button');btn_transaccion_divisas.className='btn btn-primary';btn_transaccion_divisas.innerText="Generar transacion"
      var input=document.createElement('input');input.placeholder='Cantidad que entrega el cliente'
      input.onkeyup=function(event){
        var enter=event.which || event.keyCode;
        if (enter==13) {
          alert('El cambio que se entrega es: '+(this.value-cantidad).toFixed(2))
        }

      }
      complemento.append(btn_transaccion_divisas,input);
      btn_transaccion_divisas.onclick=function(){
        btn_transaccion_divisas.disabled=true;
        var obj_tr_div=new Object()
        var tds=complemento.getElementsByTagName('td');
        //console.log(tds);
        var hoy = new Date();
        //console.log(hoy);
        obj_tr_div.fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
        obj_tr_div.time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds();
        //obj_tr_div.div1=tds[0].innerText;
        obj_tr_div.div1=tds[0].firstChild.dataset.divisa;
        //obj_tr_div.div2=tds[1].innerText;
        obj_tr_div.div2=1;
        obj_tr_div.cantidad=tds[2].innerText;
        obj_tr_div.precio_uni=tds[3].innerText.replace(/[,]/g, "");
        var temp=tds[4].innerText.replace(/[.]/g,'');
        obj_tr_div.total=temp.replace(',','.');
        obj_tr_div.cedula_cajero=document.getElementById('cedula_cajero_login').innerText;
        obj_tr_div.nombre_cajero=document.getElementById('nombre_cajero_login').innerText;
        obj_tr_div.cedula_cliente=document.getElementById('input_cedula').value;
        obj_tr_div.nombre_cliente=document.getElementById('cliente_nombre').innerText;
        //console.log(obj_tr_div)
        //return;
        guardar_transaccion_divisa(obj_tr_div,'venta');
        $('#modal_principal').modal('show');
        $('#modal_body_principal').html('');
        var label=document.createElement('label');
        label.innerText='CONTINENTAL CAMBIOS\nCAMBIO DE MONEDA\nMoney Exchange\nTERMINAL TERRESTRE\nLocal 47 Piso 2 Ipiales-Nariño\nTel:7731277\nCel:3175736096\nNit: 1.085.901.197-9\n-------------------\nFecha:'+obj_tr_div['fecha']+' Hora:'+obj_tr_div['time']+'\nCajero:'+obj_tr_div['nombre_cajero']+'\nTiquete N°:'+Date.now()+'\nCliente:'+obj_tr_div['nombre_cliente']+'\nOperación:Venta Divisa: '+tds[0].innerText+'\n-------------------\nMonto:'+obj_tr_div['cantidad']+'\nCotizacion:'+obj_tr_div['precio_uni']+'\n-------------------\nPago Cliente:'+obj_tr_div['total']+'\n'+obj_tr_div['nombre_cliente']+'\nVERIFIQUE SU DINERO\n ANTES DE SALIR DE VENTANILLA\nNO SE ACEPTAN RECLAMOS \nDESPUÉS DE RETIRARSE\n GRACIAS POR PREFERIRNOS';
        $('#modal_body_principal').append(label);
        // for (const prop in obj_tr_div) {
        //   var label=document.createElement('label');
        //   var br=document.createElement('br');
        //   label.innerText=prop+':'+obj_tr_div[prop];
        //   $('#modal_body_principal').append(br,label);
        // }

        var btn_imprimir=document.createElement('button');btn_imprimir.className='btn btn-success';btn_imprimir.innerText='Imprimir';
        var btn_imprimir_18=document.createElement('button');btn_imprimir_18.className='btn btn-info';btn_imprimir_18.innerText='Imprimir formulario 18';btn_imprimir_18.setAttribute('data-info',JSON.stringify(obj_tr_div));btn_imprimir_18.setAttribute('data-opcion','venta');btn_imprimir_18.setAttribute('data-divisa',tds[0].innerText);
        var btn_imprimir_2=document.createElement('button');btn_imprimir_2.className='btn btn-info';btn_imprimir_2.innerText='Imprimir formulario 536';btn_imprimir_2.setAttribute('data-info',JSON.stringify(obj_tr_div));btn_imprimir_2.setAttribute('data-opcion','venta');
        $('#modal_footer_principal').html('')
        $('#modal_footer_principal').append(btn_imprimir,btn_imprimir_18,btn_imprimir_2);
        btn_imprimir.onclick=function(){
          $('#modal_body_principal_2').printThis();
        }
        btn_imprimir_18.onclick=function(){
          prueba_formulario_18(this);
        }
        btn_imprimir_2.onclick=function(){
          formulario_2(this);

        }
      }
    },
  });

}
//
function consultar_datos_seguros(){
  var complemento = document.getElementById('div_complemento_caja')
  complemento.innerHTML=''
  var obj=JSON.parse(document.getElementById('sel_categoria_destino').value);
  //
  var trh=document.createElement('tr');
  var th1=document.createElement('th');th1.innerText='Nombre seg';
  var th2=document.createElement('th');th2.innerText='Categoria';
  var th3=document.createElement('th');th3.innerText='Placa';
  var th4=document.createElement('th');th4.innerText='Precio Und';
  var th5=document.createElement('th');th5.innerText='Cantidad';
  var th6=document.createElement('th');th6.innerText='total';
  trh.append(th1,th2,th3,th4,th5,th6);
  complemento.append(trh);
  //
  var div =document.createElement('tr');
  //div.className='row'
  for (var i = 0; i <6; i++) {
    var td=document.createElement('td');
    var h5 =document.createElement('h5');
    var label =document.createElement('span');label.className='badge badge-success';
    var input =document.createElement('input');input.className='form-control';input.placeholder='xxx-123'
    //h5.append(label);

    switch (i) {
      case 0:
        h5.innerText=obj.nombre_objeto_seg
        td.append(h5);
        div.append(td);
        break;
      case 1:
        h5.innerText=obj.categoria_seg;
        td.append(h5);
        div.append(td);
        break;
      case 2:
      var br=document.createElement('br')
        td.append(input);
        input.id='input_placa';
        div.append(td);
        break;
      case 3:
      h5.innerText=obj.precio_obj_venta;
      td.append(h5);
      div.append(td);
        break;
      case 4:
        h5.innerText=document.getElementById('input_cantidad').value.replace(/[,]/g, "");
        td.append(h5);
        div.append(td);
        break;
      case 5:
      var tot=obj.precio_obj_venta*document.getElementById('input_cantidad').value.replace(/[,]/g, "");
        h5.innerText=tot;
        td.append(h5);
        div.append(td);
        break;

    }
  }
  complemento.append(div);
  var btn_grd_seg=document.createElement('button');btn_grd_seg.className='btn btn-primary';btn_grd_seg.innerText='Confirmar Transaccion Seguro'
  btn_grd_seg.onclick=function(){
    if (document.getElementById('input_placa').value=="") {
      alert('debe ingresar la placa del veiculo para continuar con la transaccion');
    } else {
      //alert(document.getElementById('cedula_cajero_login').innerText);
      btn_grd_seg.disabled=true;
      var tds=complemento.getElementsByTagName('td');
      obj_tra_seg=new Object();
      var hoy = new Date();
      obj_tra_seg.fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
      obj_tra_seg.time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds();
      obj_tra_seg.nombre=tds[0].innerText;
      obj_tra_seg.categoria=tds[1].innerText;
      obj_tra_seg.placa=document.getElementById('input_placa').value;
      obj_tra_seg.cantidad=tds[4].innerText;
      obj_tra_seg.precio_uni=tds[3].innerText;
      obj_tra_seg.precio_base=obj.precio_obj_compra;
      obj_tra_seg.total=tds[5].innerText;
      obj_tra_seg.cedula_cajero=document.getElementById('cedula_cajero_login').innerText;
      obj_tra_seg.nombre_cajero=document.getElementById('nombre_cajero_login').innerText;
      obj_tra_seg.cedula_cliente=document.getElementById('input_cedula').value;
      obj_tra_seg.nombre_cliente=document.getElementById('cliente_nombre').innerText;
      guardar_transaccion_seguros(obj_tra_seg);
      $('#modal_principal').modal('show');
      $('#modal_body_principal').html('');
      var tmp_fecha=new Date();
      var l=document.createElement('label');l.innerText='Seguros '+tmp_fecha.getFullYear()+'/'+(tmp_fecha.getMonth()+1)+'/'+tmp_fecha.getDate()+'hora: '+tmp_fecha.getHours()+':'+tmp_fecha.getMinutes()+':'+tmp_fecha.getSeconds();+'\n';$('#modal_body_principal').append(l);
      for (const prop in obj_tra_seg) {
        if (prop!='precio_base') {
          var label=document.createElement('label');
          var br=document.createElement('br');
          label.innerText=prop+':'+obj_tra_seg[prop];
          $('#modal_body_principal').append(br,label);
        }

      }
      var btn_imprimir=document.createElement('button');btn_imprimir.className='btn btn-success';btn_imprimir.innerText='Imprimir';
      $('#modal_footer_principal').html('');
      $('#modal_footer_principal').append(btn_imprimir);
      btn_imprimir.onclick=function(){
        $('#modal_body_principal').printThis();
      }
      //console.log(obj_tra_seg);
    }

    //guardar_transaccion_divisa(obj_tr_div);
  }
  complemento.append(btn_grd_seg);
}
//
function consultar_datos_general(){
  var complemento = document.getElementById('div_complemento_caja')
  //complemento.innerHTML=''
  var obj=JSON.parse(document.getElementById('sel_categoria_destino').value);
  //alert(obj.precio_obj_venta);
  //alert(div_2);
  if (/Nombre Objeto/.test(complemento.innerText)) {

  } else {
    var trh=document.createElement('tr');
    var th1=document.createElement('th');th1.innerText='Nombre Objeto'
    var th2=document.createElement('th');th2.innerText='Categoria Objeto'
    var th3=document.createElement('th');th3.innerText='Precio Und Objeto'
    var th4=document.createElement('th');th4.innerText='Cantidad Objeto'
    var th5=document.createElement('th');th5.innerText='Sub-total'
    var th6=document.createElement('th');th6.innerText='Total';th6.id='total_general_tabla';

    //div.className='row'
    trh.append(th1,th2,th3,th4,th5,th6);
    complemento.append(trh);
    var btn_grd_gen=document.createElement('button');btn_grd_gen.className='btn btn-primary';btn_grd_gen.innerText='Confirmar Transaccion General';
    complemento.append(btn_grd_gen);
    btn_grd_gen.onclick=function(){
      btn_grd_gen.disabled=true;
      var objs_gen=[];
      var trs=document.getElementById('div_complemento_caja').getElementsByTagName('tr');
      $('#modal_principal').modal('show');
      $('#modal_body_principal').html('');
      var tmp_fecha=new Date();
      var l=document.createElement('label');l.innerText='Varios '+tmp_fecha.getFullYear()+'/'+(tmp_fecha.getMonth()+1)+'/'+tmp_fecha.getDate()+'hora: '+tmp_fecha.getHours()+':'+tmp_fecha.getMinutes()+':'+tmp_fecha.getSeconds();+'\n';$('#modal_body_principal').append(l);
      //console.log(trs);
      for (var i = 1; i < trs.length; i++) {
        var obj_tra_gen=new Object();
        var tds=trs[i].getElementsByTagName('td');
        var hoy = new Date();
        obj_tra_gen.fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
        obj_tra_gen.time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds();
        obj_tra_gen.nombre=tds[0].innerText;
        obj_tra_gen.categoria=tds[1].innerText;
        obj_tra_gen.cantidad=tds[3].innerText;
        obj_tra_gen.precio_uni=tds[2].innerText;
        obj_tra_gen.precio_base=obj.precio_obj_gen_compra;
        obj_tra_gen.total=tds[4].innerText;
        obj_tra_gen.cedula_cajero=document.getElementById('cedula_cajero_login').innerText;
        obj_tra_gen.nombre_cajero=document.getElementById('nombre_cajero_login').innerText;
        obj_tra_gen.cedula_cliente=document.getElementById('input_cedula').value;
        obj_tra_gen.nombre_cliente=document.getElementById('cliente_nombre').innerText;
        //guardar_transaccion_general(obj_tra_gen);
        //console.log(obj_tra_gen);
        objs_gen.push(obj_tra_gen);
        // recibo


          var label1=document.createElement('label');var label2=document.createElement('label');var label3=document.createElement('label');var label4=document.createElement('label');
          var br1=document.createElement('br');var br2=document.createElement('br');var br3=document.createElement('br');var br4=document.createElement('br');
          label1.innerText='objeto: '+obj_tra_gen.nombre;label2.innerText='categoria: '+obj_tra_gen.categoria;label3.innerText='cantidad: '+obj_tra_gen.cantidad;label4.innerText='precio unit: '+obj_tra_gen.precio_uni
          $('#modal_body_principal').append(br1,label1,br3,label3,br4,label4);
          //alert(obj_tra_gen.nombre);

        //

      }
      var brf=document.createElement('br');
      var t_label=document.createElement('label');t_label.innerText='TOTAL: '+tot_general;
      $('#modal_body_principal').append(brf,t_label);
      var btn_imprimir=document.createElement('button');btn_imprimir.className='btn btn-success';btn_imprimir.innerText='Imprimir';
      $('#modal_footer_principal').html('');
      $('#modal_footer_principal').append(btn_imprimir);
      btn_imprimir.onclick=function(){
        $('#modal_body_principal').printThis();
      }
      //console.log(objs_gen),
      guardar_transaccion_general(objs_gen,tot_general);

    }
  }
  var div =document.createElement('tr');
  for (var i = 0; i <6; i++) {
    var h5 =document.createElement('h5');
    var label =document.createElement('td');//label.className='badge badge-success';
    label.append(h5);
    div.append(label);
    switch (i) {
      case 0:
        h5.innerText=obj.nombre_objeto_gen
        break;
      case 1:
        h5.innerText=obj.categoria_gen
        //div.append(h5);
        break;
      case 2:
        h5.innerText=obj.precio_obj_gen_venta
        //div.append(h5);
        break;
      case 3:
        h5.innerText=document.getElementById('input_cantidad').value.replace(/[,]/g, "");
        //div.append(h5);
        break;
      case 4:
        var tot=obj.precio_obj_gen_venta*document.getElementById('input_cantidad').value.replace(/[,]/g, "")
        h5.innerText=tot;
        tot_general+=tot;
        document.getElementById('total_general_tabla').innerText='Total: '+(tot_general);
        //div.append(h5);
        break;
      case 5:
        var icon=document.createElement('i');icon.className='fas fa-trash-alt btn btn-warning';
        label.append(icon);
        icon.onclick=function(){
          tot_general-=tot;
          document.getElementById('total_general_tabla').innerText='Total: '+(tot_general);
          complemento.removeChild(div)
        }
        break;

    }
  }
  complemento.append(div);


}
function guardar_transaccion_divisa(obj,opcion){
  //console.log(obj)
  if (opcion=='compra') {
    obj.precio_base=obj.precio_uni;
    guardar_transaccion_divisa_final(obj,opcion);
  } else {
    var hoy2 = new Date();
    var fecha=hoy2.getFullYear()+'/'+(hoy2.getMonth()+1)+'/'+hoy2.getDate();
    var cajero=document.getElementById('cedula_cajero_login').innerText;
   $.ajax({
     url:$("#url").val()+"Divisas/consultar_relacion2",
     type:'Post',
     data:{div1:obj.div1,div2:obj.div2,cedula:cajero,fecha:fecha},
     success:function(respuesta){
       console.log(respuesta);
       var datos=JSON.parse(respuesta);
       //console.log(obj.time+1)
       for (var i = 0; i < datos.length; i++) {
         var hoy = new Date();
         var nuevo_obj=new Object()
         if (parseFloat(obj.total)>parseFloat(datos[i].cantidad)) {
           obj.total=obj.total-datos[i].cantidad
           nuevo_obj.total=datos[i].cantidad;
           nuevo_obj.cantidad=nuevo_obj.total*obj.precio_uni
           obj.cantidad=obj.cantidad-nuevo_obj.cantidad
           nuevo_obj.cedula_cajero=obj.cedula_cajero;
           nuevo_obj.cedula_cliente=obj.cedula_cliente;
           nuevo_obj.div1=obj.div1;
           nuevo_obj.div2=obj.div2;
           nuevo_obj.fecha=obj.fecha;
           nuevo_obj.nombre_cajero=obj.nombre_cajero;
           nuevo_obj.nombre_cliente=obj.nombre_cliente;
           nuevo_obj.precio_uni=obj.precio_uni;
           nuevo_obj.precio_base=datos[i].precio_base;
           nuevo_obj.time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds()+'.'+hoy.getMilliseconds();
           //console.log(nuevo_obj);
           guardar_transaccion_divisa_final(nuevo_obj,opcion)
         } else {
           //obj.cantidad=obj.precio_uni*obj.total;
           obj.precio_base=datos[i].precio_base;
           obj.time=hoy.getHours()+':'+hoy.getMinutes()+':'+hoy.getSeconds()+'.'+hoy.getMilliseconds();
           guardar_transaccion_divisa_final(obj,opcion)
           //console.log(obj)
           return;
         }
       }
     },
     error:function(){

     },
   });
  }
}
function guardar_transaccion_divisa_final(obj,opcion){
 $.ajax({
   url:$("#url").val()+"Transacciones/transaciones_divisas",
   type:'Post',
   data:{obj:obj,opcion:opcion},
   success:function(respuesta){
     //
     if (respuesta=='true') {
       alert('se guardo la transaccion en divisas');
     } else {

     }
   },
   error:function(){

   },
 });
}
//
function guardar_transaccion_seguros(obj){
 $.ajax({
   url:$("#url").val()+"Transacciones/transaciones_seguros",
   type:'Post',
   data:{obj:obj},
   success:function(respuesta){
     //
     if (respuesta=='true') {
       alert('se guardo la transaccion seguros');
     } else {

     }
   },
   error:function(){

   },
 });
}
function guardar_transaccion_general(obj,total){
 $.ajax({
   url:$("#url").val()+"Transacciones/transaciones_general",
   type:'Post',
   data:{obj:obj,total:total},
   success:function(respuesta){
     //
     console.log(respuesta);
     if (respuesta!='false') {
       alert('se guardo la transaccion general');
     } else {

     }
   },
   error:function(){

   },
 });
}
function format(input)
{
  var num = input.value.replace(/\,/g,'');
  if(!isNaN(num)){
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
    num = num.split('').reverse().join('').replace(/^[\,]/,'');
    input.value = num;
  }

  else{ alert('Solo se permiten numeros');
    input.value = input.value.replace(/[^\d\.]*/g,'');
  }
}
function prueba_formulario_18(element){
  //console.log(element.dataset.info);
  var factura = JSON.parse(element.dataset.info);
  $('#modal_formulario_18').modal('show');
  document.getElementById('fecha_hoy_18').innerText=factura.fecha;
  document.getElementById('identificacion_cliente_18').innerText=factura.cedula_cliente;
  document.getElementById('nombre_cliente_18').innerText=factura.nombre_cliente;
  document.getElementById('tasa_cambio_18').innerText=factura.precio_uni;
  document.getElementById('monto_negociado_18').innerText=factura.cantidad;
  document.getElementById('moneda_negociada_18').innerText=element.dataset.divisa;
  document.getElementById('total_18').innerText=factura.total;
  document.getElementById('input_efectivo_18').value=factura.total;

}
function imprimir_formulario_18(){
  $('#modal_body_formulario_18').printThis();
}
function formulario_2(element){
  var factura = JSON.parse(element.dataset.info);
  $('#modal_formulario_2').modal('show');
  var factura = JSON.parse(element.dataset.info);
  var nombre=factura.nombre_cliente.split(' ');
  //console.log(nombre);
  document.getElementById('fecha_hoy_536').innerText=factura.fecha;
  document.getElementById('cedula_cliente_536').innerText=factura.cedula_cliente;
  if (nombre.length==2) {
    document.getElementById('primer_apellido_536').innerText=nombre[1];
    document.getElementById('segundo_appellido_536').innerText='';
    document.getElementById('segundo_nombre_536').innerText='';
  } else {
    if (nombre.length==3) {
      document.getElementById('primer_apellido_536').innerText=nombre[nombre.length-2];
      document.getElementById('segundo_appellido_536').innerText=nombre[nombre.length-1];
      document.getElementById('segundo_nombre_536').innerText='';
      //
    } else {
      document.getElementById('primer_apellido_536').innerText=nombre[nombre.length-2];
      document.getElementById('segundo_appellido_536').innerText=nombre[nombre.length-1];
      var otros_n=''
      for (var i = 1; i < nombre.length-2; i++) {
        otros_n+=nombre[i]+' '
      }
      document.getElementById('segundo_nombre_536').innerText=otros_n;
    }
  }
  document.getElementById('primer_nombre_536').innerText=nombre[0];
  //document.getElementById('primer_nombre_536').innerText=factura.nombre_cliente;
  //document.getElementById('tasa_cambio_536').innerText=factura.precio_uni;
  document.getElementById('monto_negociado_536').innerText=factura.cantidad;
  document.getElementById('moneda_negociada_536').innerText=element.dataset.divisa;
  document.getElementById('valor_pesos_536').innerText=factura.total;
  var hoy_año=new Date();
  document.getElementById('año_536').innerText=hoy_año.getFullYear();
}
function imprimir_formulario_536(){
  $('#modal_body_formulario_2').printThis();

}
