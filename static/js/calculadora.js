$(document).ready(calculadora_in);
var bandera_inpt_1=false;
var bandera_inpt_2=false;
var op='+';
var estado = false;
//
function calculadora_in(){
  $('#calculadora').click(function(){
    $('#modal_calculadora').modal('show');
    cargar_origen();
  });

  //verificar();
}
function verificar(){
  var cedula = document.getElementById('cedula_cajero_login').innerText;
  var boton = document.getElementById('boton_turno');
  $.ajax({
    url:"verificar_estado",
    type:"Post",
    data:{cajero:cedula},
    beforeSend:function(){},
    success:function(respuesta){
      if(respuesta == "true"){
        estado = true;
        boton.innerText = 'Cerrar Turno'
        boton.className = 'badge badge-danger' 


      }else{
        estado = false;
      }

      console.log(estado);
    }
  });
}

function iniciar_turno(){
  var hoy = new Date()
  var fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  var cedula = document.getElementById('cedula_cajero_login').innerText;
  hora = hoy.getHours()+":"+hoy.getMinutes()
  if(estado == false){
    $.ajax({
      url:"iniciar_turno",
      type:'Post',
      data:{
        cedula:cedula,
        fecha:fecha,
        hora:hora
      },
      beforeSend:function(){
  
      },
      success:function(respuesta){
        if(respuesta=="true"){
          alert("Se ha Iniciado Turno")
          verificar();
        }
      }
    });
  }
  else{
    $.ajax({
      url:"terminar_turno",
      type:'Post',
      data:{
        cedula:cedula,
      },
      beforeSend:function(){
  
      },
      success:function(respuesta){
        if(respuesta=="true"){
          alert("Se ha cerrado Turno")
          verificar();
        }
      }
    });
  }
  

}
function cajero_modal(){
  $('#modal_cajero').modal('show');
  mostrar_cajeros()

}
//
function tecla_press(element){
  var inp1 = document.getElementById('input_1_calc');
  var inp2 = document.getElementById('input_2_calc');
  if (element.dataset.numero=='+'||element.dataset.numero=='-'||element.dataset.numero=='*'||element.dataset.numero=='/') {
    bandera_inpt_1=true;
  } else {
    if (element.dataset.numero=='=') {
      if (inp1.value=='' || inp2.value=='') {
        alert('hay un dato incompleto')
      } else {
        var d1=Number(inp1.value);
        var d2=Number(inp2.value);
        //alert(d2);
        switch (op) {
          case '+':
          //alert('ḿas'+bandera_inpt_1);
            document.getElementById('result_calc').innerText=d1+d2
            break;
          case '-':
            document.getElementById('result_calc').innerText=d1-d2
            break;
          case '*':
            document.getElementById('result_calc').innerText=d1*d2
            break;
          case '/':
            if (d2===0) {
              alert('revisa los valores no puede ser 0 el dividendo');
            } else {
              document.getElementById('result_calc').innerText=d1/d2;
            }
            break;


        }
      }
    } else {
      if (element.dataset.numero=='c') {
        inp1.value='';
        inp2.value='';
        bandera_inpt_1=false;
      } else {
        if (bandera_inpt_1==false) {
        //alert('1');
          if (inp1.value=='') {
            //alert('1');
            inp1.value=element.dataset.numero
          } else {
            inp1.value+=element.dataset.numero
          }
        } else {
          if (inp2.value=='') {
            //alert('1');
            inp2.value=element.dataset.numero
          } else {
            inp2.value+=element.dataset.numero
          }
        }
      }
    }
  }
  switch (element.dataset.numero) {
    case '+':
    //alert('ḿas'+bandera_inpt_1);
    op='+';
    document.getElementById('operador').innerText='+'
      break;
    case '-':
      op='-';
      document.getElementById('operador').innerText='-'
      break;
    case '*':
      op='*';
      document.getElementById('operador').innerText='*'
      break;
    case '/':
      op='/';
      document.getElementById('operador').innerText='/';
      break;


  }
}
function cargar_origen(){
  $.ajax({
        url:"categorias_divisas",
        type:'Post',
        data:{},
        beforeSend:function(){
        	//
        },
        success:function(respuesta){
          document.getElementById('sel_origen_modal').innerHTML='';
          var def=document.createElement('option');
          def.innerText='selecciona divisa 1'
          document.getElementById('sel_origen_modal').append(def);
            var categorias=JSON.parse(respuesta);
            //var opt = document.createElement('option');
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_categoria;
              opt.value=categorias[i].id_categoria;
              if (categorias[i].nombre_categoria!='PESO') {
                document.getElementById('sel_origen_modal').append(opt);
              }

            }


            //text.innerHTML=respuesta;
            //alert('se guardaron datos principales del auto')

        },

        error:function(){
            //$("#texto").html("pailas");
            //text.innerHTML='error';
        },

    });
}
function cargar_destino(){
  $.ajax({
        url:"categorias_menos",
        type:'Post',
        data:{option:document.getElementById('sel_origen_modal').value},
        beforeSend:function(){
        	//
        },
        success:function(respuesta){
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');
            def.innerText='selecciona divisa 2'
            document.getElementById('sel_destino_modal').innerHTML=''
            document.getElementById('sel_destino_modal').append(def);
            //var opt = document.createElement('option');
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_categoria;
              opt.value=categorias[i].id_categoria;
              if (categorias[i].nombre_categoria=='PESO') {
                document.getElementById('sel_destino_modal').append(opt);
              }
            }


            //text.innerHTML=respuesta;
            //alert('se guardaron datos principales del auto')

        },

        error:function(){
            //$("#texto").html("pailas");
            //text.innerHTML='error';
        },

    });
}
//
function opciones_venta_compra(){
  var div_1 = document.getElementById('sel_origen_modal').value;
  var div_2 = document.getElementById('sel_destino_modal').value;
  var cedula=document.getElementById('cedula_cajero_login').innerText;
  var hoy =new Date();
  var fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  $.ajax({
        url:"consultar_relacion2",
        type:'Post',
        data:{div1:div_1,div2:div_2,fecha:fecha,cedula:cedula},
        success:function(respuesta){
          //alert(respuesta)
          if (respuesta=='false') {
          } else {
            var rel=JSON.parse(respuesta);
            console.log(rel);
            var opt0 = document.createElement('option');
            opt0.innerText='opciones venta';
            var opt1 = document.createElement('option');
            opt1.className='btn btn-success';
            opt1.innerText='venta uno: '+rel[0].precio_venta1;
            opt1.value=rel[0].precio_venta1;
            var opt2 = document.createElement('option');
            opt2.className='btn btn-success';
            opt2.innerText='venta dos: '+rel[0].precio_venta2;
            opt2.value=rel[0].precio_venta2;
            var opt3 = document.createElement('option');
            opt3.className='btn btn-success';
            opt3.innerText='venta tres: '+rel[0].precio_venta3;
            opt3.value=rel[0].precio_venta3;
            $('#modal_venta').html('');
            $('#modal_venta').append(opt0,opt1,opt2,opt3);
          }
        },

        error:function(){

        },

    });
    var hoy = new Date()
    var fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
    var cedula = document.getElementById('cedula_cajero_login').innerText;
    $.ajax({
          url:"consultar_relacion",
          type:'Post',
          data:{div1:div_1,div2:div_2,cedula:cedula,fecha:fecha},
          success:function(respuesta){
            //alert(respuesta)
            $('#modal_compra').html('');
            if (respuesta=='false') {
            } else {
              var rel=JSON.parse(respuesta);
              console.log(rel);
              var opt0 = document.createElement('option');
              opt0.innerText='opciones compra';
              $('#modal_compra').append(opt0);
              for (var i = 0; i < rel.length; i++) {
                var opt = document.createElement('option');
                opt.value=rel[i].precio_base;opt.innerText=rel[i].precio_base
                $('#modal_compra').append(opt);
              }
              // var opt1 = document.createElement('option');
              // opt1.className='btn btn-info';
              // opt1.innerText='compra uno: '+rel[0].precio_base;
              // opt1.value=rel[0].precio_base;
              // var opt2 = document.createElement('option');
              // opt2.className='btn btn-info';
              // opt2.innerText='compra dos: '+rel[0].precio_compra2;
              // opt2.value=rel[0].precio_compra2;
              // var opt3 = document.createElement('option');
              // opt3.className='btn btn-info';
              // opt3.innerText='compra tres: '+rel[0].precio_compra3;
              // opt3.value=rel[0].precio_compra3;

              //$('#modal_compra').append(opt0,opt1,opt2,opt3);

            }
          },

          error:function(){

          },

      });
}
function calcular_compra(){
  var inpt1 = document.getElementById('input_1_calc').value;
  var precio =document.getElementById('modal_compra').value;
  document.getElementById('result_calc').innerText=inpt1*precio
}
function calcular_venta(){
  var inpt1 = document.getElementById('input_1_calc').value;
  var precio =document.getElementById('modal_venta').value;
  document.getElementById('result_calc').innerText=(inpt1/precio).toFixed(2);
}
function guardar_cajero(){
  var cajero=new Object()
  cajero.cedula=document.getElementById('input_cedula_cajero').value;
  cajero.nombre=document.getElementById('input_nombre_cajero').value;
  cajero.apellido=document.getElementById('input_apellido_cajero').value;
  cajero.clave=document.getElementById('input_telefono_cajero').value;
  if (cajero.cedula==''||cajero.nombre==''||cajero.clave==''||cajero.apellido=='') {
    alert('hay datos del cajero necesrios para guardar')
  } else {
    $.ajax({
          url:"insertar_cajero",
          type:'Post',
          data:{cajero:cajero},
          beforeSend:function(){
          	//
          },
          success:function(respuesta){
            if (respuesta=="true") {
              alert('se guardo el cajero correctamente');
              mostrar_cajeros();
            } else {
              alert('no se guardo el cajero');
            }

          },

          error:function(){
              alert('ocurrio un error al guardar el cajero revisa los datos');
          },

      });
  }
}
function mostrar_cajeros(){
  $('#cajeros_body').html('');
  $.ajax({
        url:"cajeros",
        type:'Post',
        data:{},
        beforeSend:function(){
          //
        },
        success:function(respuesta){
          var datos = JSON.parse(respuesta);
          for (var i = 0; i < datos.length; i++) {
            var tr=document.createElement('tr');
            var td1=document.createElement('td');var td2=document.createElement('td');var td3=document.createElement('td');var td4=document.createElement('td');
            var btn_borrar_cajero=document.createElement('button');btn_borrar_cajero.className='btn btn-danger fas fa-trash';
            btn_borrar_cajero.setAttribute("data-cedula", datos[i].cedula);
            btn_borrar_cajero.onclick = function() {
              $.ajax({
                url:"eliminar_cajero",
                type:'Post',
                //data:{fecha:fecha,fecha2:fecha2},
                data:{cedula:this.dataset.cedula},
                success:function(respuesta){
                  if (respuesta=='true') {
                    alert('se elimino');
                    mostrar_cajeros();
                  } else {
                    alert('no se elimino');
                  }
                },
                error:function(){
            
                },
              });
            }; td4.append(btn_borrar_cajero);

            tr.append(td1,td2,td3,td4);
            td1.innerText=datos[i].nombre;td2.innerText=datos[i].cedula;td3.innerText=datos[i].clave;
            $('#cajeros_body').append(tr);
          }

        },

        error:function(){
            alert('error cajeros');
        },

    });
}

function mostar_informe_cajero_seguro(){
  var cajero=document.getElementById('cedula_cajero_login').innerText
  $('#modal_informe_cajeros_seguros').modal('show');
  var hoy = new Date();
  var fecha = hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  console.log(cajero);
  $.ajax({
    url:"informe_cajero_seguros",
    type:"Post",
    data:{cajero:cajero, fecha:fecha},
    success:function(respuesta){
      var objetos = JSON.parse(respuesta);
      console.log(objetos);
      var total=0
      for (var i = 0; i < objetos.length; i++) {
        //total+=parseInt(objetos[i].total_tra_gen);
        var tr=document.createElement('tr');
        var td1=document.createElement('td');td1.innerText=objetos[i].fecha_seg
        var td2=document.createElement('td');td2.innerText=objetos[i].nombre_seguros
        var td3=document.createElement('td');td3.innerText=objetos[i].nombre_seg
        var td4=document.createElement('td');td4.innerText=objetos[i].precio_base_seg
        var td5=document.createElement('td');td5.innerText=objetos[i].precio_uni_seg
        var td6=document.createElement('td');td6.innerText=objetos[i].cantidad_seg
        var td7=document.createElement('td');td7.innerText=objetos[i].total_tra_seg
        total += parseFloat(objetos[i].total_tra_seg)
        tr.append(td1,td2,td3,td4,td5,td6,td7);
        document.getElementById('contenedor_informe_cajeros_seguros').append(tr);
      }
      var trf=document.createElement('tr');
      var tdf=document.createElement('td');tdf.colSpan='5';tdf.innerText='TOTAL: '+total;
      trf.append(tdf);
      document.getElementById('contenedor_informe_cajeros_seguros').append(trf);
      
    }
  })
}

function mostar_informe_cajero(){
  var cajero=document.getElementById('cedula_cajero_login').innerText
  $('#modal_informe_cajeros').modal('show');
  var hoy = new Date();
  var fecha =hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  $.ajax({
    url:"informe_cajero",
    type:"Post",
    data:{cajero:cajero,fecha:fecha},
    success:function(respuesta){
      var objetos=JSON.parse(respuesta);
      document.getElementById('contenedor_informe_cajeros').innerHTML=''
      var pesos_compra=0;var pesos_venta=0;
      for (var i = 0; i < objetos[0].length; i++) {
        if(objetos[0][i].tipo=='compra'){
          pesos_compra+=parseFloat(objetos[0][i].total_tra_div);
        }
        else{
          pesos_venta+=parseFloat(objetos[0][i].cantidad_div);
        }
      }
      var divisa='';
      var tot_parcial=0
      var suma_ingresos=0;
      var suma_egresos=0;
      for (var i = 0; i < objetos[1].length; i++) {
        var egreso_actualizacion=0;
        var ingreso_actualizacion=0;        
        if(objetos[1][i].ingreso!=null){
          ingreso_actualizacion=objetos[1][i].ingreso;
        }
        if(objetos[1][i].egreso!=null){
          egreso_actualizacion=objetos[1][i].egreso;
        }
        if (divisa!=objetos[1][i].nombre_categoria&&objetos[1][i].nombre_categoria!='PESO') {
          if (divisa!='') {
            document.getElementById('head'+divisa).innerText=divisa+' TOTAL: '+tot_parcial;
            document.getElementById('sum_ing'+divisa).innerText=' SUMA INGRESOS: '+suma_ingresos;
            document.getElementById('sum_egr'+divisa).innerText=' SUMA EGRESOS: '+suma_egresos;
            
          }
          tot_parcial=0;
          suma_ingresos=0;
          suma_egresos=0;
          divisa=objetos[1][i].nombre_categoria;
          var table = document.createElement('table');table.className='table table-hover table-bordered';
          var thead = document.createElement('thead');thead.className='thead-dark';
          var tbody = document.createElement('tbody');tbody.id=divisa;
          var trh1 = document.createElement('tr');
          var th1=document.createElement('th');th1.colSpan="2";trh1.append(th1);th1.id='head'+divisa;
          var th6=document.createElement('th');th6.innerText='SUMA Ingresos: ';trh1.append(th6);th6.id='sum_ing'+divisa;
          var th7=document.createElement('th');th7.innerText='SUMA Egresos';trh1.append(th7);th7.id='sum_egr'+divisa;
          var trh2 = document.createElement('tr');thead.append(trh1,trh2);
          var th2=document.createElement('th');th2.innerText='Precio';trh2.append(th2);
          var th3=document.createElement('th');th3.innerText='Cantidad';trh2.append(th3);
          var th4=document.createElement('th');th4.innerText='Ingresos';trh2.append(th4);
          var th5=document.createElement('th');th5.innerText='Egresos';trh2.append(th5);
          table.append(thead,tbody);
          var tr = document.createElement('tr');var td1=document.createElement('td');var td2=document.createElement('td');var td3=document.createElement('td');var td4=document.createElement('td');tr.append(td1,td2,td3,td4);tbody.append(tr);
          td1.innerText=objetos[1][i].precio_base;td2.innerText=objetos[1][i].cantidad;td3.innerText=ingreso_actualizacion;td4.innerText=egreso_actualizacion;
          document.getElementById('contenedor_informe_cajeros').append(table);
          tot_parcial+=parseFloat(objetos[1][i].cantidad)
          suma_ingresos+=parseFloat(ingreso_actualizacion);
          suma_egresos+=parseFloat(egreso_actualizacion);
          if (i==objetos[1].length-1) {
            document.getElementById('head'+divisa).innerText=divisa+' TOTAL: '+tot_parcial;
            document.getElementById('sum_ing'+divisa).innerText=' SUMA INGRESOS: '+suma_ingresos;
            document.getElementById('sum_egr'+divisa).innerText=' SUMA EGRESOS: '+suma_egresos;
          }
        } else {
          
          if (objetos[1][i].nombre_categoria=='PESO') {
            var table = document.createElement('table');table.className='table table-hover table-bordered';
            var thead = document.createElement('thead');thead.className='thead-dark';
            var trh1 = document.createElement('tr');            
            var th1=document.createElement('th');th1.colSpan="2";trh1.append(th1);th1.id='peso'
            th1.innerText='PESO TOTAL: '+(parseFloat(objetos[1][i].cantidad)+pesos_venta-pesos_compra);
            var th2=document.createElement('th');trh1.append(th2);th2.innerText='Ingresos: '+(parseFloat(ingreso_actualizacion));
            var th3=document.createElement('th');trh1.append(th3);th3.innerText='Egresos: '+(parseFloat(egreso_actualizacion));
            thead.append(trh1);
            table.append(thead);
            document.getElementById('contenedor_informe_cajeros').append(table);
            //tot_parcial+=parseFloat(objetos[1][i].cantidad)
          } else {
            var tr = document.createElement('tr');var td1=document.createElement('td');var td2=document.createElement('td');var td3=document.createElement('td');var td4=document.createElement('td');tr.append(td1,td2,td3,td4);
            document.getElementById(divisa).append(tr);
            td1.innerText=objetos[1][i].precio_base;td2.innerText=objetos[1][i].cantidad;
            td3.innerText=ingreso_actualizacion;td4.innerText=egreso_actualizacion;
            tot_parcial+=parseFloat(objetos[1][i].cantidad)
            suma_ingresos+=parseFloat(ingreso_actualizacion);
            suma_egresos+=parseFloat(egreso_actualizacion);
            if (i==objetos[1].length-1) {
              document.getElementById('head'+divisa).innerText=divisa+' TOTAL: '+tot_parcial;
              document.getElementById('sum_ing'+divisa).innerText=divisa+' SUMA INGRESOS: '+suma_ingresos;
              document.getElementById('sum_egr'+divisa).innerText=divisa+' SUMA EGRESOS: '+suma_egresos;
            }
          }

        }
      }
    },
    error:function(){

    },
  });
}
function format(input)
{
  var num = input.value.replace(/\,/g,'');
  if(!isNaN(num)){
    num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
    num = num.split('').reverse().join('').replace(/^[\,]/,'');
    input.value = num;
  }

  else{ alert('Solo se permiten numeros');
    input.value = input.value.replace(/[^\d\.]*/g,'');
  }
}
function otro_compra_calculadora(event){
  var codigo = event.which || event.keyCode;
  if(codigo===13){
    var inpt1 = document.getElementById('input_1_calc').value;
    var precio =document.getElementById('otro_modal_compra').value.replace(/[,]/g,'');
    document.getElementById('result_calc').innerText=inpt1*precio
  }
}
function otro_venta_calculadora(event){
  var codigo = event.which || event.keyCode;
  if(codigo===13){
    var inpt1 = document.getElementById('input_1_calc').value;
    var precio =document.getElementById('otro_modal_venta').value.replace(/[,]/g,'');
    document.getElementById('result_calc').innerText=(inpt1/precio).toFixed(2);
  }
}
function divisas_reporte_cajero(){
  $('#modal_reporte_divisas_cajeros').modal('show');
  var hoy = new Date();
  var fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  var cedula=document.getElementById('cedula_cajero_login').innerText;
  document.getElementById('body_tabla_modal').innerHTML='';
  consultar_datos_div(fecha,cedula)
  // if ((inicio==''&&fin=='')||(inicio==''&&fin!='')||(inicio!=''&&fin=='')||(inicio!=''&&fin!=''&&inicio<=fin)) {
  //   //inicio=inicio.replace(/[/]/g,'-');fin=fin.replace(/[/]/g,'-');
  //   //alert(inicio+'<>'+fin)
  //   consultar_datos_div(inicio,fin)
  // } else {
  //   alert('La fecha fin debe ser mayor a la fecha de inicio de buscqueda');
  // }
}
function consultar_datos_div(fecha,cedula){
  $.ajax({

    url:"consultar_transacciones_divisas2",
    type:'Post',
    data:{fecha:fecha,cedula:cedula},
    success:function(respuesta){
      //
      //console.log(respuesta);
      var datos=JSON.parse(respuesta);
      for (var i = 0; i < datos.length; i++) {
        var tr = document.createElement('tr');
        for (var j = 0; j < 13; j++) {
          var td = document.createElement('td');
          tr.append(td);
          switch (j) {
            case 0:
              td.innerText=datos[i]['fecha_div'];
              break;
            case 1:
                td.innerText=datos[i]['hora_div'];
                break;
            case 2:
              td.innerText=datos[i]['nombre_categoria'];
              break;
            case 3:
              //td.innerText=datos[i]['div2'];
              td.innerText='PESO';
              break;
            case 4:
              td.innerText=datos[i]['cantidad_div'];
              break;
            case 5:
              td.innerText=datos[i]['precio_uni_div'];
              break;
            case 6:
              td.innerText=datos[i]['total_tra_div'];
              break;
            case 7:
              td.innerText=datos[i]['tipo'];
              break;
            case 8:
              td.innerText=datos[i]['ced_cajero_div'];
              break;
            case 9:
              td.innerText=datos[i]['nombre_cajero_div'];
              break;
            case 10:
              td.innerText=datos[i]['ced_cliente_div'];
              break;
            case 11:
              td.innerText=datos[i]['nombre_cliente_div'];
              break;
            case 12:
              var btn=document.createElement('button');btn.setAttribute('data-objeto',JSON.stringify(datos[i]));btn.className='btn btn-info';btn.innerText='IMPRIMIR'
              var btn2=document.createElement('button');btn2.setAttribute('data-objeto',JSON.stringify(datos[i]));btn2.setAttribute('data-orden',(datos.length)-i);btn2.className='btn btn-danger';btn2.innerText='eliminar'
              var btn3=document.createElement('button');btn3.setAttribute('data-objeto',JSON.stringify(datos[i]));btn3.innerText='18'
              var btn4=document.createElement('button');btn4.setAttribute('data-objeto',JSON.stringify(datos[i]));btn4.innerText='536'
              btn.onclick=function(){
                imprimir_nuevamente(this);
              }
              btn2.onclick=function(){
                var r= confirm('esta seguro de eliminar');
                if (r==true) {
                  eliminar_transaccion_divisa(this);
                } else {

                }
              }
              btn3.onclick=function(){
                prueba_formulario_18_rep(this);
              }
              btn4.onclick=function(){
                formulario_2_rep(this)
              }
              td.append(btn,btn3,btn4);
              break;

          }
        }
        $('#body_tabla_modal').append(tr);
      }
    },
    error:function(){

    },
  });
}
function eliminar_transaccion_divisa (element){
  //alert(element.dataset.orden);
  $.ajax({
    url:"eliminar_transaccion_divisa",
    type:'Post',
    //data:{fecha:fecha,fecha2:fecha2},
    data:{orden:element.dataset.orden,objeto:JSON.parse(element.dataset.objeto)},
    success:function(respuesta){
      if (respuesta=='true') {
        alert('se elimino');
        divisas_reporte_cajero()
      } else {
        alert('no se elimino');
      }
    },
    error:function(){

    },
  });
}
function imprimir_nuevamente(element){
  $('#modal_footer_principal').html('');
  var obj_tr_div=JSON.parse(element.dataset.objeto);
  //console.log(obj_tr_div);
  $('#modal_principal').modal('show');
  $('#modal_body_principal').html('');
  var label=document.createElement('label');
  label.innerText='CONTINENTAL CAMBIOS\nCAMBIO DE MONEDA\nMoney Exchange\nTERMINAL TERRESTRE\nLocal 47 Piso 2 Ipiales-Nariño\nTel:7731277\nCel:3175736096\nNit: 1.085.901.197-9\n-------------------\nFecha:'+obj_tr_div['fecha_div']+' Hora:'+obj_tr_div['hora_div']+'\nCajero:'+obj_tr_div['nombre_cajero_div']+'\nTiquete N°:'+Date.now()+'\nCliente:'+obj_tr_div['nombre_cliente_div']+'\nOperación:Compra Divisa: '+obj_tr_div['nombre_categoria']+'\n-------------------\nMonto:'+obj_tr_div['cantidad_div']+'\nCotizacion:'+obj_tr_div['precio_uni_div']+'\n-------------------\nPago Cliente:'+obj_tr_div['total_tra_div']+'\n'+obj_tr_div['nombre_cliente_div']+'\nVERIFIQUE SU DINERO\n ANTES DE SALIR DE VENTANILLA\nNO SE ACEPTAN RECLAMOS \nDESPUÉS DE RETIRARSE\n GRACIAS POR PREFERIRNOS';
  $('#modal_body_principal').append(label);
  var btn_imprimir=document.createElement('button');btn_imprimir.className='btn btn-success';btn_imprimir.innerText='Imprimir';
  $('#modal_footer_principal').append(btn_imprimir);
  btn_imprimir.onclick=function(){
    $('#modal_body_principal_2').printThis();
  }
}
function ingresos_egresos(){
  $('#modal_solicitud_cajeros').modal('show');
  $.ajax({
        url:"categorias_divisas",
        type:'Post',
        data:{},
        success:function(respuesta){
          document.getElementById('sel_divisa_ingresos_egresos').innerHTML='';
            var categorias=JSON.parse(respuesta);
            var def=document.createElement('option');def.innerText='Divisa';document.getElementById('sel_divisa_ingresos_egresos').append(def);;
            for (var i = 0; i < categorias.length; i++) {
              var opt = document.createElement('option');
              opt.innerText=categorias[i].nombre_categoria;
              opt.value=categorias[i].id_categoria;
              document.getElementById('sel_divisa_ingresos_egresos').append(opt);
            }
            document.getElementById('sel_divisa_ingresos_egresos').onchange=function(){
              consultar_precios_relacion();
            }
        },

        error:function(){
          alert('ocurrio un error')
        },

    });
  //
}
function consultar_precios_relacion(){
  var div_1=document.getElementById('sel_divisa_ingresos_egresos').value;
  var div_2=1;
  var cedula=document.getElementById('cedula_cajero_login').innerText;
  var hoy = new Date();
  var fecha = hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  $.ajax({
        url:"consultar_relacion",
        type:'Post',
        data:{div1:div_1,div2:div_2,cedula:cedula,fecha:fecha},
        success:function(respuesta){
          $('#sel_precios_ingresos_egresos').html('');
          $('#modal_compra').html('');
          if (respuesta=='false') {
          } else {
            var rel=JSON.parse(respuesta);
            console.log(rel);
            var opt0 = document.createElement('option');
            opt0.innerText='opciones compra';
            $('#sel_precios_ingresos_egresos').append(opt0);
            for (var i = 0; i < rel.length; i++) {
              var opt = document.createElement('option');
              //opt.value=rel[i].precio_base;
              opt.value=JSON.stringify(rel[i]);
              opt.innerText=rel[i].precio_base;
              $('#sel_precios_ingresos_egresos').append(opt);
            }

          }
        },

        error:function(){

        },

    });
}
function guardar_ingreso_egreso(element){
  //console.log(document.getElementById('cedula_cajero_login'))
  var opt=JSON.parse(document.getElementById('sel_precios_ingresos_egresos').value);
  var obj=new Object();
  var hoy =new Date();
  var fecha=hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  obj.cajero=document.getElementById('cedula_cajero_login').innerText;
  obj.divisa=document.getElementById('sel_divisa_ingresos_egresos').value;
  obj.precio=opt.precio_base;
  obj.valor=document.getElementById('input_ingreso_egreso').value.replace(/[,]/g,'');
  obj.tipo=element.dataset.tipo;
  obj.fecha=fecha;
  if (obj.cajero!=''||obj.divisa!=''||obj.valor!=''||obj.precio!='') {
    console.log(opt)
    if (obj.tipo=='egreso') {
      if (obj.divisa==1) {
        var hoy = new Date();
        var fecha =hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
        $.ajax({
          url:"informe_cajero",
          type:"Post",
          data:{cajero:obj.cajero,fecha:fecha},
          success:function(respuesta){
            var objetos=JSON.parse(respuesta);
            //document.getElementById('contenedor_informe_cajeros').innerHTML=''
            var pesos_compra=0;var pesos_venta=0;
            for (var i = 0; i < objetos[0].length; i++) {
              if(objetos[0][i].tipo=='compra'){
                pesos_compra+=parseFloat(objetos[0][i].total_tra_div);
              }
              else{
                pesos_venta+=parseFloat(objetos[0][i].cantidad_div);
              }
            }
            var parcial_pesos=parseFloat(opt.cantidad)+pesos_venta-pesos_compra
            if (parcial_pesos<obj.valor) {
              alert('La cantidad excece a la cantidad en tú base diaria para este egreso, no se generó solicitud')
              return;
            }
          },
        });
      } else {
        if (parseFloat(opt.cantidad)<obj.valor) {
          alert('La cantidad excece a la cantidad en tú base diaria para este egreso, no se generó solicitud')
          return;
        }
      }
    }
    $.ajax({
          url:"insertar_solicitud_cajero",
          type:'Post',
          data:{obj:obj},
          success:function(respuesta){
            if (respuesta=='true') {
              alert('se genero solicitud correctamente')
            } else {
              alert('NO se genero solicitud');
            }
          },

          error:function(){
            alert('ocurrio un error')
          },

      });
  } else {
    alert('!revisa los valores¡');
  }

}
function prueba_formulario_18_rep(element){
  //console.log(element.dataset.info);
  var factura = JSON.parse(element.dataset.objeto);
  $('#modal_formulario_18_rep').modal('show');
  document.getElementById('fecha_hoy_18_rep').innerText=factura.fecha_div;
  document.getElementById('identificacion_cliente_18_rep').innerText=factura.ced_cliente_div;
  document.getElementById('nombre_cliente_18_rep').innerText=factura.nombre_cliente_div;
  document.getElementById('tasa_cambio_18_rep').innerText=factura.precio_uni_div;
  document.getElementById('monto_negociado_18_rep').innerText=factura.cantidad_div;
  document.getElementById('moneda_negociada_18_rep').innerText=factura.nombre_categoria;
  document.getElementById('total_18_rep').innerText=factura.total_tra_div;
  document.getElementById('input_efectivo_18_rep').value=factura.total_tra_div;

}
function imprimir_formulario_18_rep(){
  $('#modal_body_formulario_18_rep').printThis();
}
function formulario_2_rep(element){
  var factura = JSON.parse(element.dataset.objeto);
  $('#modal_formulario_2_rep').modal('show');
  //var factura = JSON.parse(element.dataset.info);
  var nombre=factura.nombre_cliente_div.split(' ');
  //console.log(nombre);
  document.getElementById('fecha_hoy_536_rep').innerText=factura.fecha_div;
  document.getElementById('cedula_cliente_536_rep').innerText=factura.ced_cliente_div;
  if (nombre.length==2) {
    document.getElementById('primer_apellido_536_rep').innerText=nombre[1];
    document.getElementById('segundo_appellido_536_rep').innerText='';
    document.getElementById('segundo_nombre_536_rep').innerText='';
  } else {
    if (nombre.length==3) {
      document.getElementById('primer_apellido_536_rep').innerText=nombre[nombre.length-2];
      document.getElementById('segundo_appellido_536_rep').innerText=nombre[nombre.length-1];
      document.getElementById('segundo_nombre_536_rep').innerText='';
      //
    } else {
      document.getElementById('primer_apellido_536_rep').innerText=nombre[nombre.length-2];
      document.getElementById('segundo_appellido_536_rep').innerText=nombre[nombre.length-1];
      var otros_n=''
      for (var i = 1; i < nombre.length-2; i++) {
        otros_n+=nombre[i]+' '
      }
      document.getElementById('segundo_nombre_536_rep').innerText=otros_n;
    }
  }
  document.getElementById('primer_nombre_536_rep').innerText=nombre[0];
  document.getElementById('monto_negociado_536_rep').innerText=factura.cantidad_div;
  document.getElementById('moneda_negociada_536_rep').innerText=factura.nombre_categoria;
  document.getElementById('valor_pesos_536_rep').innerText=factura.total_tra_div;
  var hoy_año=new Date();
  document.getElementById('año_536_rep').innerText=hoy_año.getFullYear();
}
function imprimir_formulario_536_rep(){
  $('#modal_body_formulario_2_rep').printThis();

}
function mostar_informe_cajero_general(){
  $('#modal_informe_cajeros_general').modal('show');
  var obj=new Object()
  var hoy = new Date();
  obj.fecha = hoy.getFullYear()+'/'+(hoy.getMonth()+1)+'/'+hoy.getDate();
  obj.cajero=document.getElementById('cedula_cajero_login').innerText;
  //obj.cedula = 87718975;
  //obj.fecha=document.getElementById('cajeros_fecha_input_general').value;
  //obj.cajero=document.getElementById('cajeros_select_modal_general').value;
  $.ajax({
    url:"cajeros_general_transacciones",
    type:"Post",
    data:{obj:obj},
    success:function(respuesta){
      document.getElementById('contenedor_informe_cajeros_general').innerHTML=''
      var objts=JSON.parse(respuesta)
      var total=0
      for (var i = 0; i < objts.length; i++) {
        total+=parseInt(objts[i].total_tra_gen);
        var tr=document.createElement('tr');
        var td1=document.createElement('td');td1.innerText=objts[i].nombre_gen
        var td2=document.createElement('td');td2.innerText=objts[i].nombre_general
        var td3=document.createElement('td');td3.innerText=objts[i].precio_uni_gen
        var td4=document.createElement('td');td4.innerText=objts[i].cantidad_gen
        var td5=document.createElement('td');td5.innerText=objts[i].total_tra_gen
        tr.append(td1,td2,td3,td4,td5);
        document.getElementById('contenedor_informe_cajeros_general').append(tr);
      }
      var trf=document.createElement('tr');
      var tdf=document.createElement('td');tdf.colSpan='5';tdf.innerText='TOTAL: '+total;
      trf.append(tdf);
      document.getElementById('contenedor_informe_cajeros_general').append(trf);
    },
  });
}
function herramienta_contadora(){
  $('#modal_contador_herramienta').modal('show');
  document.getElementById('contenedor_contador_herramienta').innerHTML='';
  //var total=0;
  for (var i = 0; i <=10; i++) {
    var tr=document.createElement('tr');
    var td1=document.createElement('td');var td2=document.createElement('td');var td3=document.createElement('td');td3.id='tot_h'+i
    var input1=document.createElement('input');input1.className='form-control';input1.value='';input1.id='den_h'+i;input1.placeholder='Denomincaion'
    var input2=document.createElement('input');input2.className='form-control';input2.setAttribute('data-j',i);input2.setAttribute('data-total',0);input2.id='cant_h'+i
    td1.append(input1);td2.append(input2);tr.append(td1,td2,td3);document.getElementById('contenedor_contador_herramienta').append(tr);
    input2.onchange=function(){
      if (document.getElementById('den_h'+this.dataset.j).value=='') {
        alert('ingresa al denominación primero')
      } else {
        var total=document.getElementById('den_h'+this.dataset.j).value*this.value;
        document.getElementById('tot_h'+this.dataset.j).innerText=total;
        this.dataset.total=total;
        //alert(total)
      }
    }
  }
  //console.log(total);
}
function sumar_herramienta(){
  var total=0;
  for (var i = 0; i <= 10; i++) {
    var obj=document.getElementById('cant_h'+i);
    total+=parseFloat(obj.dataset.total)
  }
  alert('total sumado: '+total);
}
