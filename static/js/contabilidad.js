//$(document).ready(contabilidad);
function contabilidad(){
  $.ajax({
        url:"caja_general",
        type:'Post',
        data:{},
        beforeSend:function(){
        	//
        },
        success:function(respuesta){
            var total=0;
            var datos=JSON.parse(respuesta);
            //var opt = document.createElement('option');
            for (var i = 0; i < datos.length; i++) {
              var tr = document.createElement('tr');var tr1 = document.createElement('tr');var tr2 = document.createElement('tr');
              var td1 = document.createElement('td');var td2 = document.createElement('td');var td3 = document.createElement('td');var td4 = document.createElement('td');var td0 = document.createElement('td');
              td0.innerText=datos[i].hora_conta;td1.innerText=datos[i].fecha;td2.innerText=datos[i].tran_tipo;
              var span=document.createElement('sapan');
              span.innerText=datos[i].ganancia;
              td3.innerText=datos[i].debito;td4.append(tr1,tr2);
              if (datos[i].ganancia>0) {
                span.className='badge badge-success';
              } else {
                span.className='badge badge-danger';
              }
              tr1.innerText=datos[i].credito;tr2.append(span)
              // if (datos[i].tran_tipo=='DIVISAS' && parseFloat(datos[i].ganancia) <=0 ) {
              //   td4.innerText=datos[i].credito;td3.append(tr1,tr2);
              //   span.className='badge badge-success';
              //   datos[i].ganancia=parseFloat(datos[i].ganancia)*(-1);
              //   span.innerText=datos[i].ganancia;
              //   tr1.innerText=datos[i].debito;tr2.append(span)
              // } else {
              //   span.innerText=datos[i].ganancia;
              //   td3.innerText=datos[i].debito;td4.append(tr1,tr2);
              //   if (datos[i].ganancia>0) {
              //     span.className='badge badge-success';
              //   } else {
              //     span.className='badge badge-danger';
              //   }
              //   tr1.innerText=datos[i].credito;tr2.append(span)
              // }
              total+=parseFloat(datos[i].ganancia);

              tr.append(td1,td0,td2,td3,td4);
              document.getElementById('body_tabla').append(tr);
            }
            document.getElementById('ganancia').innerText=total
        },

        error:function(){
          alert('ocurrio algo mientras se cargaban los datos presiona f5');
        },

    });
}
function report_cont(){
  var inicio=document.getElementById('fecha_inicio').value,
      fin=document.getElementById('fecha_fin').value;
  document.getElementById('body_tabla').innerHTML='';
  if ((inicio==''&&fin=='')||(inicio==''&&fin!='')||(inicio!=''&&fin=='')||(inicio!=''&&fin!=''&&inicio<=fin)) {
    //inicio=inicio.replace(/[/]/g,'-');fin=fin.replace(/[/]/g,'-');
    //alert(inicio+'<>'+fin)
    consultar_datos_cont(inicio,fin)
  } else {
    alert('La fecha fin debe ser mayor a la fecha de inicio de buscqueda');
  }
}
function consultar_datos_cont(inicio,fin){
  $.ajax({    
    url:"consultar_conta",
    type:'Post',
    data:{inicio:inicio,fin:fin},
    success:function(respuesta){
      var total=0;
      var datos=JSON.parse(respuesta);
      //var opt = document.createElement('option');
      for (var i = 0; i < datos.length; i++) {
        var tr = document.createElement('tr');var tr1 = document.createElement('tr');var tr2 = document.createElement('tr');
        var td1 = document.createElement('td');var td2 = document.createElement('td');var td3 = document.createElement('td');var td4 = document.createElement('td');var td0=document.createElement('td');
        td0.innerText=datos[i].hora_conta;td1.innerText=datos[i].fecha;td2.innerText=datos[i].tran_tipo;
        var span=document.createElement('sapan');
        span.innerText=datos[i].ganancia;
        td3.innerText=datos[i].debito;td4.append(tr1,tr2);
        if (datos[i].ganancia>0) {
          span.className='badge badge-success';
        } else {
          span.className='badge badge-danger';
        }
        // if (datos[i].tran_tipo=='DIVISAS' && parseFloat(datos[i].ganancia) <=0 ) {
        //   td4.innerText=datos[i].debito;td3.append(tr1,tr2);
        //   span.className='badge badge-success';
        //   datos[i].ganancia=parseFloat(datos[i].ganancia)*(-1);
        //   span.innerText=datos[i].ganancia;
        // } else {
        //   span.innerText=datos[i].ganancia;
        //   td3.innerText=datos[i].debito;td4.append(tr1,tr2);
        //   if (datos[i].ganancia>0) {
        //     span.className='badge badge-success';
        //   } else {
        //     span.className='badge badge-danger';
        //   }
        // }
        total+=parseFloat(datos[i].ganancia);
        tr1.innerText=datos[i].credito;tr2.append(span)
        tr.append(td1,td0,td2,td3,td4);
        document.getElementById('body_tabla').append(tr);
      }
      document.getElementById('ganancia').innerText=total
    },
    error:function(){

    },
  });
}
function consultar_datos_cont2(inicio,fin){
  $.ajax({

    url:"consultar_transaciones_divisas",
    type:'Post',
    data:{inicio:inicio,fin:fin},
    success:function(respuesta){
      var total=0;
      var datos=JSON.parse(respuesta);
      //var opt = document.createElement('option');
      for (var i = 0; i < datos.length; i++) {
        var tr = document.createElement('tr');var tr1 = document.createElement('tr');var tr2 = document.createElement('tr');
        var td1 = document.createElement('td');var td2 = document.createElement('td');var td3 = document.createElement('td');var td4 = document.createElement('td');var td0 = document.createElement('td');
        td1.innerText=datos[i].fecha;td0.innerText=datos[i].hora_conta;td2.innerText=datos[i].tipo;
        var span=document.createElement('span');
        //span.innerText=datos[i].ganancia;
        span.innerText=datos[i].ganancia.toFixed(2);
        td3.innerText=datos[i].debito;td4.append(tr1,tr2);
        if (datos[i].ganancia>0) {
          span.className='badge badge-success';
        } else {
          span.className='badge badge-danger';
        }
        total+=parseFloat(datos[i].ganancia);
        tr1.innerText=datos[i].credito;tr2.append(span)
        tr.append(td1,td0,td2,td3,td4);
        document.getElementById('body_tabla').append(tr);
      }
      document.getElementById('ganancia').innerText=total
    },
    error:function(){

    },
  });
}
function seleccion_contabilidad(element){
  var fecha_inicio=document.getElementById('fecha_inicio').value;
  var fecha_fin=document.getElementById('fecha_fin').value;
  if ((fecha_inicio=='')||(fecha_inicio > fecha_fin)||(fecha_fin=='')) {
    alert('Recuerda que debes seleccionar un rango valido ')
    return
  }
  $.ajax({
        url:"seleccion_contabilidad",
        type:'Post',
        data:{tipo:element.dataset.tipo,fecha_inicio:fecha_inicio,fecha_fin:fecha_fin},
        beforeSend:function(){
        	//
        },
        success:function(respuesta){
            var total=0;
            var datos=JSON.parse(respuesta);
            document.getElementById('body_tabla').innerHTML=''
            //var opt = document.createElement('option');
            for (var i = 0; i < datos.length; i++) {
              var tr = document.createElement('tr');var tr1 = document.createElement('tr');var tr2 = document.createElement('tr');
              var td1 = document.createElement('td');var td2 = document.createElement('td');var td3 = document.createElement('td');var td4 = document.createElement('td');var td0=document.createElement('td');
              td0.innerText=datos[i].hora_conta;td1.innerText=datos[i].fecha;td2.innerText=datos[i].tran_tipo;
              var span=document.createElement('sapan');
              span.innerText=parseFloat(datos[i].ganancia).toFixed(2);
              td3.innerText=parseFloat(datos[i].debito).toFixed(2);td4.append(tr1,tr2);
              if (datos[i].ganancia>0) {
                span.className='badge badge-success';
              } else {
                span.className='badge badge-danger';
              }
              total+=parseFloat(datos[i].ganancia);
              tr1.innerText=parseFloat(datos[i].credito).toFixed(2);tr2.append(span)
              tr.append(td1,td0,td2,td3,td4);
              document.getElementById('body_tabla').append(tr);
            }
            document.getElementById('ganancia').innerText=parseFloat(total).toFixed(2)

        },

        error:function(){
          alert('ocurrio algo mientras se cargaban los datos presiona f5');
        },

    });
}
