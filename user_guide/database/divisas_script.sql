--
-- PostgreSQL database dump
--

-- Dumped from database version 11.12
-- Dumped by pg_dump version 11.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bancos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bancos (
    valor_bancos numeric
);


ALTER TABLE public.bancos OWNER TO postgres;

--
-- Name: base_diaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.base_diaria (
    fecha_base date NOT NULL,
    hora_transaccion time without time zone NOT NULL,
    divisa integer NOT NULL,
    cantidad numeric,
    cajero character varying NOT NULL,
    precio_base numeric NOT NULL,
    precio_venta1 numeric,
    precio_venta2 numeric,
    precio_venta3 numeric,
    ingreso numeric,
    egreso numeric,
    cantidad_inicial numeric
);


ALTER TABLE public.base_diaria OWNER TO postgres;

--
-- Name: caja_general; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.caja_general (
    id_caja_general bigint NOT NULL,
    fecha date NOT NULL,
    tran_tipo text,
    debito numeric,
    credito numeric,
    ganancia numeric,
    cajero_conta character varying,
    hora_conta time with time zone
);


ALTER TABLE public.caja_general OWNER TO postgres;

--
-- Name: caja_general_id_caja_general_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.caja_general_id_caja_general_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.caja_general_id_caja_general_seq OWNER TO postgres;

--
-- Name: caja_general_id_caja_general_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.caja_general_id_caja_general_seq OWNED BY public.caja_general.id_caja_general;


--
-- Name: cajero; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cajero (
    id_cajero integer NOT NULL,
    nombre_cajero text NOT NULL,
    cedula_cajero character varying NOT NULL,
    telefono character varying NOT NULL
);


ALTER TABLE public.cajero OWNER TO postgres;

--
-- Name: cajero_id_cajero_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cajero_id_cajero_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cajero_id_cajero_seq OWNER TO postgres;

--
-- Name: cajero_id_cajero_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cajero_id_cajero_seq OWNED BY public.cajero.id_cajero;


--
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoria (
    id_categoria integer NOT NULL,
    nombre_categoria text
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- Name: categoria_id_categoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categoria_id_categoria_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoria_id_categoria_seq OWNER TO postgres;

--
-- Name: categoria_id_categoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categoria_id_categoria_seq OWNED BY public.categoria.id_categoria;


--
-- Name: clientes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clientes (
    id_cliente integer NOT NULL,
    doc_identidad character varying NOT NULL,
    nombre text,
    telefono character varying,
    pais_origen text,
    correo character varying
);


ALTER TABLE public.clientes OWNER TO postgres;

--
-- Name: clientes_id_cliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clientes_id_cliente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_id_cliente_seq OWNER TO postgres;

--
-- Name: clientes_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clientes_id_cliente_seq OWNED BY public.clientes.id_cliente;


--
-- Name: general; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.general (
    id_general integer NOT NULL,
    nombre_general text NOT NULL
);


ALTER TABLE public.general OWNER TO postgres;

--
-- Name: general_id_general_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.general_id_general_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.general_id_general_seq OWNER TO postgres;

--
-- Name: general_id_general_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.general_id_general_seq OWNED BY public.general.id_general;


--
-- Name: objeto_general; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.objeto_general (
    id_objeto_gen integer NOT NULL,
    nombre_objeto_gen text NOT NULL,
    cantidad_gen integer NOT NULL,
    precio_obj_gen_venta integer NOT NULL,
    precio_obj_gen_compra integer NOT NULL,
    categoria_gen integer NOT NULL
);


ALTER TABLE public.objeto_general OWNER TO postgres;

--
-- Name: objeto_general_id_objeto_gen_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.objeto_general_id_objeto_gen_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.objeto_general_id_objeto_gen_seq OWNER TO postgres;

--
-- Name: objeto_general_id_objeto_gen_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.objeto_general_id_objeto_gen_seq OWNED BY public.objeto_general.id_objeto_gen;


--
-- Name: objeto_seguro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.objeto_seguro (
    id_objeto_seg integer NOT NULL,
    nombre_objeto_seg text NOT NULL,
    cantidad_seg integer NOT NULL,
    precio_obj_venta integer NOT NULL,
    precio_obj_compra integer NOT NULL,
    categoria_seg integer NOT NULL
);


ALTER TABLE public.objeto_seguro OWNER TO postgres;

--
-- Name: objeto_seguro_id_objeto_seg_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.objeto_seguro_id_objeto_seg_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.objeto_seguro_id_objeto_seg_seq OWNER TO postgres;

--
-- Name: objeto_seguro_id_objeto_seg_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.objeto_seguro_id_objeto_seg_seq OWNED BY public.objeto_seguro.id_objeto_seg;


--
-- Name: objetos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.objetos (
    id_objeto integer NOT NULL,
    nombre_objeto text NOT NULL,
    cantidad integer NOT NULL,
    categoria integer NOT NULL
);


ALTER TABLE public.objetos OWNER TO postgres;

--
-- Name: objetos_id_objeto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.objetos_id_objeto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.objetos_id_objeto_seq OWNER TO postgres;

--
-- Name: objetos_id_objeto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.objetos_id_objeto_seq OWNED BY public.objetos.id_objeto;


--
-- Name: precios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.precios (
    id_precio integer NOT NULL,
    precio_venta1 integer,
    precio_venta2 integer,
    precio_venta3 integer,
    precio_compra1 integer,
    precio_compra2 integer,
    precio_compra3 integer,
    categoria_origen integer NOT NULL,
    categoria_destino integer NOT NULL,
    precio_base numeric NOT NULL,
    cantidad_base numeric
);


ALTER TABLE public.precios OWNER TO postgres;

--
-- Name: precios_id_precio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.precios_id_precio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.precios_id_precio_seq OWNER TO postgres;

--
-- Name: precios_id_precio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.precios_id_precio_seq OWNED BY public.precios.id_precio;


--
-- Name: prestamos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.prestamos (
    id_prestamo bigint NOT NULL,
    fecha date NOT NULL,
    nombre_prestamo text NOT NULL,
    cedula_prestamo character varying NOT NULL,
    estado character varying(1) NOT NULL,
    cantidad_prestamo numeric NOT NULL,
    divisa integer,
    abono numeric
);


ALTER TABLE public.prestamos OWNER TO postgres;

--
-- Name: prestamos_id_prestamo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.prestamos_id_prestamo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prestamos_id_prestamo_seq OWNER TO postgres;

--
-- Name: prestamos_id_prestamo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.prestamos_id_prestamo_seq OWNED BY public.prestamos.id_prestamo;


--
-- Name: seguros; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seguros (
    id_seguros integer NOT NULL,
    nombre_seguros text NOT NULL
);


ALTER TABLE public.seguros OWNER TO postgres;

--
-- Name: seguros_id_seguros_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seguros_id_seguros_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seguros_id_seguros_seq OWNER TO postgres;

--
-- Name: seguros_id_seguros_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seguros_id_seguros_seq OWNED BY public.seguros.id_seguros;


--
-- Name: solicitud; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud (
    cajero_sol character varying,
    divisa_sol integer,
    fecha_sol date,
    precio numeric,
    valor_sol numeric,
    tipo_sol character varying,
    estado_sol character(1),
    id_sol bigint NOT NULL
);


ALTER TABLE public.solicitud OWNER TO postgres;

--
-- Name: solicitud_id_sol_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_id_sol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_id_sol_seq OWNER TO postgres;

--
-- Name: solicitud_id_sol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_id_sol_seq OWNED BY public.solicitud.id_sol;


--
-- Name: transacciones_divisas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transacciones_divisas (
    fecha_div date NOT NULL,
    hora_div time without time zone NOT NULL,
    div1 integer,
    div2 integer,
    cantidad_div numeric,
    precio_uni_div numeric,
    precio_base_div numeric,
    total_tra_div numeric,
    ced_cajero_div character varying NOT NULL,
    nombre_cajero_div text,
    ced_cliente_div character varying,
    nombre_cliente_div text,
    tipo character varying
);


ALTER TABLE public.transacciones_divisas OWNER TO postgres;

--
-- Name: transacciones_general; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transacciones_general (
    id_trans_gen bigint NOT NULL,
    fecha_gen date,
    hora_gen time without time zone,
    nombre_gen text,
    categoria_gen integer,
    cantidad_gen integer,
    precio_uni_gen numeric,
    precio_base_gen numeric,
    total_tra_gen numeric,
    ced_cajero_gen character varying,
    nombre_cajero_gen text,
    ced_cliente_gen character varying,
    nombre_cliente_gen text
);


ALTER TABLE public.transacciones_general OWNER TO postgres;

--
-- Name: transacciones_general_id_trans_gen_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transacciones_general_id_trans_gen_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transacciones_general_id_trans_gen_seq OWNER TO postgres;

--
-- Name: transacciones_general_id_trans_gen_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transacciones_general_id_trans_gen_seq OWNED BY public.transacciones_general.id_trans_gen;


--
-- Name: transacciones_seguros; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transacciones_seguros (
    fecha_seg date NOT NULL,
    hora_seg time without time zone NOT NULL,
    nombre_seg text,
    categoria_seg integer,
    placa character varying,
    cantidad_seg integer,
    precio_uni_seg numeric,
    precio_base_seg numeric,
    total_tra_seg numeric,
    ced_cajero_seg character varying NOT NULL,
    nombre_cajero_seg text,
    ced_cliente_seg character varying,
    nombre_cliente_seg text
);


ALTER TABLE public.transacciones_seguros OWNER TO postgres;

--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    id integer NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    cedula character varying NOT NULL,
    clave text NOT NULL,
    tipo character(1)
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO postgres;

--
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- Name: caja_general id_caja_general; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.caja_general ALTER COLUMN id_caja_general SET DEFAULT nextval('public.caja_general_id_caja_general_seq'::regclass);


--
-- Name: cajero id_cajero; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cajero ALTER COLUMN id_cajero SET DEFAULT nextval('public.cajero_id_cajero_seq'::regclass);


--
-- Name: categoria id_categoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria ALTER COLUMN id_categoria SET DEFAULT nextval('public.categoria_id_categoria_seq'::regclass);


--
-- Name: clientes id_cliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes ALTER COLUMN id_cliente SET DEFAULT nextval('public.clientes_id_cliente_seq'::regclass);


--
-- Name: general id_general; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.general ALTER COLUMN id_general SET DEFAULT nextval('public.general_id_general_seq'::regclass);


--
-- Name: objeto_general id_objeto_gen; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objeto_general ALTER COLUMN id_objeto_gen SET DEFAULT nextval('public.objeto_general_id_objeto_gen_seq'::regclass);


--
-- Name: objeto_seguro id_objeto_seg; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objeto_seguro ALTER COLUMN id_objeto_seg SET DEFAULT nextval('public.objeto_seguro_id_objeto_seg_seq'::regclass);


--
-- Name: objetos id_objeto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objetos ALTER COLUMN id_objeto SET DEFAULT nextval('public.objetos_id_objeto_seq'::regclass);


--
-- Name: precios id_precio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.precios ALTER COLUMN id_precio SET DEFAULT nextval('public.precios_id_precio_seq'::regclass);


--
-- Name: prestamos id_prestamo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prestamos ALTER COLUMN id_prestamo SET DEFAULT nextval('public.prestamos_id_prestamo_seq'::regclass);


--
-- Name: seguros id_seguros; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seguros ALTER COLUMN id_seguros SET DEFAULT nextval('public.seguros_id_seguros_seq'::regclass);


--
-- Name: solicitud id_sol; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud ALTER COLUMN id_sol SET DEFAULT nextval('public.solicitud_id_sol_seq'::regclass);


--
-- Name: transacciones_general id_trans_gen; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_general ALTER COLUMN id_trans_gen SET DEFAULT nextval('public.transacciones_general_id_trans_gen_seq'::regclass);


--
-- Name: usuarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- Data for Name: bancos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bancos (valor_bancos) FROM stdin;
0
\.


--
-- Data for Name: base_diaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.base_diaria (fecha_base, hora_transaccion, divisa, cantidad, cajero, precio_base, precio_venta1, precio_venta2, precio_venta3, ingreso, egreso, cantidad_inicial) FROM stdin;
2022-08-18	10:39:09	2	40	654321	4040	4050	4051	4052	\N	\N	40
2022-08-18	10:39:09	2	40	654321	4035	4050	4051	4052	\N	\N	40
2022-08-18	10:39:09	2	40	654321	4030	4050	4051	4052	\N	\N	40
2022-08-18	09:11:23	2	38.3	123456	4035	4050	4055	4060	\N	\N	50
2022-08-18	09:11:23	2	50	123456	4030	4050	4055	4060	\N	\N	50
2022-08-18	09:11:23	2	0	123456	4040	4050	4055	4060	\N	\N	200
2022-08-18	09:11:53	1	400000	123456	0	0	0	0	\N	\N	400000
2022-08-18	09:44:28	5	200	123456	4500	4600	4700	4800	\N	\N	200
\.


--
-- Data for Name: caja_general; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.caja_general (id_caja_general, fecha, tran_tipo, debito, credito, ganancia, cajero_conta, hora_conta) FROM stdin;
28	2022-08-18	DIVISAS	41410	41410	0	123456	09:21:14-05
29	2022-08-18	DIVISAS	47436.25	47209.5	226.75	123456	09:31:31.73-05
30	2022-08-18	DIVISAS	852563.75	849410	3153.75	123456	09:31:31.729-05
\.


--
-- Data for Name: cajero; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cajero (id_cajero, nombre_cajero, cedula_cajero, telefono) FROM stdin;
\.


--
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categoria (id_categoria, nombre_categoria) FROM stdin;
2	DOLAR
1	PESO
5	EURO
\.


--
-- Data for Name: clientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clientes (id_cliente, doc_identidad, nombre, telefono, pais_origen, correo) FROM stdin;
4	123	general	31832132323	Colombia	general@gmail.com
\.


--
-- Data for Name: general; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.general (id_general, nombre_general) FROM stdin;
\.


--
-- Data for Name: objeto_general; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.objeto_general (id_objeto_gen, nombre_objeto_gen, cantidad_gen, precio_obj_gen_venta, precio_obj_gen_compra, categoria_gen) FROM stdin;
\.


--
-- Data for Name: objeto_seguro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.objeto_seguro (id_objeto_seg, nombre_objeto_seg, cantidad_seg, precio_obj_venta, precio_obj_compra, categoria_seg) FROM stdin;
\.


--
-- Data for Name: objetos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.objetos (id_objeto, nombre_objeto, cantidad, categoria) FROM stdin;
\.


--
-- Data for Name: precios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.precios (id_precio, precio_venta1, precio_venta2, precio_venta3, precio_compra1, precio_compra2, precio_compra3, categoria_origen, categoria_destino, precio_base, cantidad_base) FROM stdin;
13	0	0	0	0	0	0	1	1	0	100000
17	4600	4700	4800	\N	\N	\N	5	1	4500	800
14	4050	4055	4060	\N	\N	\N	2	1	4040	160
16	4040	4045	4050	\N	\N	\N	2	1	4035	160
15	4040	4045	4050	\N	\N	\N	2	1	4030	60
\.


--
-- Data for Name: prestamos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.prestamos (id_prestamo, fecha, nombre_prestamo, cedula_prestamo, estado, cantidad_prestamo, divisa, abono) FROM stdin;
4	2022-07-25	fernando burbano	1234321	A	100000	1	5000
\.


--
-- Data for Name: seguros; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.seguros (id_seguros, nombre_seguros) FROM stdin;
\.


--
-- Data for Name: solicitud; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud (cajero_sol, divisa_sol, fecha_sol, precio, valor_sol, tipo_sol, estado_sol, id_sol) FROM stdin;
987654321	2	2022-07-21	4300	100	ingreso	A	8
987654321	1	2022-07-21	0	300000	ingreso	A	9
987654321	1	2022-07-21	0	42400	egreso	A	10
987654321	2	2022-07-21	4300	10.3	egreso	A	11
\.


--
-- Data for Name: transacciones_divisas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transacciones_divisas (fecha_div, hora_div, div1, div2, cantidad_div, precio_uni_div, precio_base_div, total_tra_div, ced_cajero_div, nombre_cajero_div, ced_cliente_div, nombre_cliente_div, tipo) FROM stdin;
2022-08-18	09:21:14	2	1	10.25	4040	4040	41410	123456	Gabriel Lopez	123	GENERAL	compra
2022-08-18	09:31:31.73	2	1	47436.25	4055	4035	11.7	123456	Gabriel Lopez	123	GENERAL	venta
2022-08-18	09:31:31.729	2	1	852563.75	4055	4040	210.25	123456	Gabriel Lopez	123	GENERAL	venta
\.


--
-- Data for Name: transacciones_general; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transacciones_general (id_trans_gen, fecha_gen, hora_gen, nombre_gen, categoria_gen, cantidad_gen, precio_uni_gen, precio_base_gen, total_tra_gen, ced_cajero_gen, nombre_cajero_gen, ced_cliente_gen, nombre_cliente_gen) FROM stdin;
\.


--
-- Data for Name: transacciones_seguros; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transacciones_seguros (fecha_seg, hora_seg, nombre_seg, categoria_seg, placa, cantidad_seg, precio_uni_seg, precio_base_seg, total_tra_seg, ced_cajero_seg, nombre_cajero_seg, ced_cliente_seg, nombre_cliente_seg) FROM stdin;
\.


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios (id, nombre, apellido, cedula, clave, tipo) FROM stdin;
1	Carlos Efrain	Revelo Erira	1085901197	D1085924193	a
7	Gabriel	Lopez	123456	123456	c
8	jorge	paz	654321	654321	c
\.


--
-- Name: caja_general_id_caja_general_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.caja_general_id_caja_general_seq', 30, true);


--
-- Name: cajero_id_cajero_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cajero_id_cajero_seq', 3, true);


--
-- Name: categoria_id_categoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categoria_id_categoria_seq', 5, true);


--
-- Name: clientes_id_cliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clientes_id_cliente_seq', 4, true);


--
-- Name: general_id_general_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.general_id_general_seq', 3, true);


--
-- Name: objeto_general_id_objeto_gen_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.objeto_general_id_objeto_gen_seq', 2, true);


--
-- Name: objeto_seguro_id_objeto_seg_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.objeto_seguro_id_objeto_seg_seq', 1, true);


--
-- Name: objetos_id_objeto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.objetos_id_objeto_seq', 9, true);


--
-- Name: precios_id_precio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.precios_id_precio_seq', 17, true);


--
-- Name: prestamos_id_prestamo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.prestamos_id_prestamo_seq', 4, true);


--
-- Name: seguros_id_seguros_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seguros_id_seguros_seq', 2, true);


--
-- Name: solicitud_id_sol_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_id_sol_seq', 11, true);


--
-- Name: transacciones_general_id_trans_gen_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transacciones_general_id_trans_gen_seq', 7, true);


--
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 8, true);


--
-- Name: base_diaria base_diaria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.base_diaria
    ADD CONSTRAINT base_diaria_pkey PRIMARY KEY (fecha_base, cajero, divisa, precio_base);


--
-- Name: caja_general caja_general_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.caja_general
    ADD CONSTRAINT caja_general_pkey PRIMARY KEY (id_caja_general);


--
-- Name: cajero cajero_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cajero
    ADD CONSTRAINT cajero_pkey PRIMARY KEY (cedula_cajero);


--
-- Name: categoria categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (id_categoria);


--
-- Name: clientes clientes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_pkey PRIMARY KEY (doc_identidad);


--
-- Name: general general_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.general
    ADD CONSTRAINT general_pkey PRIMARY KEY (id_general);


--
-- Name: objeto_general objeto_general_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objeto_general
    ADD CONSTRAINT objeto_general_pkey PRIMARY KEY (nombre_objeto_gen, categoria_gen);


--
-- Name: objeto_seguro objeto_seguro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objeto_seguro
    ADD CONSTRAINT objeto_seguro_pkey PRIMARY KEY (nombre_objeto_seg, categoria_seg);


--
-- Name: objetos objetos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objetos
    ADD CONSTRAINT objetos_pkey PRIMARY KEY (nombre_objeto, categoria);


--
-- Name: usuarios pk_usuarios_cedula; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT pk_usuarios_cedula PRIMARY KEY (cedula);


--
-- Name: precios precios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.precios
    ADD CONSTRAINT precios_pkey PRIMARY KEY (categoria_origen, precio_base);


--
-- Name: prestamos prestamos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prestamos
    ADD CONSTRAINT prestamos_pkey PRIMARY KEY (id_prestamo);


--
-- Name: seguros seguros_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seguros
    ADD CONSTRAINT seguros_pkey PRIMARY KEY (id_seguros);


--
-- Name: transacciones_divisas transacciones_divisas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_divisas
    ADD CONSTRAINT transacciones_divisas_pkey PRIMARY KEY (fecha_div, hora_div, ced_cajero_div);


--
-- Name: transacciones_general transacciones_general_id_trans_gen_fecha_gen_hora_gen_ced_c_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_general
    ADD CONSTRAINT transacciones_general_id_trans_gen_fecha_gen_hora_gen_ced_c_key UNIQUE (id_trans_gen, fecha_gen, hora_gen, ced_cajero_gen);


--
-- Name: transacciones_general transacciones_general_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_general
    ADD CONSTRAINT transacciones_general_pkey PRIMARY KEY (id_trans_gen);


--
-- Name: transacciones_seguros transacciones_seguros_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_seguros
    ADD CONSTRAINT transacciones_seguros_pkey PRIMARY KEY (fecha_seg, hora_seg, ced_cajero_seg);


--
-- Name: base_diaria base_diaria_cajero_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.base_diaria
    ADD CONSTRAINT base_diaria_cajero_fkey FOREIGN KEY (cajero) REFERENCES public.usuarios(cedula);


--
-- Name: base_diaria base_diaria_divisa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.base_diaria
    ADD CONSTRAINT base_diaria_divisa_fkey FOREIGN KEY (divisa) REFERENCES public.categoria(id_categoria);


--
-- Name: objeto_general objeto_general_categoria_gen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objeto_general
    ADD CONSTRAINT objeto_general_categoria_gen_fkey FOREIGN KEY (categoria_gen) REFERENCES public.general(id_general);


--
-- Name: objeto_seguro objeto_seguro_categoria_seg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objeto_seguro
    ADD CONSTRAINT objeto_seguro_categoria_seg_fkey FOREIGN KEY (categoria_seg) REFERENCES public.seguros(id_seguros);


--
-- Name: objetos objetos_categoria_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.objetos
    ADD CONSTRAINT objetos_categoria_fkey FOREIGN KEY (categoria) REFERENCES public.categoria(id_categoria);


--
-- Name: precios precios_categoria_destino_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.precios
    ADD CONSTRAINT precios_categoria_destino_fkey FOREIGN KEY (categoria_destino) REFERENCES public.categoria(id_categoria);


--
-- Name: precios precios_categoria_origen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.precios
    ADD CONSTRAINT precios_categoria_origen_fkey FOREIGN KEY (categoria_origen) REFERENCES public.categoria(id_categoria);


--
-- Name: prestamos prestamos_divisa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prestamos
    ADD CONSTRAINT prestamos_divisa_fkey FOREIGN KEY (divisa) REFERENCES public.categoria(id_categoria);


--
-- Name: transacciones_divisas transacciones_divisas_ced_cajero_div_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_divisas
    ADD CONSTRAINT transacciones_divisas_ced_cajero_div_fkey FOREIGN KEY (ced_cajero_div) REFERENCES public.usuarios(cedula);


--
-- Name: transacciones_divisas transacciones_divisas_ced_cliente_div_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_divisas
    ADD CONSTRAINT transacciones_divisas_ced_cliente_div_fkey FOREIGN KEY (ced_cliente_div) REFERENCES public.clientes(doc_identidad);


--
-- Name: transacciones_divisas transacciones_divisas_div1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_divisas
    ADD CONSTRAINT transacciones_divisas_div1_fkey FOREIGN KEY (div1) REFERENCES public.categoria(id_categoria);


--
-- Name: transacciones_divisas transacciones_divisas_div2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_divisas
    ADD CONSTRAINT transacciones_divisas_div2_fkey FOREIGN KEY (div2) REFERENCES public.categoria(id_categoria);


--
-- Name: transacciones_general transacciones_general_categoria_gen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_general
    ADD CONSTRAINT transacciones_general_categoria_gen_fkey FOREIGN KEY (categoria_gen) REFERENCES public.general(id_general);


--
-- Name: transacciones_general transacciones_general_ced_cajero_gen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_general
    ADD CONSTRAINT transacciones_general_ced_cajero_gen_fkey FOREIGN KEY (ced_cajero_gen) REFERENCES public.usuarios(cedula);


--
-- Name: transacciones_general transacciones_general_ced_cliente_gen_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_general
    ADD CONSTRAINT transacciones_general_ced_cliente_gen_fkey FOREIGN KEY (ced_cliente_gen) REFERENCES public.clientes(doc_identidad);


--
-- Name: transacciones_seguros transacciones_seguros_categoria_seg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_seguros
    ADD CONSTRAINT transacciones_seguros_categoria_seg_fkey FOREIGN KEY (categoria_seg) REFERENCES public.seguros(id_seguros);


--
-- Name: transacciones_seguros transacciones_seguros_ced_cajero_seg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_seguros
    ADD CONSTRAINT transacciones_seguros_ced_cajero_seg_fkey FOREIGN KEY (ced_cajero_seg) REFERENCES public.usuarios(cedula);


--
-- Name: transacciones_seguros transacciones_seguros_ced_cliente_seg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transacciones_seguros
    ADD CONSTRAINT transacciones_seguros_ced_cliente_seg_fkey FOREIGN KEY (ced_cliente_seg) REFERENCES public.clientes(doc_identidad);


--
-- PostgreSQL database dump complete
--

