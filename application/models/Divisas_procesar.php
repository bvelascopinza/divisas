<?php
defined('BASEPATH') or exit('No direct script access allowed');
//echo "entro";

class Divisas_procesar extends CI_Model
{
	function __construct()
	{

		parent::__construct();

	}
	public function nueva_divisa($divisa){
		$dato=array(
		  'nombre_categoria' => $divisa
		);
		$this->db->INSERT('categoria',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
	}
  public function categorias_divisas(){
    $this->db->SELECT('*');
		$this->db->from('categoria');
		$this->db->order_by('nombre_categoria','asc');
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
	public function categorias_seguros(){
    $this->db->SELECT('*');
		$this->db->from('seguros');
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
	public function categorias_general(){
    $this->db->SELECT('*');
		$this->db->from('general');
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
	public function categorias_menos($opcion){
    $this->db->SELECT('*');
		$this->db->from('categoria');
		$this->db->where('id_categoria !=',$opcion);
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
	public function seguros_objetos($opcion){
		//print_r($opcion);
    $this->db->SELECT('*');
		$this->db->from('objeto_seguro');
		$this->db->where('categoria_seg',$opcion);
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
	public function general_objetos($opcion){
    $this->db->SELECT('*');
		$this->db->from('objeto_general');
		$this->db->where('categoria_gen',$opcion);
		$this->db->order_by('nombre_objeto_gen',$opcion);
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
  public function objetos($categoria){
    $this->db->SELECT('*');
		$this->db->from('objeto');
    $this->db->where('categoria',$categoria);
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
  public function cliente_unico($cedula){
    $this->db->SELECT('*');
		$this->db->from('clientes');
    $this->db->where('doc_identidad',$cedula);
		$resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
      return $resultado->result_array();
    }


  }
	public function inv_agregar_relacion($objeto){
		$dato=array(
			'cantidad_base' => $objeto['cant_base'],
			'precio_base' => $objeto['base'],
		  	'precio_venta1' => $objeto['v1'],
		 	'precio_venta2' => $objeto['v2'],
			'precio_venta3' => $objeto['v3'],
			// 'precio_compra1' => $objeto['c1'],
			// 'precio_compra2' => $objeto['c2'],
			// 'precio_compra3' => $objeto['c3'],
		 	'categoria_origen' => $objeto['origen'],
      		'categoria_destino' => $objeto['destino']
		);
		$this->db->INSERT('precios',$dato);
		if($this->db->affected_rows()>0){
			//$id_max=$this->ultima_pregunta();
			// $dato1=array(
			// 	'cantidad_base' => $objeto['cant_base'],
			// 	'precio_base' => $objeto['base'],
			//   'precio_venta1' => $objeto['v1'],
			//  	'precio_venta2' => $objeto['v2'],
			// 	'precio_venta3' => $objeto['v3'],
			// 	// 'precio_compra1' => $objeto['c1'],
			// 	// 'precio_compra2' => $objeto['c2'],
			// 	// 'precio_compra3' => $objeto['c3'],
			//  	'categoria_origen' => $objeto['destino'],
	    //   'categoria_destino' => $objeto['origen']
			// );
			// $this->db->INSERT('precios',$dato1);
			return true;
		}else{
			return  'false';
		}
	}
	public function inv_agregar_relacion2($objeto){
		$dato=array(
			'cantidad' => 0,
			'cantidad_inicial' => 0,
			'precio_base' => $objeto['base'],
		  	'precio_venta1' => $objeto['v1'],
		 	'precio_venta2' => $objeto['v2'],
			'precio_venta3' => $objeto['v3'],
			'fecha_base' => $objeto['fecha'],
			'hora_transaccion' => $objeto['time'],
			'cajero' => $objeto['cajero'],
		 	'divisa' => $objeto['origen']
		);
		$this->db->INSERT('base_diaria',$dato);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return  false;
		}
	}
	function consultar_relacion_precios($div_1,$div_2){
		$this->db->SELECT('*');
		$this->db->from('precios');
    $this->db->where('categoria_origen',$div_1);
		$this->db->where('categoria_destino',$div_2);
		$resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
			return $resultado->result_array();
    }
	}
	public function consultar_relacion($div_1,$div_2,$cedula,$fecha){
		$this->db->SELECT('*');
		$this->db->from('base_diaria');
    $this->db->where('divisa',$div_1);
		$this->db->where('cajero',$cedula);
		//$this->db->where('fecha_base',$fecha);
		$this->db->order_by('precio_base','desc');
		$resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
			return $resultado->result_array();
    }
		//consultar_relacion vieja
		/*
		$this->db->SELECT('*');
		$this->db->from('precios');
    $this->db->where('categoria_origen',$div_1);
		$this->db->where('categoria_destino',$div_2);
		$this->db->order_by('precio_base','desc');
		$this->db->or_where('categoria_origen',$div_2);
		$this->db->where('categoria_destino',$div_1);
		$this->db->order_by('precio_base','desc');
		$resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
			return $resultado->result_array();
    }
		*/
		//
	}
	public function consultar_relacion2($div_1,$div_2,$cedula,$fecha){
		$this->db->SELECT('*');
		$this->db->from('base_diaria');
		$this->db->join('categoria','categoria.id_categoria=base_diaria.divisa');
    	$this->db->where('divisa',$div_1);
		$this->db->where('cajero',$cedula);
		$this->db->where('fecha_base',$fecha);
		$this->db->where('cantidad >',0);
		$this->db->order_by('precio_base','desc');
		$resultado = $this->db->get();
		if ($resultado->num_rows()==0) {
			return false;
		}else{
			return $resultado->result_array();
		}
	}
	public function nuevo_cliente($cliente){
		//print_r($cliente);
		$dato=array(
		  'doc_identidad' => $cliente['doc'],
		 	'nombre' => $cliente['nombre'],
			'telefono' => $cliente['telefono'],
			'pais_origen' => $cliente['pais'],
			'correo' => $cliente['email']
		);
		$this->db->INSERT('clientes',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			//return $resultado->result_array();
		}
	}
	//
	//si algo los parametros con los datos de recios de compra era: ($origen,$compra,$venta,$base,$cant_base,$cant_base_ant)
	public function actualizar_relacion($origen,$venta,$base,$cant_base,$cant_base_ant){
		//print_r($base);
		$this->db->set('cantidad_base', $cant_base);
		//$this->db->set('precio_base', $base);
		$this->db->set('precio_venta1', $venta[0]);
		$this->db->set('precio_venta2', $venta[1]);
		$this->db->set('precio_venta3', $venta[2]);
		// $this->db->set('precio_compra1', $compra[0]);
		// $this->db->set('precio_compra2', $compra[1]);
		// $this->db->set('precio_compra3', $compra[2]);
		$this->db->where('categoria_origen', $origen['categoria_origen']);
		$this->db->where('categoria_destino', $origen['categoria_destino']);
		$this->db->where('precio_base', $base);
		$this->db->or_where('categoria_destino', $origen['categoria_origen']);
		$this->db->where('categoria_origen', $origen['categoria_destino']);
		$this->db->where('precio_base', $base);
		$this->db->update('precios');
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
	}
	//
	public function nuevo_objeto($elements,$categoria){
		//print_r($elements);
		//print_r($categoria);
		for ($i=0; $i <count($elements) ; $i++) {
			$dato=array(
			 	'nombre_objeto' => $elements[$i]['name'],
				'cantidad' => $elements[$i]['cant'],
				'categoria' => $categoria
			);
			$this->db->INSERT('objetos',$dato);
			if($this->db->affected_rows()==0){
				return false;
			}
			else{
				//return $resultado->result_array();
			}
			// code...
		}

	}
	public function actualizar_objetos_divisas($categoria,$elements,$total,$cajero){
		for ($i=0; $i <count($elements) ; $i++) {
			// code...
			//print_r($elements[$i]);
			$this->db->set('cantidad', $elements[$i]['nueva_cant']);
	  	$this->db->where('categoria', $categoria);
	  	$this->db->where('nombre_objeto', $elements[$i]['nombre']);
	  	$this->db->update('objetos');
		}
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			$hoy = getdate();
			//print_r($hoy);
			$dato=array(
			 	'fecha_base' => $hoy['year'].'-'.$hoy['mon'].'-'.$hoy['mday'],
				'hora_transaccion' => $hoy['hours'].':'.$hoy['minutes'].':'.$hoy['seconds'],
				'divisa' => $categoria,
				'cantidad' => $total,
				'cajero' => $cajero
			);
			$this->db->INSERT('base_diaria',$dato);
			if($this->db->affected_rows()==0){
				return false;
			}
			else{
				//return $resultado->result_array();
				return true;
			}

		}
	}
	public function actualizar_objetos_divisas2($elements){
			for ($i=0; $i <count($elements) ; $i++) {
				// code...
				//print_r($elements[$i]);
				$this->db->set('cantidad', $elements[$i]['cantidad']);
		  	$this->db->where('categoria', $elements[$i]['categoria']);
		  	$this->db->where('nombre_objeto', $elements[$i]['nombre']);
		  	$this->db->update('objetos');
			}
			if($this->db->affected_rows()==0){
				return false;
			}
			else{
				return true;
			}
	}
	//funciones categoria general
	public function agregar_categoria_general($cat){
		$dato=array(
		 	'nombre_general' => $cat
		);
		$this->db->INSERT('general',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
	}
	//
	public function agregar_categoria_seguros($cat){
		$dato=array(
		 	'nombre_seguros' => $cat
		);
		$this->db->INSERT('seguros',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
	}
	//
	public function nuevo_objeto_general($valores,$categoria){
		//print_r($categoria);
		$dato=array(
		 	'nombre_objeto_gen' => $valores[0],
			'cantidad_gen' => $valores[1],
			'precio_obj_gen_venta' => $valores[2],
			'precio_obj_gen_compra' => $valores[3],
			'categoria_gen' => $categoria
		);
		$this->db->INSERT('objeto_general',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
	}
	//
	public function nuevo_objeto_seguros($valores,$categoria){
		//print_r($categoria);
		$dato=array(
		 	'nombre_objeto_seg' => $valores[0],
			'cantidad_seg' => $valores[1],
			'precio_obj_venta' => $valores[2],
			'precio_obj_compra' => $valores[3],
			'categoria_seg' => $categoria
		);
		$this->db->INSERT('objeto_seguro',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
	}
	//
	public function obj_general($categoria){
		$this->db->SELECT('*');
		$this->db->from('objeto_general');
		$this->db->where('categoria_gen',$categoria);
		$this->db->order_by('nombre_objeto_gen','asc');
		$resultado = $this->db->get();
    return $resultado->result_array();
	}
 //
 public function modificar_obj_general($original,$valores){
	 $this->db->set('cantidad_gen', $valores[0]);
	 $this->db->set('precio_obj_gen_compra', $valores[1]);
	 $this->db->set('precio_obj_gen_venta', $valores[2]);
	 $this->db->where('id_objeto_gen', $original['id_objeto_gen']);
	 $this->db->where('nombre_objeto_gen', $original['nombre_objeto_gen']);
	 $this->db->update('objeto_general');
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 //
 public function modificar_obj_seguros($original,$valores){
	 $this->db->set('cantidad_seg', $valores[0]);
	 $this->db->set('precio_obj_compra', $valores[1]);
	 $this->db->set('precio_obj_venta', $valores[2]);
	 $this->db->where('id_objeto_seg', $original['id_objeto_seg']);
	 $this->db->where('nombre_objeto_seg', $original['nombre_objeto_seg']);
	 $this->db->update('objeto_seguro');
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 public function consultar_objetos_divisas($categoria){
	 $this->db->SELECT('*');
	 $this->db->from('precios');
	 $this->db->where('categoria_origen',$categoria);
	 $this->db->where('categoria_destino',1);
	 $this->db->where('cantidad_base >',0);
	 $this->db->order_by('precio_base','desc');
	 $resultado = $this->db->get();
	 return $resultado->result_array();
 }
 //cajeros
 public function cajeros(){
	 $this->db->SELECT('*');
 	$this->db->from('usuarios');
	//$this->db->where('tipo','c');
 	$resultado = $this->db->get();
 	return $resultado->result_array();
 }
 public function insertar_cajero($cajero){
	 $dato=array(
		 'nombre' => $cajero['nombre'],
		 'apellido' => $cajero['apellido'],
		 'cedula' => $cajero['cedula'],
		 'clave' => $cajero['clave'],
		 'tipo' => 'c'
	 );
	 $this->db->INSERT('usuarios',$dato);
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 //eliminar objetos
 public function eliminar_objeto_divisas($objeto,$categoria){
	 $this->db->where('nombre_objeto', $objeto);
	 $this->db->where('categoria', $categoria);
	 $this->db->delete('objetos');
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 public function eliminar_cajero($orden){

	$msj = "123456789";
	$this->db->set('cajero', $msj);
	$this->db->where('cajero', $orden);
	$this->db->update('base_diaria');

	$this->db->set('ced_cajero_div', $msj);
	$this->db->where('ced_cajero_div', $orden);
	$this->db->update('transacciones_divisas');


	$this->db->where('cedula', $orden);
	$this->db->delete('usuarios');
	if($this->db->affected_rows()==0){
		return false;
	}
	else{
		
		return true;
		
	}
}
 public function eliminar_objeto_seguros($objeto,$categoria){
	 $this->db->where('nombre_objeto_seg', $objeto);
	 $this->db->where('categoria_seg', $categoria);
	 $this->db->delete('objeto_seguro');
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 public function eliminar_objeto_general($objeto,$categoria){
	 $this->db->where('nombre_objeto_gen', $objeto);
	 $this->db->where('categoria_gen', $categoria);
	 $this->db->delete('objeto_general');
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 public function eliminar_categoria_seguros($categoria){
	 $this->db->where('categoria_seg', $categoria);
	 $this->db->delete('objeto_seguro');
	 $this->db->where('categoria_seg', $categoria);
	 $this->db->delete('transacciones_seguros');
	 $this->db->where('id_seguros', $categoria);
	 $this->db->delete('seguros');
	 return true;
 }
 public function eliminar_categoria_general($categoria){
	 $this->db->where('categoria_gen', $categoria);
	 $this->db->delete('objeto_general');
	 $this->db->where('categoria_gen', $categoria);
	 $this->db->delete('transacciones_general');
	 $this->db->where('id_general', $categoria);
	 $this->db->delete('general');
	 return true;
 }
 public function ver_bases_diarias($fecha){
	$this->db->SELECT('*');
  	$this->db->from('base_diaria');
	$this->db->where('fecha_base',$fecha);
	$this->db->join('categoria','categoria.id_categoria=base_diaria.divisa');
	$this->db->join('usuarios','usuarios.cedula=base_diaria.cajero');
	//$this->db->order_by('fecha_base','desc');
	$this->db->order_by('cajero','asc');
	$this->db->order_by('divisa','asc');	
  $resultado = $this->db->get();
  return $resultado->result_array();
 }
 public function notificaciones(){
	 $this->db->SELECT('*');
   $this->db->from('precios');
	 $this->db->join('categoria','categoria.id_categoria=precios.categoria_origen');
	 $this->db->where('cantidad_base <=',300);
   $resultado = $this->db->get();
   return $resultado->result_array();
 }
 public function divisa_unica($categoria){
	 $this->db->SELECT('nombre_categoria');
   $this->db->from('categoria');
	 //$this->db->join('categoria','categoria.id_categoria=precios.categoria_origen');
	 $this->db->where('id_categoria',$categoria);
   $resultado = $this->db->get();
   return $resultado->result_array();
 }
 // public function informe_cajero($cajero,$fecha){
	//  //$hoy = getdate();
	//  $this->db->SELECT('*');
 //   $this->db->from('transacciones_divisas');
	//  $this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
	//  $this->db->where('ced_cajero_div',$cajero);
	//  //$this->db->where('fecha_div',$hoy['year'].'-'.$hoy['mon'].'-'.'20');
	//  $this->db->where('fecha_div',$fecha);
	//  $this->db->order_by('div1','asc');
 //   $resultado = $this->db->get();
 //   return $resultado->result_array();
 // }
 public function informe_cajero($cajero,$fecha){
	 $this->db->SELECT('*');
	 $this->db->from('base_diaria');
	 //$this->db->where('divisa',$div_1);
	 $this->db->where('cajero',$cajero);
	 $this->db->order_by('fecha_base','asc');
	 $this->db->order_by('hora_transaccion','asc');
	 $fecha_temp1 = $this->db->get();
	 $fecha_temp=$fecha_temp1->result_array();
	 //print_r($fecha_temp);
	 $todo=array();
	 $this->db->SELECT('*');
	 $this->db->from('transacciones_divisas');
	 $this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
	 $this->db->where('ced_cajero_div',$cajero);
	 //$this->db->where('fecha_div',$hoy['year'].'-'.$hoy['mon'].'-'.'20');
	 $this->db->where('fecha_div >=',$fecha_temp[0]['fecha_base']);
	 $this->db->where('hora_div >=',$fecha_temp[0]['hora_transaccion']);
	 $this->db->order_by('div1','asc');
	 $resultado = $this->db->get();
	 $todo[0]=$resultado->result_array();
	 $this->db->SELECT('*');
	 $this->db->from('base_diaria');
	 $this->db->join('categoria','categoria.id_categoria=base_diaria.divisa');
	 $this->db->where('cajero',$cajero);
	 //$this->db->where('fecha_div',$hoy['year'].'-'.$hoy['mon'].'-'.'20');
	 //$this->db->where('fecha_base',$fecha);
	 $this->db->order_by('divisa','asc');
	 $this->db->order_by('precio_base','asc');
	 $resultado2 = $this->db->get();
	 $todo[1]=$resultado2->result_array();
   return $todo;
 }
 public function consultar_transaciones_divisas($inicio,$fin){
	 $this->db->SELECT('*');
	 $this->db->from('transacciones_divisas');
	 $this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
	 if ($inicio!='' && $fin=='') {
		 //echo "sisis".$inicio;
		 $this->db->where('fecha_div >=',$inicio);

	 } else {
		 if ($inicio!='' && $fin!='') {
			 //echo "2".$fin;
			 $this->db->where('fecha_div >=',$inicio);
			 $this->db->where('fecha_div <=',$fin);
		 } else {
			 if ($inicio=='' && $fin!='') {
				 // code...
				 //echo "3".$fin;
				 $this->db->where('fecha_div <=',$fin);
			 } else {
				 // code...
			 }

		 }

	 }
	 $this->db->order_by('fecha_div','desc');
	 $this->db->order_by('hora_div','desc');
	 $resultado = $this->db->get();
	 if ($resultado->num_rows()==0) {
		 return false;
	 }else{
		 return $resultado->result_array();
	 }

 }
 public function consultar_transaciones_divisas2($fecha,$cedula){
	 $this->db->SELECT('*');
	 $this->db->from('base_diaria');
	 //$this->db->where('divisa',$div_1);
	 $this->db->where('cajero',$cedula);
	 $this->db->order_by('fecha_base','asc');
	 $this->db->order_by('hora_transaccion','asc');
	 $fecha_temp1 = $this->db->get();
	 $fecha_temp=$fecha_temp1->result_array();
	 $this->db->SELECT('*');
	 $this->db->from('transacciones_divisas');
	 $this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
	 $this->db->where('fecha_div >=',$fecha_temp[0]['fecha_base']);
	 $this->db->where('hora_div >=',$fecha_temp[0]['hora_transaccion']);
	 $this->db->where('ced_cajero_div',$cedula);
	 $this->db->order_by('fecha_div','desc');
	 $this->db->order_by('hora_div','desc');
	 $resultado = $this->db->get();
	 if ($resultado->num_rows()==0) {
		 return false;
	 }else{
		 return $resultado->result_array();
	 }

 }
 function eliminar_transaccion_divisa($orden,$objeto){
	 $this->db->where('fecha', $objeto['fecha_div']);
	 $this->db->where('hora_conta', $objeto['hora_div']);
	 $this->db->where('cajero_conta', $objeto['ced_cajero_div']);
	 $this->db->where('tran_tipo', 'DIVISAS');
	 $res=$this->db->delete('caja_general');
	 if ($res) {
		 // code...
		 if ($objeto['tipo']=='compra') {
			 // code...
			 $this->actuaizar_canitdad_divisa_venta($objeto['div1'],$objeto['div2'],$objeto['precio_base_div'],$objeto['cantidad_div']);
		 } else {
			 // code...
			 $this->actuaizar_canitdad_divisa_compra($objeto['div1'],$objeto['div2'],$objeto['precio_base_div'],$objeto['cantidad_div']);
		 }

		 $this->db->where('fecha_div', $objeto['fecha_div']);
		 $this->db->where('hora_div', $objeto['hora_div']);
		 $this->db->where('ced_cajero_div', $objeto['ced_cajero_div']);
		 $res2=$this->db->delete('transacciones_divisas');
		 if ($res2) {
			 // code...
			 return true;
		 }
	 }
	 return false;
 }
 function actuaizar_canitdad_divisa_compra($div1,$div2,$precio_base,$cantidad){
	 //echo $precio_base;
	 $this->db->SELECT('cantidad_base');
	 $this->db->from('precios');
	 $this->db->where('categoria_origen', $div1);
	 $this->db->where('categoria_destino', $div2);
	 $this->db->where('precio_base', $precio_base);
	 $this->db->or_where('categoria_destino', $div1);
	 $this->db->where('categoria_origen', $div2);
	 $this->db->where('precio_base', $precio_base);
	 $resultado = $this->db->get();
	 $resultado1=$resultado->result_array();
	 //print_r($resultado1[0]['cantidad_base']);
	 //print_r($resultado1[1]['cantidad_base']);
	 if ($resultado->num_rows()!=0) {
		 $this->db->set('cantidad_base', $cantidad+$resultado1[0]['cantidad_base']);
		 $this->db->where('categoria_origen', $div1);
		 $this->db->where('categoria_destino', $div2);
		 $this->db->where('precio_base', $precio_base);
		 $this->db->or_where('categoria_destino', $div1);
		 $this->db->where('categoria_origen', $div2);
		 $this->db->where('precio_base', $precio_base);
		 $this->db->update('precios');
		 if($this->db->affected_rows()==0){
			 return false;
		 }
		 else{
			 //print_r('isisisisisisi');
			 return true;
		 }
	 }else{
		 //return $resultado->result_array();
	 }
	 //$this->db->set('cantidad_base', $cant_base);
 }
 //
 function actuaizar_canitdad_divisa_venta($div1,$div2,$precio_base,$cantidad){
	 //echo $precio_base;
	 $this->db->SELECT('cantidad_base');
	 $this->db->from('precios');
	 $this->db->where('categoria_origen', $div1);
	 $this->db->where('categoria_destino', $div2);
	 $this->db->where('precio_base', $precio_base);
	 $this->db->or_where('categoria_destino', $div1);
	 $this->db->where('categoria_origen', $div2);
	 $this->db->where('precio_base', $precio_base);
	 $resultado = $this->db->get();
	 $resultado1=$resultado->result_array();
	 //print_r($resultado1[0]['cantidad_base']);
	 //print_r($resultado1[1]['cantidad_base']);
	 if ($resultado->num_rows()!=0) {
		 $this->db->set('cantidad_base', $resultado1[0]['cantidad_base']-$cantidad);
		 $this->db->where('categoria_origen', $div1);
		 $this->db->where('categoria_destino', $div2);
		 $this->db->where('precio_base', $precio_base);
		 $this->db->or_where('categoria_destino', $div1);
		 $this->db->where('categoria_origen', $div2);
		 $this->db->where('precio_base', $precio_base);
		 $this->db->update('precios');
		 if($this->db->affected_rows()==0){
			 return false;
		 }
		 else{
			 //print_r('isisisisisisi');
			 return true;
		 }
	 }else{
		 //return $resultado->result_array();
	 }
	 //$this->db->set('cantidad_base', $cant_base);
 }
 public function insertar_ingresos_egresos($obj){
	 //$hoy = getdate();
	 //$fecha=$hoy['year'].'-'.$hoy['mon'].'-'.$hoy['mday'];
	 //echo $fecha;
	 $resultado=$this->consultar_base_especifica($obj['divisa'],$obj['cajero'],$obj['fecha'],$obj['precio']);
	 $this->db->SELECT('cantidad_base');
	 $this->db->from('precios');
	 $this->db->where('categoria_origen', $obj['divisa']);
	 $this->db->where('categoria_destino', 1);
	 $this->db->where('precio_base', $obj['precio']);
	 $global = $this->db->get();
	 $global1=$global->result_array();
	 $mayor='El valor solicitado exede la cantidad de este precio de divisa en el inventario';
	 if ($obj['tipo']=='ingreso'&&$global1[0]['cantidad_base']<$obj['valor']) {
	 	// code...
		return $mayor;
	 }
	 if ($obj['tipo']=='egreso'&&count($global1)==0) {
	 	// code...
		//echo "si";
		 //print_r($global1);
		 //return;
			 $dato=array(
 				'cantidad_base' => 0,
 				'precio_base' => $obj['precio'],
 			  'precio_venta1' => $obj['precio']+1,
 			 	'precio_venta2' => $obj['precio']+2,
 				'precio_venta3' => $obj['precio']+3,
 			 	'categoria_origen' => $obj['divisa'],
 	      'categoria_destino' => 1
 				);
 			$this->db->INSERT('precios',$dato);
	 }
	 //print_r($resultado);
	 //return;
	 if ($resultado[0][$obj['tipo']]=='') {
	 	// code...
		$this->db->set($obj['tipo'], $obj['valor']);
	 } else {
	 	// code...
		$this->db->set($obj['tipo'], $obj['valor']+$resultado[0][$obj['tipo']]);
	 }
	 if ($obj['tipo']=='ingreso') {
		$this->db->set('cantidad', $obj['valor']+$resultado[0]['cantidad']);
	 } else {
	 	$this->db->set('cantidad', $resultado[0]['cantidad']-$obj['valor']);
	 }

	 $this->db->where('precio_base', $obj['precio']);
	 $this->db->where('fecha_base', $obj['fecha']);
	 $this->db->where('divisa', $obj['divisa']);
	 $this->db->where('cajero', $obj['cajero']);
	 $this->db->update('base_diaria');
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 //$this->actualizar_inventario_por_ing_egre($obj['divisa'],$obj['precio'],$obj['tipo'],$obj['valor']);
		 if ($obj['tipo']=='egreso') {
			 if (count($global1)==0) {
			 	$this->db->set('cantidad_base', $obj['valor']);

			 } else {
			 	// code...
				$this->db->set('cantidad_base', $obj['valor']+$global1[0]['cantidad_base']);
			 }
		 } else {
		 	$this->db->set('cantidad_base', $global1[0]['cantidad_base']-$obj['valor']);
		 }

		 $this->db->where('categoria_origen', $obj['divisa']);
		 $this->db->where('categoria_destino', 1);
		 $this->db->where('precio_base', $obj['precio']);
		 $this->db->update('precios');
		 return true;
	 }
 }
 public function actualizar_inventario_por_ing_egre($divisa,$precio,$tipo,$valor){
	 $this->db->SELECT('cantidad_base');
	 $this->db->from('precios');
	 $this->db->where('categoria_origen', $divisa);
	 $this->db->where('categoria_destino', 1);
	 $this->db->where('precio_base', $precio);
	 $resultado = $this->db->get();
	 $resultado1 =$resultado->result_array();
	 if ($resultado->num_rows()!=0) {
		 if ($tipo=='ingreso') {
		 	$this->db->set('cantidad_base',$resultado1[0]['cantidad_base']-$valor);
		 } else {
		 	$this->db->set('cantidad_base',$resultado1[0]['cantidad_base']+$valor);
		 }
		 $this->db->where('categoria_origen', $divisa);
		 $this->db->where('categoria_destino', 1);
		 $this->db->where('precio_base', $precio);
		 $this->db->update('precios');
		 if($this->db->affected_rows()==0){
			 return false;
		 }
		 else{
			 //print_r('isisisisisisi');
			 return true;
		 }
	 }else{
		 //return $resultado->result_array();
	 }
 }
 public function consultar_base_especifica($divisa,$cajero,$fecha,$precio){
	 $this->db->SELECT('*');
	 $this->db->from('base_diaria');
	 $this->db->where('fecha_base', $fecha);
	 $this->db->where('divisa', $divisa);
	 $this->db->where('cajero', $cajero);
	 $this->db->where('precio_base', $precio);
	 $resultado = $this->db->get();
	 if ($resultado->num_rows()==0) {
		 return false;
	 }else{
		 return $resultado->result_array();
	 }
 }
 public function insertar_solicitud_cajero($obj){
	 //$hoy = getdate();
	 //$fecha=$hoy['year'].'-'.$hoy['mon'].'-'.$hoy['mday'];
	 $dato=array(
		 'cajero_sol' => $obj['cajero'],
		 'divisa_sol' => $obj['divisa'],
		 'fecha_sol' => $obj['fecha'],
		 'precio' => $obj['precio'],
		 'valor_sol' => $obj['valor'],
		 'tipo_sol' => $obj['tipo'],
		 'estado_sol' => 'P'
	 );
	 $this->db->INSERT('solicitud',$dato);
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 function solicitud_pendiente(){
	 $this->db->SELECT('*');
   $this->db->from('solicitud');
	 $this->db->join('categoria','categoria.id_categoria=solicitud.divisa_sol');
	 $this->db->where('estado_sol','P');
   $resultado = $this->db->get();
   return $resultado->result_array();
 }
 function cambiar_estado_solicitud($obj){
	 $this->db->set('estado_sol', $obj['estado_sol']);
	 $this->db->where('id_sol', $obj['id_sol']);
	 //$this->db->where('nombre_objeto', $elements[$i]['nombre']);
	 $this->db->update('solicitud');
	 if($this->db->affected_rows()==0){
		 return false;
	 }
	 else{
		 return true;
	 }
 }
 function base_diaria_entrega($objetos){
	 //print_r($objetos);
	 for ($i=0; $i <count($objetos) ; $i++) {
		 $dato=array(
 		 	'precio_base' => $objetos[$i]['precio_base'],
 			'cantidad' => $objetos[$i]['cantidad'],
			'cantidad_inicial' => $objetos[$i]['cantidad'],
 			'fecha_base' => $objetos[$i]['fecha'],
 			'hora_transaccion' => $objetos[$i]['time'],
			'divisa' => $objetos[$i]['divisa'],
			'cajero' => $objetos[$i]['cajero'],
			'precio_venta1' => $objetos[$i]['precio_venta1'],
			'precio_venta2' => $objetos[$i]['precio_venta2'],
			'precio_venta3' => $objetos[$i]['precio_venta3']
	 		);
	 		$this->db->INSERT('base_diaria',$dato);
			if ($this->db->affected_rows()==0) {
			 	return false;
			}else{
				$this->db->set('cantidad_base',$objetos[$i]['stock']-$objetos[$i]['cantidad']);
				$this->db->where('categoria_origen',$objetos[$i]['divisa']);
				$this->db->where('categoria_destino',1);
				$this->db->where('precio_base',$objetos[$i]['precio_base']);
			 	$this->db->update('precios');
			}
	 	}
		return true;
		//print_r($this->db->affected_rows());

 	}
	public function pesos_maximos($div_1,$div_2,$cedula,$fecha){
		$this->db->SELECT('cantidad');
		$this->db->from('base_diaria');
		$this->db->join('categoria','categoria.id_categoria=base_diaria.divisa');
    $this->db->where('divisa',$div_2);
		$this->db->where('cajero',$cedula);
		$this->db->where('fecha_base',$fecha);
		$resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
			return $resultado->result_array();
    }
	}
	public function divisa_maximos($div_1,$div_2,$cedula,$fecha){
		$this->db->SELECT('cantidad');
		$this->db->from('base_diaria');
		//$this->db->join('categoria','categoria.id_categoria=base_diaria.divisa');
    $this->db->where('divisa',$div_1);
		$this->db->where('cajero',$cedula);
		$this->db->where('fecha_base',$fecha);
		$resultado = $this->db->get();
		$cantidades=$resultado->result_array();
		$total=0;
		for ($i=0; $i <count($cantidades) ; $i++) {
			$total=$total+$cantidades[$i]['cantidad'];
		}
		return $total;
	}
	public function cajeros_general_transacciones($obj){
		$this->db->SELECT('fecha,hora');
 	 	$this->db->from('cajeros_estado');
 	 	$this->db->where('cedula_cajero', $obj['cajero']);
	 	$resultado2 = $this->db->get();
   	$resultado1=$resultado2->result_array();
		//echo count($resultado1);
		//print_r($obj);
		if ($resultado1!=null||count($resultado1)>0) {
			$this->db->SELECT('*');
	 	 	$this->db->from('transacciones_general');
		 	$this->db->join('general','general.id_general=transacciones_general.categoria_gen');
	 	 	$this->db->where('fecha_gen >=', $resultado1[0]['fecha']);
			$this->db->where('hora_gen >=', $resultado1[0]['hora']);
	 	 	$this->db->where('ced_cajero_gen', $obj['cajero']);
		 	$this->db->order_by('categoria_gen', 'asc');
		 	$this->db->order_by('fecha_gen', 'asc');
		 	$resultado = $this->db->get();
	   	return $resultado->result_array();
		}

	}
	public function cajeros_seguros_transacciones($obj){
		$this->db->SELECT('fecha,hora');
 	 	$this->db->from('cajeros_estado');
 	 	$this->db->where('cedula_cajero', $obj['cajero']);
	 	$resultado = $this->db->get();
   	$resultado1=$resultado->result_array();
		//echo count($resultado1);
		//print_r($obj);
		if ($resultado1!=null||count($resultado1)>0) {
			//print_r($resultado1);
			//print_r($obj);
			$this->db->SELECT('*');
	 	 	$this->db->from('transacciones_seguros');
		 	$this->db->join('seguros','seguros.id_seguros=transacciones_seguros.categoria_seg');
	 	 	$this->db->where('fecha_seg >=', $resultado1[0]['fecha']);
			$this->db->where('hora_seg >=', $resultado1[0]['hora']);
	 	 	$this->db->where('ced_cajero_seg', $obj['cajero']);
		 	$this->db->order_by('categoria_seg', 'asc');
		 	$this->db->order_by('fecha_seg', 'asc');
			$this->db->order_by('hora_seg', 'asc');
		 	$resultado2 = $this->db->get();
	   	return $resultado2->result_array();
		}
		$str='no existe reporte de ingreso';
		return $str;
	}
	public function transacion_inventario_global($objs){
		for ($i=0; $i < count($objs) ; $i++) {
			//print_r($objs[$i]);
			$this->db->SELECT('cantidad_base');
	 	 	$this->db->from('precios');
		 	$this->db->where('categoria_origen',$objs[$i]['divisa']);
	 	 	$this->db->where('categoria_destino',1);
	 	 	$this->db->where('precio_base',$objs[$i]['precio']);
			$resultado = $this->db->get();
			$resultado1=$resultado->result_array();
			if ($resultado1==null||count($resultado1)==0) {
				// code...
				$dato=array(
					'cantidad_base' => 0,
					'precio_base' => $objs[$i]['precio'],
				  'precio_venta1' => $objs[$i]['precio']+10,
				 	'precio_venta2' => $objs[$i]['precio']+11,
					'precio_venta3' => $objs[$i]['precio']+12,
				 	'categoria_origen' => $objs[$i]['divisa'],
		      'categoria_destino' => 1
				);
	 	 		$this->db->INSERT('precios',$dato);
				//return $objs[$i];

			} else {
				// code...
			}

		}
		for ($i=0; $i < count($objs) ; $i++) {
			//print_r($objs[$i]);
			$this->db->SELECT('cantidad_base');
	 	 	$this->db->from('precios');
		 	$this->db->where('categoria_origen',$objs[$i]['divisa']);
	 	 	$this->db->where('categoria_destino',1);
	 	 	$this->db->where('precio_base',$objs[$i]['precio']);
			$resultado = $this->db->get();
			$resultado1=$resultado->result_array();
			//echo$resultado1[0]['cantidad_base'].'<br>';
			  $this->db->set('cantidad_base',$resultado1[0]['cantidad_base']+$objs[$i]['cantidad']);
			  $this->db->where('categoria_origen',$objs[$i]['divisa']);
			  $this->db->where('categoria_destino',1);
			  $this->db->where('precio_base',$objs[$i]['precio']);
			  $this->db->update('precios');
			  if ($this->db->affected_rows()==0) {
			   	return false;
			  }
		}
   	return true;
	}
	function reiniciar_base_cajero($cajero){
		$this->db->where('cajero', $cajero);
		//$this->db->where('fecha_base', $objetos[0]['cajero']);
		//$this->db->where('categoria', $categoria);
		$this->db->delete('base_diaria');
		if ($this->db->affected_rows()>0) {
			return true;
	   }else{
		   return false;
	   }
	}
}
