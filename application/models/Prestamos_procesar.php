<?php
defined('BASEPATH') or exit('No direct script access allowed');
//echo "entro";

class Prestamos_procesar extends CI_Model
{
	function __construct()
	{

		parent::__construct();

	}
  public function insertar_prestamo($objeto){
		//print_r($objeto);
    $dato=array(
      'fecha' => $objeto['fecha'],
      'nombre_prestamo' => $objeto['nombre'],
      'cedula_prestamo' => $objeto['cedula'],
      'cantidad_prestamo' => $objeto['cantidad'],
      'estado' => 'A',
      'divisa' => $objeto['divisa']
    );
    $this->db->INSERT('prestamos',$dato);
    if($this->db->affected_rows()>0){
      return true;
    }else{
      return  false;
    }
  }
  public function cargar_prestamos(){
    $this->db->SELECT('*');
    $this->db->from('prestamos');
		$this->db->join('categoria','categoria.id_categoria=prestamos.divisa');
		$this->db->where('estado','A');
    $resultado = $this->db->get();
    return $resultado->result_array();
  }
  public function categorias_divisas(){
    $this->db->SELECT('*');
		$this->db->from('categoria');
		$resultado = $this->db->get();
    return $resultado->result_array();
  }
  public function actualizar_prestamo($objeto){
    $this->db->set('estado', $objeto['estado']);
		$this->db->where('id_prestamo', $objeto['id_prestamo']);
		$this->db->update('prestamos');
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
  }
	public function actualizar_abono($objeto){
    $this->db->set('abono', $objeto['abono']);
		$this->db->where('id_prestamo', $objeto['id_prestamo']);
		$this->db->update('prestamos');
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			return true;
		}
  }
}
