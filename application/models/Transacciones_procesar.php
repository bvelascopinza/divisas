<?php
defined('BASEPATH') or exit('No direct script access allowed');
//echo "entro";

class Transacciones_procesar extends CI_Model
{
	function __construct()
	{

		parent::__construct();

	}
	public function insertar_transaccion_divisas($obj,$opcion){
		//print_r($obj);
		$total=0;
   		$resultado=$this->consultar_relacion($obj['div1'],$obj['div2'],$obj['precio_base'],$obj['cedula_cajero'],$obj['fecha']);
		if ($opcion=='compra') {
			// code...
			$total = $obj['cantidad']*$obj['precio_uni'];
		} else {
			// code...
			$total = $obj['cantidad']/$obj['precio_uni'];
		}
    	$dato=array(
		  	'fecha_div' => $obj['fecha'],
		 	'hora_div' => $obj['time'],
			'div1' => $obj['div1'],
			'div2' => $obj['div2'],
			'cantidad_div' => $obj['cantidad'],
      		'precio_uni_div' => $obj['precio_uni'],
		 	'precio_base_div' =>$obj['precio_base'],
			//'total_tra_div' => str_replace(".","",$obj['total']),
			'total_tra_div' => $this->redondear_dos_decimal($total),
			'ced_cajero_div' => $obj['cedula_cajero'],
			'nombre_cajero_div' => $obj['nombre_cajero'],
      		'ced_cliente_div' => $obj['cedula_cliente'],
			'nombre_cliente_div' => $obj['nombre_cliente'],
			'tipo' => $opcion
		);
		$this->db->INSERT('transacciones_divisas',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			if ($opcion=='compra') {
				//viejo
				//$this->insertar_debitos($obj['fecha'],'DIVISAS',str_replace(".","",$obj['total']),$resultado[0]['precio_base']*$obj['cantidad']);
				//$this->actuaizar_canitdad_divisa($obj['div1'],$obj['div2'],$resultado[0]['precio_base'],$obj['cantidad']);
				//$this->insertar_debitos($obj['fecha'],'DIVISAS',str_replace(".","",$obj['total']),$obj['precio_uni']*$obj['cantidad'],$obj['time'],$obj['cedula_cajero']);
				//nuevo error 2024
				//$valor_formateado=str_replace(".","",$obj['total']);
				//$valor_formateado=str_replace(",",".",$valor_formateado);
				$this->insertar_debitos($obj['fecha'],'DIVISAS',$obj['total'],$obj['precio_uni']*$obj['cantidad'],$obj['time'],$obj['cedula_cajero']);
				$this->actuaizar_canitdad_divisa_compra($obj['div1'],$obj['div2'],$obj['precio_uni'],$obj['cantidad'],$obj['cedula_cajero'],$obj['fecha']);

	      return true;
			} else {
				if ($opcion=='venta') {
					//echo $this->redondear_dos_decimal($total).'--';
					//echo $resultado[0]['precio_base'];
					$total=$this->redondear_dos_decimal($total);
					$this->insertar_debitos($obj['fecha'],'DIVISAS',$obj['cantidad'],$resultado[0]['precio_base']*$total,$obj['time'],$obj['cedula_cajero']);
					$this->actuaizar_canitdad_divisa_venta($obj['div1'],$obj['div2'],$obj['precio_base'],$total,$obj['cedula_cajero'],$obj['fecha']);
					//$this->actuaizar_canitdad_divisa_venta($obj['div1'],$obj['div2'],$resultado[0]['precio_base'],$obj['total']);
					//$this->insertar_debitos($obj['fecha'],'DIVISAS',$obj['cantidad'],$resultado[0]['precio_base']*str_replace(".","",$obj['total']));
		      return true;
				} else {
					// code...
				}

				// code...
			}


			//return $resultado->result_array();
		}
  }
  public function consultar_relacion($div_1,$div_2,$precio_base,$cedula,$fecha){
		$this->db->SELECT('*');
		$this->db->from('base_diaria');
    $this->db->where('divisa',$div_1);
		$this->db->where('precio_base',$precio_base);
		$this->db->where('cantidad >',0);
		$this->db->where('cajero',$cedula);
		$this->db->where('fecha_base',$fecha);
		$this->db->order_by('precio_base','desc');
		$resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
			return $resultado->result_array();
    }
	}
  public function insertar_transaccion_seguros($obj){
		//print_r($obj);
    $dato=array(
		  'fecha_seg' => $obj['fecha'],
		 	'hora_seg' => $obj['time'],
			'nombre_seg' => $obj['nombre'],
			'categoria_seg' => $obj['categoria'],
      'placa' => $obj['placa'],
			'cantidad_seg' => $obj['cantidad'],
      'precio_uni_seg' => $obj['precio_uni'],
		 	'precio_base_seg' =>$obj['precio_base'],
			'total_tra_seg' => $obj['total'],
			'ced_cajero_seg' => $obj['cedula_cajero'],
			'nombre_cajero_seg' => $obj['nombre_cajero'],
      'ced_cliente_seg' => $obj['cedula_cliente'],
			'nombre_cliente_seg' => $obj['nombre_cliente'],
			'estado_seguro' => 'APROBADO'
		);
		$this->db->INSERT('transacciones_seguros',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			$this->insertar_debitos($obj['fecha'],'SEGUROS',$obj['total'],$obj['precio_base'],$obj['time'],$obj['cedula_cajero']);
      return true;
			//return $resultado->result_array();
		}
  }
  public function insertar_transaccion_general($obj,$total){
    //print_r(gettype($obj));

		//$cantidad_inv[0]['cantidad_gen']
    for ($i=0; $i < count($obj) ; $i++) {
			$cantidad_inv=$this->consultar_obj_gen($obj[$i]['nombre'],$obj[$i]['categoria']);
      $dato=array(
  		  'fecha_gen' => $obj[$i]['fecha'],
  		 	'hora_gen' => $obj[$i]['time'],
  			'nombre_gen' => $obj[$i]['nombre'],
  			'categoria_gen' => $obj[$i]['categoria'],
  			'cantidad_gen' => $obj[$i]['cantidad'],
        'precio_uni_gen' => $obj[$i]['precio_uni'],
  		 	'precio_base_gen' =>$obj[$i]['precio_base'],
  			'total_tra_gen' => $obj[$i]['total'],
  			'ced_cajero_gen' => $obj[$i]['cedula_cajero'],
  			'nombre_cajero_gen' => $obj[$i]['nombre_cajero'],
        'ced_cliente_gen' => $obj[$i]['cedula_cliente'],
  			'nombre_cliente_gen' => $obj[$i]['nombre_cliente']
  		);
  		$this->db->INSERT('transacciones_general',$dato);
  		if($this->db->affected_rows()==0){
  			return false;
  		}
  		else{
				$this->db->set('cantidad_gen', $cantidad_inv[0]['cantidad_gen']-$obj[$i]['cantidad']);
				$this->db->where('categoria_gen', $obj[$i]['categoria']);
				$this->db->where('nombre_objeto_gen', $obj[$i]['nombre']);
				$this->db->update('objeto_general');
				if($this->db->affected_rows()==0){
					return false;
				}
				else{
					$this->insertar_debitos($obj[0]['fecha'],'GENERAL',$obj[$i]['total'],$obj[$i]['precio_base']*$obj[$i]['cantidad'],$obj[$i]['time'],$obj[$i]['cedula_cajero']);
				}
        //return true;
  			//return $resultado->result_array();
  		}
    }

		//$this->insertar_debitos($obj[0]['fecha'],'GENERAL',$total);
		return true;
  }
	public function consultar_obj_gen($nombre,$categoria){
		$this->db->SELECT('*');
		$this->db->from('objeto_general');
		$this->db->where('nombre_objeto_gen',$nombre);
		$this->db->where('categoria_gen',$categoria);
		$resultado = $this->db->get();
		if ($resultado->num_rows()==0) {
      return false;
    }else{
      return $resultado->result_array();
    }
	}
  public function consultar_transaciones_divisas($inicio,$fin){
    $this->db->SELECT('*');
		$this->db->from('transacciones_divisas');
		$this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
    if ($inicio!='' && $fin=='') {
      //echo "sisis".$inicio;
      $this->db->where('fecha_div >=',$inicio);

    } else {
      if ($inicio!='' && $fin!='') {
        //echo "2".$fin;
        $this->db->where('fecha_div >=',$inicio);
        $this->db->where('fecha_div <=',$fin);
      } else {
        if ($inicio=='' && $fin!='') {
          // code...
          //echo "3".$fin;
          $this->db->where('fecha_div <=',$fin);
        } else {
          // code...
        }

      }

    }
		$this->db->order_by('fecha_div','desc');
		$this->db->order_by('hora_div','desc');
    $resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
      return $resultado->result_array();
    }

  }
	public function consultar_transacciones_divisas_dia($hoy,$tipo){
		/*
		$resultado=array();
    	$this->db->SELECT('*');
		$this->db->from('transacciones_divisas');
		$this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
		$this->db->where('fecha_div =',$hoy);
		$this->db->where('tipo','compra');
		$this->db->order_by('transacciones_divisas.div1','asc');
    
    	$resultado_compra = $this->db->get();
    	if ($resultado_compra->num_rows()==0) {
			$resultado[0]='false';
      		
    	}else{
      	
	  	$resultado[0]=$resultado_compra->result_array();
    	}
		$this->db->SELECT('*');
		$this->db->from('transacciones_divisas');
		$this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
		$this->db->where('fecha_div =',$hoy);
		$this->db->where('tipo','venta');
		$this->db->order_by('transacciones_divisas.div1','asc');
    
    	$resultado_venta = $this->db->get();
    	if ($resultado_venta->num_rows()==0) {
			$resultado[1]='false';
      		
    	}else{
      	
	  	$resultado[1]=$resultado_venta->result_array();
    	}
		return $resultado;
		*/
		$this->db->SELECT('*');
		$this->db->from('categoria');		
		$this->db->where('nombre_categoria !=','PESO');		
		$divisas = $this->db->get();
		if ($divisas->num_rows()==0) {
			return false;
		}else{
			$divisas = $divisas->result_array();
		}
		//print_r($divisas);
		$resultado=array();
		foreach ($divisas as $div_rep) {
			
			$this->db->SELECT('*');
			$this->db->from('transacciones_divisas');
			$this->db->join('categoria','categoria.id_categoria=transacciones_divisas.div1');
			$this->db->where('fecha_div =',$hoy);
			$this->db->where('tipo',$tipo);
			$this->db->where('id_categoria',$div_rep['id_categoria']);
			$this->db->order_by('transacciones_divisas.div1','asc');
			$resultado_tipo = $this->db->get();
			if ($resultado_tipo->num_rows()==0) {
				//$resultado[0]='false';
				array_push($resultado,'false');  
			}else{
				array_push($resultado, $resultado_tipo->result_array());
			  	//$resultado[0]=$resultado_tipo->result_array();
			}			
		}
		return $resultado;
		
  	}
	public function consultar_transaciones_seguros($inicio,$fin){
    $this->db->SELECT('*');
		$this->db->from('transacciones_seguros');
    if ($inicio!='' && $fin=='') {
      //echo "sisis".$inicio;
      $this->db->where('fecha_seg >=',$inicio);

    } else {
      if ($inicio!='' && $fin!='') {
        //echo "2".$fin;
        $this->db->where('fecha_seg >=',$inicio);
        $this->db->where('fecha_seg <=',$fin);
      } else {
        if ($inicio=='' && $fin!='') {
          // code...
          //echo "3".$fin;
          $this->db->where('fecha_seg <=',$fin);
        } else {
          // code...
        }

      }

    }
    $resultado = $this->db->get();
    if ($resultado->num_rows()==0) {
      return false;
    }else{
      return $resultado->result_array();
    }

	}


	public function consultar_seguros($cajero){
		$this->db->SELECT('fecha,hora');
		$this->db->from('cajeros_estado');
		$this->db->where('cedula_cajero',$cajero);
		$fecha1=$this->db->get();
		$fecha = $fecha1->result_array(); 


		$this->db->SELECT('fecha_seg, nombre_seguros, nombre_seg , precio_base_seg, precio_uni_seg, cantidad_seg, total_tra_seg');
		$this->db->from('transacciones_seguros');
		$this->db->join('seguros', 'seguros.id_seguros = transacciones_seguros.categoria_seg');
		$this->db->where('ced_cajero_seg', $cajero);
		$this->db->where('fecha_seg >=',$fecha[0]['fecha']);
		$this->db->where('hora_seg >=',$fecha[0]['hora']);
		$this->db->order_by('fecha_seg','desc');
		$resultado =  $this->db->get();
		if ($resultado->num_rows()==0) {
			return false;
		}else{
			return $resultado->result_array();
		}


	}
	public function verificar_estado($cajero){
		$this->db->SELECT('*');
		$this->db->from('cajeros_estado');
		$this->db->where('cedula_cajero',$cajero);
		$resultado =  $this->db->get();
		if ($resultado->num_rows()==0) {
			return false;
		}else{
			return true;
		}

	}

	public function terminar_turno($cajero){
		$this->db->where('cedula_cajero', $cajero);
		$res=$this->db->delete('cajeros_estado');
		if($res){
			return true;
		}
		else{
			return false;
		}
	}

	public function iniciar_turno($cedula, $fecha, $hora){
		$dato = array(
			'cedula_cajero' => $cedula,
			'fecha' => $fecha,
			'hora' => $hora
		);

		$this->db->INSERT('cajeros_estado',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
      return true;
			//return $resultado->result_array();
		}

	}
	public function consultar_transaciones_general($inicio,$fin){
		$this->db->SELECT('*');
		$this->db->from('transacciones_general');
		if ($inicio!='' && $fin=='') {
			//echo "sisis".$inicio;
			$this->db->where('fecha_gen >=',$inicio);

		} else {
			if ($inicio!='' && $fin!='') {
				//echo "2".$fin;
				$this->db->where('fecha_gen >=',$inicio);
				$this->db->where('fecha_gen <=',$fin);
			} else {
				if ($inicio=='' && $fin!='') {
					// code...
					//echo "3".$fin;
					$this->db->where('fecha_gen <=',$fin);
				} else {
					// code...
				}

			}

		}
		$this->db->order_by('fecha_gen','desc');
		$this->db->order_by('categoria_gen','desc');
		$resultado = $this->db->get();
		if ($resultado->num_rows()==0) {
			return false;
		}else{
			return $resultado->result_array();
		}

	}
	//
	public function insertar_debitos($fecha,$tipo,$valor_debito,$valor_credito,$hora,$cajero){

		$dato=array(
		  'fecha' => $fecha,
		 	'tran_tipo' => $tipo,
			'debito' => $valor_debito,
			'credito' => $valor_credito,
			'ganancia' => round(($valor_debito-$valor_credito),2),
			'cajero_conta' =>$cajero,
			'hora_conta'=> $hora
		);
		//print_r($dato);
		$this->db->INSERT('caja_general',$dato);
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
      return true;
			//return $resultado->result_array();
		}
	}
	public function caja_general(){
		$this->db->SELECT('*');
		$this->db->from('caja_general');
		$this->db->order_by('id_caja_general','desc');
		$resultado = $this->db->get();
		if ($resultado->num_rows()==0) {
      return false;
    }else{
      return $resultado->result_array();
    }
	}
	public function consultar_conta($inicio,$fin){
		$this->db->SELECT('*');
		$this->db->from('caja_general');
		if ($inicio!='' && $fin=='') {
			//echo "sisis".$inicio;
			$this->db->where('fecha >=',$inicio);

		} else {
			if ($inicio!='' && $fin!='') {
				//echo "2".$fin;
				$this->db->where('fecha >=',$inicio);
				$this->db->where('fecha <=',$fin);
			} else {
				if ($inicio=='' && $fin!='') {
					// code...
					//echo "3".$fin;
					$this->db->where('fecha <=',$fin);
				} else {
					// code...
				}

			}

		}
		$resultado = $this->db->get();
		if ($resultado->num_rows()==0) {
			return false;
		}else{
			return $resultado->result_array();
		}

	}
	function actuaizar_canitdad_divisa_compra($div1,$div2,$precio_base,$cantidad,$cedula,$fecha){
		//echo $precio_base;
		$this->db->SELECT('cantidad');
		$this->db->from('base_diaria');
		$this->db->where('divisa', $div1);
		$this->db->where('cajero', $cedula);
		$this->db->where('fecha_base', $fecha);
		$this->db->where('precio_base', $precio_base);
		$resultado = $this->db->get();
		$resultado1=$resultado->result_array();
		//print_r($resultado1[0]['cantidad_base']);
		//print_r($resultado1[1]['cantidad_base']);
		if ($resultado->num_rows()!=0) {
      $this->db->set('cantidad', $cantidad+$resultado1[0]['cantidad']);
			$this->db->where('divisa', $div1);
			$this->db->where('cajero', $cedula);
			$this->db->where('fecha_base', $fecha);
			$this->db->where('precio_base', $precio_base);
			$this->db->update('base_diaria');
			if($this->db->affected_rows()==0){
				return false;
			}
			else{
				//print_r('isisisisisisi');
				return true;
			}
    }else{
      //return $resultado->result_array();
    }
		/* viejo
		$this->db->SELECT('cantidad_base');
		$this->db->from('precios');
		$this->db->where('categoria_origen', $div1);
		$this->db->where('categoria_destino', $div2);
		$this->db->where('precio_base', $precio_base);
		$this->db->or_where('categoria_destino', $div1);
		$this->db->where('categoria_origen', $div2);
		$this->db->where('precio_base', $precio_base);
		$resultado = $this->db->get();
		$resultado1=$resultado->result_array();
		//print_r($resultado1[0]['cantidad_base']);
		//print_r($resultado1[1]['cantidad_base']);
		if ($resultado->num_rows()!=0) {
      $this->db->set('cantidad_base', $cantidad+$resultado1[0]['cantidad_base']);
			$this->db->where('categoria_origen', $div1);
			$this->db->where('categoria_destino', $div2);
			$this->db->where('precio_base', $precio_base);
			$this->db->or_where('categoria_destino', $div1);
			$this->db->where('categoria_origen', $div2);
			$this->db->where('precio_base', $precio_base);
			$this->db->update('precios');
			if($this->db->affected_rows()==0){
				return false;
			}
			else{
				//print_r('isisisisisisi');
				return true;
			}
    }else{
      //return $resultado->result_array();
    }
		*/
		//$this->db->set('cantidad_base', $cant_base);
	}
	//for
	function actuaizar_canitdad_divisa_venta($div1,$div2,$precio_base,$cantidad,$cajero,$fecha){
		$this->db->SELECT('cantidad');
		$this->db->from('base_diaria');
		$this->db->where('divisa', $div1);
		$this->db->where('cajero', $cajero);
		$this->db->where('precio_base', $precio_base);
		$this->db->where('fecha_base', $fecha);
		//$this->db->where('categoria_origen', $div2);
		//$this->db->where('precio_base', $precio_base);
		$resultado = $this->db->get();
		$resultado1=$resultado->result_array();
		if ($resultado->num_rows()!=0) {
			//echo $precio_base.'---'.$cantidad.' ttt '.$resultado1[0]['cantidad'].'kkk'.$cajero;
      $this->db->set('cantidad', $resultado1[0]['cantidad']-$cantidad);
			$this->db->where('divisa', $div1);
			$this->db->where('cajero', $cajero);
			$this->db->where('precio_base', $precio_base);
			$this->db->where('fecha_base', $fecha);
			$this->db->update('base_diaria');
			if($this->db->affected_rows()==0){
				return false;
			}
			else{
				return true;
			}
    }else{
    }
	}
	///informe global
	function divisas(){
		$this->db->SELECT('*');
		$this->db->where('nombre_categoria <>','PESO');
		$this->db->from('categoria');
		$resultado = $this->db->get();
    return $resultado->result_array();
	}

	// function divisas_informe_dividido($divisa,$fecha,$fecha2){
	// 	//echo($divisa['nombre_categoria']);
	// 	$this->db->SELECT('*');
	// 	$this->db->from('transacciones_divisas');
	// 	$this->db->where('fecha_div >=',$fecha);
	// 	$this->db->where('fecha_div <=',$fecha2);
	// 	$this->db->where('div1',$divisa['id_categoria']);
	// 	$this->db->order_by('precio_uni_div');
	// 	//$this->db->or_where('div2',$divisa);
	// 	$resultado = $this->db->get();
  //   return $resultado->result_array();
	// }
	//function informe_global_divisas($fecha,$fecha2){
	function informe_global_divisas(){
		$informe = array();
		//$divisas=$this->divisas();
		// for ($i=0; $i <count($divisas) ; $i++) {
		// 	//$consulta=$this->divisas_informe_dividido($divisas[$i],$fecha,$fecha2);
		// 	$consulta=$this->divisas_informe_dividido2($divisas[$i]);
		// 	array_push($informe,$consulta);
		// 	//print_r($consulta);
		// }
		$consulta=$this->divisas_informe_dividido2();
		//print_r($consulta);
		return $consulta;

	}
	function divisas_informe_dividido2(){
		//echo($divisa['nombre_categoria']);
		$this->db->SELECT('nombre_categoria,precio_base,cantidad_base');
		$this->db->from('precios');
		$this->db->join('categoria', 'categoria.id_categoria = precios.categoria_origen');
		//$this->db->where('nombre_categoria ','PESO');
		$this->db->order_by('nombre_categoria','asc');
		$this->db->order_by('precio_base','asc');
		//$this->db->or_where('div2',$divisa);
		$resultado = $this->db->get();
    return $resultado->result_array();
	}
	function informe_global_divisas_pesos(){
		//echo($divisa['nombre_categoria']);
		$this->db->SELECT('nombre_categoria,precio_base,cantidad_base');
		$this->db->from('precios');
		$this->db->join('categoria', 'categoria.id_categoria = precios.categoria_origen');
		$this->db->where('nombre_categoria','PESO');
		//$this->db->or_where('div2',$divisa);
		$resultado = $this->db->get();
    return $resultado->result_array();
	}
	function cargar_bancos(){
		$this->db->SELECT('*');
		$this->db->from('bancos');
		$resultado = $this->db->get();
    return $resultado->result_array();
	}
	function actualizar_bancos($valor){
		//print_r('isisisisisisi'.$valor);
		$this->db->set('valor_bancos',$valor);
		$this->db->update('bancos');
		if($this->db->affected_rows()==0){
			return false;
		}
		else{
			//print_r('isisisisisisi');
			return true;
		}
	}
	function redondear_dos_decimal($valor) {
  	$float_redondeado=round($valor * 100) / 100;
  	return $float_redondeado;
	}
	function eliminar_transaccion_divisa($orden,$objeto){
		$this->db->where('fecha', $objeto['fecha_div']);
		$this->db->where('hora_conta', $objeto['hora_div']);
		$this->db->where('cajero_conta', $objeto['ced_cajero_div']);
		$this->db->where('tran_tipo', 'DIVISAS');
		$res=$this->db->delete('caja_general');
		if ($res) {
			// code...
			if ($objeto['tipo']=='compra') {
				// code...
				$this->actuaizar_canitdad_divisa_venta($objeto['div1'],$objeto['div2'],$objeto['precio_base_div'],$objeto['cantidad_div'],$objeto['ced_cajero_div'],$objeto['fecha_div']);
			} else {
				// code...
				$this->actuaizar_canitdad_divisa_compra($objeto['div1'],$objeto['div2'],$objeto['precio_base_div'],$this->redondear_dos_decimal($objeto['total_tra_div']),$objeto['ced_cajero_div'],$objeto['fecha_div']);

			}

			$this->db->where('fecha_div', $objeto['fecha_div']);
			$this->db->where('hora_div', $objeto['hora_div']);
			$this->db->where('ced_cajero_div', $objeto['ced_cajero_div']);
			$res2=$this->db->delete('transacciones_divisas');
			if ($res2) {
				// code...
				return true;
			}
		}
		return false;
	}
	function seleccion_contabilidad($tipo,$fecha_inicio,$fecha_fin){
		// echo $fecha_inicio;
		// echo $fecha_fin;
		$this->db->SELECT('*');
    $this->db->from('caja_general');
 	 	//$this->db->join('categoria','categoria.id_categoria=solicitud.divisa_sol');
 	 	$this->db->where('tran_tipo',$tipo);
		$this->db->where('fecha >=',$fecha_inicio);
		$this->db->where('fecha <=',$fecha_fin);
		$this->db->order_by('fecha','desc');
		$this->db->order_by('hora_conta','desc');
    $resultado = $this->db->get();
    return $resultado->result_array();
	}
	function revertir_venta_general($obj){
		//print_r($obj);
		$this->db->SELECT('cantidad_gen');
    $this->db->from('objeto_general');
 	 	//$this->db->join('categoria','categoria.id_categoria=solicitud.divisa_sol');
 	 	$this->db->where('nombre_objeto_gen',$obj['nombre_gen']);
		$this->db->where('categoria_gen',$obj['categoria_gen']);
    $resultado = $this->db->get();
		$cantidad=$resultado->result_array();
		//print_r($cantidad[0]['cantidad_gen']);
    //return $resultado->result_array();
		if($cantidad==''){
			return false;
		}
		else{
			$cant_final=intval($cantidad[0]['cantidad_gen'])+intval($obj['cantidad_gen']);
			//print_r($cantidad[0]['cantidad_gen']);
			//echo $obj['cantidad_gen'];
			//return;
			$this->db->set('cantidad_gen',$cant_final);
			$this->db->where('nombre_objeto_gen',$obj['nombre_gen']);
			$this->db->where('categoria_gen',$obj['categoria_gen']);
			$this->db->update('objeto_general');
			if($this->db->affected_rows()==0){
				return false;
			}
			else{
				$this->db->where('id_trans_gen', $obj['id_trans_gen']);
				$this->db->where('fecha_gen', $obj['fecha_gen']);
				$this->db->where('hora_gen', $obj['hora_gen']);
				$this->db->where('ced_cajero_gen', $obj['ced_cajero_gen']);
				$res2=$this->db->delete('transacciones_general');
				if ($res2) {
					// code...
					$this->db->where('tran_tipo', 'GENERAL');
					$this->db->where('fecha', $obj['fecha_gen']);
					$this->db->where('hora_conta', $obj['hora_gen']);
					$this->db->where('cajero_conta', $obj['ced_cajero_gen']);
					$res3=$this->db->delete('caja_general');
					if ($res3) {
						return true;
					} else {
						return false;
					}

				}else{
					return false;
				}
			}
			//return true;
			//return $resultado->result_array();
		}
	}
	public function cajeros_general_transacciones($obj){
		$this->db->SELECT('*');
 	 	$this->db->from('transacciones_general');
	 	$this->db->join('general','general.id_general=transacciones_general.categoria_gen');
 	 	$this->db->where('fecha_gen', $obj['fecha']);
 	 	$this->db->where('ced_cajero_gen', $obj['cajero']);
	 	$this->db->order_by('categoria_gen', 'asc');
	 	$this->db->order_by('fecha_gen', 'asc');
	 	$resultado = $this->db->get();
   	return $resultado->result_array();
	}
	public function eliminar_transaccion_seguros($obj){
		$this->db->where('placa', $obj['placa']);
		$this->db->where('fecha_seg', $obj['fecha_seg']);
		$this->db->where('hora_seg', $obj['hora_seg']);
		$this->db->where('ced_cajero_seg', $obj['ced_cajero_seg']);
		$res2=$this->db->delete('transacciones_seguros');
		if ($res2) {
			// code...
			$this->db->where('tran_tipo', 'SEGUROS');
			$this->db->where('fecha', $obj['fecha_seg']);
			$this->db->where('hora_conta', $obj['hora_seg']);
			$this->db->where('cajero_conta', $obj['ced_cajero_seg']);
			$res3=$this->db->delete('caja_general');
			if ($res3) {
				return true;
			} else {
				return false;
			}

		}else{
			return false;
		}
	}
//final de todo
}
