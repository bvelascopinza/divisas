<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sesion extends CI_Model {

  function __construct()
	{
		parent::__construct();
    $this->load->library('session');
		$this->load->database();
	}

  function consultar_usr($usuario,$clave){
  $this->db->where('cedula',$usuario);
  $this->db->where('clave',$clave);
  $res=$this->db->get('usuarios');

  if ($res->num_rows()==0) {
    return 'Usuario o Contraseña Incorrecta';
  }else{
    $reg=$res->row();
    $this->session->set_userdata('nombre',$reg->nombre);
    $this->session->set_userdata('apellido',$reg->apellido);
    //$this->session->set_userdata('correo_usuario',$reg->correo_usuario);
    $this->session->set_userdata('tipo',$reg->tipo);
    $this->session->set_userdata('cedula',$reg->cedula);
    // return 'bienbenido';
    return $res->result_array();
  }
}

  function consultar_tipo($tipo){
    $tipo1=$tipo['mensaje'][0]['tipo'];
    switch ($tipo1) {
      case 'c':
        return 'c';
      break;
      case 'a':
        return 'a';
      break;
    }
  }
}
