<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<head>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <script src="<?php echo base_url(); ?>static/js/push.js"></script>
  <script src="<?php echo base_url(); ?>static/js/inventario.js"></script>
</head>
<div class="container" style="padding:1em;display:none" id="div_inventario">
  <div class="">
    <select class="form-control form-control-sm col-sm-4" name="" id="sel_principal" onchange="cargar_div();" style="background:white;">
      <option value="" >Categoria Principal</option>
      <option value="1" >DIVISAS</option>
      <!--<option value="2" >SEGUROS</option>-->
      <option value="3" >GENERAL</option>
    </select>
  </div>
  <div class="container" align="center" style="padding:1em;display:none;font-size:x-large" id="div_inventario_divisas">
    <div class="row">
      <button class="btn btn-primary" type="button" name="button" onclick="nueva_divisa();">Nueva Divisa</button>
      <button class="btn btn-primary" type="button" name="button" id="new_ralacion">Nueva Relacion</button>
      <!-- <button class="btn btn-primary" type="button" name="button" id="agregar_objeto">Agregar Objeto</button> -->
      <button class="btn btn-primary" type="button" name="button" id="actualizar_relacion">Atualizar Relacion</button>
      <!-- <button class="btn btn-primary" type="button" name="button" id="actualizar_inventario">Atualizar Inventario</button> -->
    </div>
    <div class="row">
      <div class="col-sm-3">
        <div class="row">
          <label><span class="badge badge-info">Divisa origen:</span></label>
          <select class="form-control form-control-sm" id="sel_categoria_origen" style="background:white;" onchange=" consultar_categoria_menos();">
            <option value=""> seleccione una categoria</option>
          </select>
        </div>
        <div class="row" id="div_sel_2">
          <label><span class="badge badge-info">Divisa destino:</span></label>
          <select class="form-control form-control-sm" id="sel_categoria_destino" style="background:white;" onchange="">
            <option value=""> seleccione un objeto</option>
          </select>
        </div>
      </div>
      <div class="col-sm-9" id="contenedor_proceso">
        <div class="row">
          <label class="col-sm-3"><span class="badge badge-info"> Precio Base:</span></label>
          <input class="form-control col-sm-3" type="text" name="" value="" onkeyup="format(this)" onchange="format(this)" id="precio_base" style="background:white">
          <label class="col-sm-2"><span class="badge badge-info"> Cantidad:</span></label>
          <input class="form-control col-sm-4" type="text" onkeyup="format(this)" onchange="format(this)" name="" value="" type="number" id="cantidad_base" style="background:white">
        </div>
        <div class="row">
          <div class="col-sm-4">
            <label class=""><span class="badge badge-info">Precio Venta 1:</span></label>
            <input class="form-control" type="text" onkeyup="format(this)" onchange="format(this)" value="" id="input_venta1" style="background:white">
          </div>
          <div class="col-sm-4">
            <label class=""><span class="badge badge-info">Precio Venta 2:</span></label>
            <input class="form-control" type="text" onkeyup="format(this)" onchange="format(this)" name="" value="" id="input_venta2" style="background:white">
          </div>
          <div class="col-sm-4">
            <label class=""><span class="badge badge-info">Precio Venta 3:</span></label>
            <input class="form-control" type="text" onkeyup="format(this)" onchange="format(this)" name="" value="" id="input_venta3" style="background:white">
          </div>
        </div>
        <hr>
        <!-- <div class="row">
          <div class="col-sm-4">
            <label class=""><span class="badge badge-info">Precio Compra 1:</span></label>
            <input class="form-control" type="text" onkeyup="format(this)" onchange="format(this)" name="" value="" id="input_compra1" style="background:white">
          </div>
          <div class="col-sm-4">
            <label class=""><span class="badge badge-info">Precio Compra 2:</span></label>
            <input class="form-control" type="text" onkeyup="format(this)" onchange="format(this)" name="" value="" id="input_compra2" style="background:white">
          </div>
          <div class="col-sm-4">
            <label class=""><span class="badge badge-info">Precio Compra 3:</span></label>
            <input class="form-control " type="text" onkeyup="format(this)" onchange="format(this)" name="" value="" id="input_compra3" style="background:white">
          </div>
        </div> -->
      </div>
    </div>
    <hr>
    <div class="" align='center' id="botones_relacion">
      <button type="button" class="btn btn-success" name="button" id="agregar_relacion_divisas">Agregar Relacion</button>
    </div>

    <hr>
     <div class="container"  id="div_complemento_inventario">
    </div>


    <input type="hidden" value="<?php echo base_url(); ?>" id="url">
  </div>
  <div class="container" align="center" style="padding:1em;display:none" id="div_inventario_general">
    <button class="btn btn-info" type="button" name="button" id="btn_agregar_cat_gen" onclick="agregar_categoria_general();">Agregar Categoria</button>
    <button class="btn btn-info" type="button" name="button" id="btn_agregar_obj_gen" onclick="agregar_objeto_general();">Agregar Objeto</button>
    <button class="btn btn-info" type="button" name="button" id="btn_actulizar_obj_gen" onclick="actualizar_obj_genral();">Atualizar Objeto</button>
    <button class="btn btn-info" type="button" name="button" id="btn_ver_obj" onclick="ver_obj_genral();">Ver objetos</button>
    <button class="btn btn-danger" type="button" name="button" onclick="eliminar_categoria_general();">Eliminar Categoria</button>
    <div class="" id="complemento_general">
    </div>
  </div>
  <div class="container" align="center" style="padding:1em;display:none" id="div_inventario_seguros">
    <button class="btn btn-success" type="button" name="button" id="btn_agregar_cat_gen" onclick="agregar_categoria_seguros();">Agregar Categoria</button>
    <button class="btn btn-success" type="button" name="button" id="btn_agregar_obj_gen" onclick="agregar_objeto_seguros();">Agregar Objeto</button>
    <button class="btn btn-success" type="button" name="button" id="btn_actulizar_obj_gen" onclick="actualizar_obj_seguros();">Atualizar Objeto</button>
    <button class="btn btn-success" type="button" name="button" id="btn_ver_obj" onclick="ver_obj_seguros();">Ver objetos</button>
    <button class="btn btn-danger" type="button" name="button" onclick="eliminar_categoria_seguros();">Eliminar Categoria</button>
    <div class="" id="complemento_seguros">
    </div>
  </div>
</div>

<div class="modal" id="modal_principal">
  <div class="modal-dialog" >
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

          <label class="modal-title" id="modal_principal_titulo"></label>


        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->

      <div class="modal-body">
          <!-- <div id="btns_head_modal"></div>
          <hr> -->
        <div class="fluid-container" id="modal_body_principal" style="" align="center">
          <div class="row">
            <span class="badge badge-primary">Divisa</span>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-plus"></i></span>
              </div>
              <input type="text" class="form-control col-sm-4" placeholder="EJ: Dolar-Peso-Euro" onkeypress="" id="input_divisa_nueva" style="">
            </div>
          </div>

        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <div id="btns_modal_foter"></div>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info" data-dismiss="modal" id="btn_guardar_nueva_divisa">Guardar</button>
      </div>

    </div>
  </div>
</div>
