<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>static/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    FACTURACION
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->

  <link href="<?php echo base_url(); ?>static/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>static/css/now-ui-kit.css?v=1.3.0" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>static/css/style.css" rel="stylesheet" />
  <script src="<?php echo base_url(); ?>static/js/core/jquery.min.js"></script>
  <!-- CSS Just for demo purpose, don't include it in your project -->

</head>

<body class="landing-page sidebar-collapse">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
    <div class="container">
      <div class="dropdown button-dropdown">
        <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
          <span class="button-bar"></span>
          <span class="button-bar"></span>
          <span class="button-bar"></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-header">Menu Principal</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/cajero">Caja</a>
          <a class="dropdown-item" ><button type="button" class="btn btn-success" name="button" onclick="mostar_informe_cajero();">ENTREGA DIVISAS</button></a>
          <a class="dropdown-item" ><button type="button" class="btn btn-success" name="button" onclick="mostar_informe_cajero_general();">ENTREGA GENERAL</button></a>
          <!--<a class="dropdown-item" ><button type="button" class="btn btn-success" name="button" onclick="mostar_informe_cajero_seguro();">ENTREGA SEGUROS</button></a>-->
          <a class="dropdown-item" ><button type="button" class="btn btn-info" name="button" onclick="ingresos_egresos();">SOLICITUD</button></a>
          <a class="dropdown-item" ><button type="button" class="btn btn-info" name="button" onclick="divisas_reporte_cajero();">REPORTES DIVISAS</button></a>

          <!-- <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/inventario">Inventario</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/base">Base Diaria</a>
          <a class="dropdown-item" href="#">Contabilidad</a>
          <a class="dropdown-item" href="#">Reportes</a> -->
          <div class="dropdown-divider"></div>
          <button class="dropdown-item" id="calculadora"><span class="badge badge-primary">Calculadora</span></button>
          <button class="dropdown-item" onclick="herramienta_contadora()"><span class="badge badge-primary">Herramienta Apoyo</span></button>
          <!-- <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">One more separated link</a> -->
        </div>
      </div>
      <div class="navbar-translate">
        <a class="navbar-brand" href="" rel="tooltip" title="Desplegar menu Principal" data-placement="bottom" target="_blank">
          Menu
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar top-bar"></span>
          <span class="navbar-toggler-bar middle-bar"></span>
          <span class="navbar-toggler-bar bottom-bar"></span>
        </button>
      </div>
      <div class="text-center">

      </div>


      <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="<?php echo base_url(); ?>static/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
          <li class="nav-item">
            <!-- <a class="nav-link" href="<?php echo base_url(); ?>Divisas/inventario">INVENTARIO</a> -->
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Divisas/cajero">CAJA</a>
          </li>


           <div class="dropdown button-dropdown">
              <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                  <img src="<?php echo base_url();?>assets/img/usuario.png" class="rounded-circle img-raised" alt="">
              </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-header">
              Menu Usuario
            </a>
            <a class="dropdown-item">
              <span id="">
              <?php
               echo $this->session->userdata('nombre')." ".$this->session->userdata('apellido')." -> ";
               ?>
            </span>
            <span id="cedula_login">
            <?php
             echo $this->session->userdata('cedula');
             ?>
          </span>
            </a>

            <div class="dropdown-divider"></div>
            <!--<button class="dropdown-item" ><span  id="boton_turno" class="badge badge-success" onclick="iniciar_turno()">INICIAR TURNO</span></button>-->
            <button class="dropdown-item" id="calculadora"><span class="badge badge-primary"><a href="<?php echo base_url ();?>login/cerrar">Salir</a></span></button>
            
            <!-- <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">One more separated link</a> -->
          </div>
        </div>




        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper">
    <div class="page-header page-header-large">
      <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>static/img/bg3.jpg');">
      </div>
      <div class="container">
        <div class="container" id="div_principal">
          <h1 class="title" id="titulo_principal">CONTINENTAL-Divisas</h1>
          <div class="cols-sm-12 rounded-lg" id='contenedor_principal_divisas'>

          </div>
          <!-- <div class="row justify-content-center">
            <div class="col-sm-4">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="width:200px" align="center">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item">
                    <img class="d-block" src="<?php echo base_url(); ?>static/images/estado.png" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">

                    </div>
                  </div>
                  <div class="carousel-item active">
                    <img class="d-block" src="<?php echo base_url(); ?>static/images/equidad.png" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">

                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block" src="<?php echo base_url(); ?>static/images/estado.png" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">

                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
              </div>
            </div>
          </div> -->

        </div>
      </div>
    </div>
    <!-- <div class="section section-about-us">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Who we are?</h2>
            <h5 class="description">According to the National Oceanic and Atmospheric Administration, Ted, Scambos, NSIDClead scentist, puts the potentially record low maximum sea ice extent tihs year down to low ice extent in the Pacific and a late drop in ice extent in the Barents Sea.</h5>
          </div>
        </div>
        <div class="separator separator-primary"></div>
        <div class="section-story-overview">
          <div class="row">
            <div class="col-md-6">
              <div class="image-container image-left" style="background-image: url('<?php echo base_url(); ?>static/img/login.jpg')">

                <p class="blockquote blockquote-primary">"Over the span of the satellite record, Arctic sea ice has been declining significantly, while sea ice in the Antarctichas increased very slightly"
                  <br>
                  <br>
                  <small>-NOAA</small>
                </p>
              </div>

              <div class="image-container" style="background-image: url('<?php echo base_url(); ?>static/img/bg3.jpg')"></div>
            </div>
            <div class="col-md-5">



              <div class="image-container image-right" style="background-image: url('<?php echo base_url(); ?>static/img/bg1.jpg')"></div>
              <h3>So what does the new record for the lowest level of winter ice actually mean</h3>
              <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
              </p>
              <p>
                For a start, it does not automatically follow that a record amount of ice will melt this summer. More important for determining the size of the annual thaw is the state of the weather as the midnight sun approaches and temperatures rise. But over the more than 30 years of satellite records, scientists have observed a clear pattern of decline, decade-by-decade.
              </p>
              <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <!-- <div class="section section-team text-center">
      <div class="container">
        <h2 class="title">Here is our team</h2>
        <div class="team">
          <div class="row">
            <div class="col-md-4">
              <div class="team-player">
                <img src="<?php echo base_url(); ?>static/img/avatar.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                <h4 class="title">Romina Hadid</h4>
                <p class="category text-primary">Model</p>
                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                  <a href="#">links</a> for people to be able to follow them outside the site.</p>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-twitter"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-instagram"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-facebook-square"></i></a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="team-player">
                <img src="<?php echo base_url(); ?>static/img/ryan.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                <h4 class="title">Ryan Tompson</h4>
                <p class="category text-primary">Designer</p>
                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                  <a href="#">links</a> for people to be able to follow them outside the site.</p>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-twitter"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-linkedin"></i></a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="team-player">
                <img src="<?php echo base_url(); ?>static/img/eva.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                <h4 class="title">Eva Jenner</h4>
                <p class="category text-primary">Fashion</p>
                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                  <a href="#">links</a> for people to be able to follow them outside the site.</p>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-google-plus"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-youtube"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-twitter"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- <div class="section section-contact-us text-center">
      <div class="container">
        <h2 class="title">Want to work with us?</h2>
        <p class="description">Your project is very important to us.</p>
        <div class="row">
          <div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">
            <div class="input-group input-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="now-ui-icons users_circle-08"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="First Name...">
            </div>
            <div class="input-group input-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="now-ui-icons ui-1_email-85"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="Email...">
            </div>
            <div class="textarea-container">
              <textarea class="form-control" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>
            </div>
            <div class="send-button">
              <a href="#pablo" class="btn btn-primary btn-round btn-block btn-lg">Send Message</a>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <footer class="footer footer-default">
      <div class=" container ">
        <nav>
          <ul>
            <li>
              <a href="#">
                <!-- <img src="<?php echo base_url(); ?>static/images/efecty-logo.png" alt=""> -->
              </a>
            </li>
            <li>
              <a href="#">
                Sobre la Empresa
              </a>
            </li>
            <li>
              <a href="#">
                Informacion CONTINENTAL SOFTWARE
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright" id="copyright">
          &copy;
          <script>
            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
          </script><!--, Designed by
          <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
          <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.-->
        </div>
      </div>
    </footer>
  </div>
  <div class="modal" id="modal_calculadora" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Calculadora...</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="row">
              <div class="col-sm-8">
                <input class="col-sm-12" type="numer" id="input_1_calc" value="">
              </div>
              <!-- <div class="col-sm-1">
                <span class="badge badge-info" id="operador">op</span>
              </div>
              <div class="col-sm-3">
                <input class="col-sm-12" type="numer" id="input_2_calc" value="">
              </div> -->
              <div class="col-sm-1">
                =
              </div>
              <div class="col-sm-3">
                <h5><span class="badge badge-success" id="result_calc"></span></h5>
              </div>

            </div>
            <div class="row">

              <div class="col-sm-4" align="center">
                <span class="badge badge-info"> Teclado</span>
                <div class="row">
                  <div class="col-sm-4">
                  <button type="button" name="numero" data-numero='1' onclick="tecla_press(this);"><span class="badge badge-primary">1</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='2' onclick="tecla_press(this);"><span class="badge badge-primary">2</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='3' onclick="tecla_press(this);"><span class="badge badge-primary">3</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='4' onclick="tecla_press(this);"><span class="badge badge-primary">4</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='5' onclick="tecla_press(this);"><span class="badge badge-primary">5</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='6' onclick="tecla_press(this);"><span class="badge badge-primary">6</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='7' onclick="tecla_press(this);"><span class="badge badge-primary">7</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='8' onclick="tecla_press(this);"><span class="badge badge-primary">8</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='9' onclick="tecla_press(this);"><span class="badge badge-primary">9</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='0' onclick="tecla_press(this);"><span class="badge badge-primary">0</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='c' onclick="tecla_press(this);"><span class="badge badge-primary">C</span></button>
                  </div>
                  <div class="col-sm-4">

                  </div>
                </div>
              </div>
              <!-- <div class="col-sm-4">
                <span class="badge badge-info">opciones basicas</span>
                <button style="width:60px" title="SUMA" type="button" name="numero" data-numero='+' onclick="tecla_press(this);"><span class="badge badge-primary">+</span></button>
                <button style="width:60px" title="RESTA" type="button" name="numero" data-numero='-' onclick="tecla_press(this);"><span class="badge badge-primary">-</span></button>
                <button style="width:60px" title="MULTIPLICACIÓN" type="button" name="numero" data-numero='*' onclick="tecla_press(this);"><span class="badge badge-primary">*</span></button>
                <button style="width:60px" title="DIVISIÓN" type="button" name="numero" data-numero='/' onclick="tecla_press(this);"><span class="badge badge-primary">/</span></button>
                <button style="width:120px" title="IGUAL" type="button" name="numero" data-numero='=' onclick="tecla_press(this);"><span class="badge badge-success">=</span></button>
              </div> -->
              <div class="col-sm-4">
                <span class="badge badge-info"> opciones de divisas</span>
                <select class="form-control form-control-sm" id="sel_origen_modal" onchange="cargar_destino();">
                  <option value="">origen</option>
                </select>
                <select class="form-control form-control-sm" id="sel_destino_modal" onchange="opciones_venta_compra();">
                  <option value="">destino</option>
                </select>
                <select class="form-control form-control-sm" id="modal_compra" onchange="calcular_compra();">
                  <option value="">compra</option>
                </select>
                <input id="otro_modal_compra" type="text" name="" value="" placeholder="otro valor compra" onkeypress="otro_compra_calculadora(event);" onkeyup="format(this)" onchange="format(this)">
                <select class="form-control form-control-sm" id="modal_venta" onchange="calcular_venta();">
                  <option value="">venta</option>
                </select>
                <input id="otro_modal_venta" type="text" name="" value="" placeholder="otro valor venta" onkeypress="otro_venta_calculadora(event);" onkeyup="format(this)" onchange="format(this)">

              </div>
            </div>



        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>static/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?php echo base_url(); ?>static/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?php echo base_url(); ?>static/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="<?php echo base_url(); ?>static/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url(); ?>static/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/calculadora.js" type="text/javascript"></script>
  <!-- <script src="<?php echo base_url(); ?>static/js/caja.js"></script> -->
</body>

</html>
<div class="modal" id="modal_informe_cajeros" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">Informe Cajeros</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- <select class="" name="" id="cajeros_select_modal">
        </select>
        <input type="date" name="" value="" id="cajeros_fecha_input">
        <button type="button" name="button" onclick="mostar_informe_cajero();">REVISION</button> -->
        <div class="container-fluid" id="contenedor_informe_cajeros" style="font-size:large">

        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_reporte_divisas_cajeros" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">Registros Divisas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="date" name="" value="" id="fecha1">
        <input type="date" name="" value="" id="fecha2">
        <div class="table-responsive" style="height:400px;overflow:auto">
          <table class="table table-hovered">
            <thead class="thead-dark">
              <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Divisa 1</th>
                <th>Divisa 2</th>
                <th>Cantidad</th>
                <th>Precio Transaccion</th>
                <th>Total</th>
                <th>Tipo Transaccion</th>
                <th>Ced Cajero</th>
                <th>Nombre Caj</th>
                <th>Cliente</th>
                <th>Nombre Cliente</th>
                <th>Acciones</th>
              </tr>
            </thead>
              <tbody id="body_tabla_modal">

              </tbody>

          </table>
        </div>

      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-info" name="button" onclick="ingresos_egresos();">SOLICITUD</button>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_solicitud_cajeros" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">Solicitud</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select class="col-sm-2" name="" id="sel_divisa_ingresos_egresos">

        </select>
        <select class="col-sm-2" name="" id="sel_precios_ingresos_egresos">

        </select>
        <input class="col-sm-2" type="text" name="" value="" onchange="format(this);" onkeyup="format(this);" id="input_ingreso_egreso">
        <button type="button" name="button" data-tipo="ingreso" data-dismiss="modal" class="col-sm-2 btn btn-info "onclick="guardar_ingreso_egreso(this);">INGRESO</button>
        <button type="button" name="button" data-tipo="egreso" data-dismiss="modal" class="col-sm-2 btn btn-info" onclick="guardar_ingreso_egreso(this);">EGRESO</button>

      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_formulario_18" tabindex="-1" role="dialog" style="overflow:auto">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">formulario 18</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container" id="modal_body_formulario_18" align="center">
          <table>
            <tr>
              <td>CONTINENTAL CAMBIOS <br>
              Profesional en Compra y Venta de Divisas y Cheque Viajeros <br>
              Cra 2 #2-81 barrio la laguna Terminal de Ípiales local 47 2do piso
              </td>
              <td>
                <p>formulario N° 18</p>
                <p>Factura N° <input id="numero_factura_formulario_18" value="" placeholder="numero factura"></p>
              </td>
            </tr>
          </table>
          <table>
            <tr>
              <td><img src="<?php echo base_url(); ?>static/images/banco_reoublica _colombia.jpeg" alt=""></td>
              <td>
                <b>Declaración de Compra y Venta de Divisas y Cheque viajeros<br>Formulario N°18<br>
                </b>Circular Regamentaria Externa DCIN-83 DE OCTUBRE DE 2017<br>Original Y Copia
              </td>
            </tr>
          </table>
          <div class="container-fluid">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>1.Ciudad</td>
                  <td>2.Fecha</td>
                </tr>
                <tr>
                  <td>ÍPIALES</td>
                  <td id="fecha_hoy_18">11/12/34</td>
                </tr>
              </tbody>
            </table>
            <b>I. IDENTIFICACIÓN DEL PROFESIONAL DE CAMBIO</b>
            <table class="table table-bordered">
              <tr>
                <td>1. Nombre o Razón Social</td>
                <td>2.Tipo</td>
                <td>3.Número Identificación</td>
              </tr>
              <tr>
                <td>CARLOS EFRAIN REVELO ERIRA</td>
                <td>CC</td>
                <td>1.085.901.197</td>
              </tr>
              <tr>
                <td>4.Número de Factura</td>
                <td colspan="2">5. Matrícula del Establecimiento de Comercio</td>
              </tr>
              <tr>
                <td> <input type="text" id="numero_de_factura_18" value="" placeholder="numero factura"> </td>
                <td colspan="2">41074</td>
              </tr>
            </table>
            <b>II. IDENTIFICACIÓN DEL CLIENTE(datos del residente que compra o vende divisas o cheques de viajeros)</b>
            <table class="table table-bordered">
              <tr>
                <td>1.Nombre o Razón social</td>
                <td>2.Tipo</td>
                <td>3.Número de Identificación</td>
              </tr>
              <tr>
                <td id="nombre_cliente_18">Angel Jose Romero Romero</td>
                <td id="tipo_cliente_18"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="C.C"></td>
                <td id="identificacion_cliente_18">123456789</td>
              </tr>
              <tr>
                <td colspan="2">4.Dirección</td>
                <td>5.Ciudad</td>
              </tr>
              <tr>
                <td colspan="2" id="dirreccion_cliente_18"> <input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="CRA 4 13-45 BARRIO BOLIVAR"></td>
                <td id="ciudad_cliente"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="Cali"></td>
              </tr>
              <tr>
                <td>6. Teléfono</td>
                <td colspan="2">7. Actividad Economica</td>
              </tr>
              <tr>
                <td> <input id="telefono_cliente_18_rep" type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="307865423"></td>
                <td colspan="2"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="Comerciante"></td>
              </tr>
            </table>
            <b>III. IDENTIFICACIÓN DEL DECLARANTE(datos de la persona natural que siscribe la declaracion, en nombre propio o representacion del cliente)</b>
            <table class="table table-bordered">
              <tr>
                <td>1.Nombre o Razón Social</td>
                <td>2.Tipo</td>
                <td>3.Número Identificación</td>
              </tr>
              <tr>
                <td>Carlos Revelo</td>
                <td>CC</td>
                <td>1.085.901.197</td>
              </tr>
              <tr>
                <td>4.Dirección</td>
                <td>5.Ciudad</td>
                <td>6.Teléfono</td>
              </tr>
              <tr>
                <td>Terminal local 47 2do piso</td>
                <td>Ipiales</td>
                <td>7731277</td>
              </tr>
            </table>
            <b>IV. DESCRIPCIÓN DE LA OPERACIÓN</b><br>
            1.Concepto(seleccione la opcion):<br>
            <input type="radio" name="concepto" value="">Compra Divisas   <input type="radio" name="concepto" value="">Venta Divisas
            <table class="table table-bordered">
              <tr>
                <td>2.Nombre de la moneda negociada</td>
                <td>3.Monto de la moneda negociada</td>
              </tr>
              <tr>
                <td id="moneda_negociada_18">DOLAR</td>
                <td id="monto_negociado_18">2745000</td>
              </tr>
              <tr>
                <td>4.Tasa de cambio</td>
              </tr>
              <tr>
                <td id="tasa_cambio_18">3050</td>
              </tr>
            </table>
            valor en pesos de la operacion
            <table class="table table-bordered">
              <tr>
                <td>5.efectivo</td>
                <td> <input id="input_efectivo_18" type="text" style="borded-style:none;border:0" value=""></td>
              </tr>
              <tr>
                <td>6.cheque de viajero</td>
                <td> <input id="input_cheque_18" type="text" style="borded-style:none;border:0" value="COP$"></td>
              </tr>
              <tr>
                <td>7.pago diferente a efectivo</td>
                <td> <input id="input_diferente_18" type="text" style="borded-style:none;border:0" value="COP$"></td>
              </tr>
              <tr>
                <td>8.TOTAL</td>
                <td id="total_18" class="fondo_verde">900000</td>
              </tr>
            </table>
            <p align="justify" style="font-size:small">
            PARA LOS FINES PREVISTOS EN EL ARTICULO 83 DE LA CONSTITUCIÓN POLITICA DE COLOMBIA,DECLARO BAJO LA GRAVEDAD DE JURAMENTO QUE LOS CONCEPTOS, CANTIDADES Y DEMAS DATOS CONSIGNADOS EN EL PRESENTE FORMULARIO SON CORRECTOS Y FIEL EXPRESIÓN DE LA VERDAD CUANDO LA DECLARACION SE REALICE ENTRE DOS (2) PROFESIONALES DEL CAMBIO SE ELABORA UNA (1) SOLA DECLARACIÓN DE CAMBIO. LA DECLARACIÓN LA DEBE DILIGENCIAR EL BENEFICIARIO O CLIENTE EN LA PAPELERIA SUMINISTRADA POR EL PROFESIONAL QUE RECIBE Y ACEPTA LA OFERTA DEL BENEFICIARIO
            </p>
            <div class="row">
              <div class="col-sm-6">
                <hr style="color:black">
                FIRMA DEL DECLARANTE
              </div>
              <div class="col-sm-6">
                HUELLA INDICE DERECHO
                <div style="height:5em;width:5em;border:solid black">

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <button type="button" onclick="imprimir_formulario_18();"  name="button"> Imprimir</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_formulario_2" tabindex="-1" role="dialog" style="overflow:auto">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">formulario 536</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container" id="modal_body_formulario_2" align="center" style="font-size:x-small">
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <tr style="padding:0px;">
              <td rowspan="2" style="padding:0px;">
                  <img src="<?php echo base_url(); ?>static/images/dian.jpeg" alt="" height="30px">
              </td>
              <td rowspan="2" style="padding:0px;">
                Declaracion de Cambio simplificada por Compra y Venta<br>Profesional de Divisas en Efectivo y Cheques de Viajero<br> en Zonas de Frontera
              </td>
              <td rowspan="1" style="padding:0px;">
                <img src="<?php echo base_url(); ?>static/images/muisca.jpeg" alt="">
              </td>
              <td style="padding:0px;">
                536
              </td>
            </tr>
          </table>
          <table class="" style="text-align:center;margin-bottom:0px;padding:0px">
            <tr style="padding:0px;">
              <td style="padding:0px;">1.Año</td>
              <td id="año_536" style="padding:0px;">2019</td>
              <td style="padding:0px;">2.Concepto: Compra <input type="radio" name="concepto" value="true"> Venta <input type="radio" name="concepto" value=""></td>
              <td style="padding:0px;">24. Número Declaracion</td>
              <td style="padding:0px;"> <input type="text" id="numero_factura_536" value="" placeholder="numero factura"> </td>
            </tr>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px;padding:0px">
            <td rowspan="4" style="padding:0px;">
                Datos Profesional Cambista
                <!-- D <br>a<br>t<br>o<br>s<br>P<br>r<br>o<br>f<br>e<br>s<br>i<br>o<br>n<br>a<br>l -->

            </td>
            <td rowspan="2" style="padding:0px;">
              <div class="container">
                <table class="table table-bordered">
                  <thead>
                    <tr class="fondo_verde sin_margen">
                      <th style="padding:0px;">5.Número de Identificación Tributaria(NIT)</th>
                      <th style="padding:0px;">6.DV</th>
                      <th style="padding:0px;">7.Primer Apellido</th>
                      <th style="padding:0px;">8.Segundo Apellido</th>
                      <th style="padding:0px;">9.Primer Nombre</th>
                      <th style="padding:0px;">10.Otros Nombres</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr style="padding:0px;">
                      <td style="padding:0px;">1|0|8|5|9|0|1|1|9|7|-|</td>
                      <td style="padding:0px;">|9|</td>
                      <td style="padding:0px;">REVELO</td>
                      <td style="padding:0px;">ERIRA</td>
                      <td style="padding:0px;">CARLOS</td>
                      <td style="padding:0px;">EFRAIN</td>
                    </tr>
                    <tr class="table-success sin_margen">
                      <th colspan="6" style="padding:0px;">11.Razón Social</th>
                    </tr>
                    <tr style="padding:0px;">
                      <td colspan="6" style="padding:0px;"> <input class="form-control" type="text" style="font-size:x-small;border:0" value="" placeholder="Razón social contenido"> </td>
                    </tr>
                  </tbody>

                </table>
              </div>

            </td>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <thead >
              <tr style="padding:0px;">
                <th  colspan="6" style="padding:0px;" style="background-color:#c2e6cb">Datos del Cliente del Profesional en Compra y Venta de Divisas en Efectivo y Cheques de Viajero</th>
              </tr>
            </thead>
            <tbody>
              <tr style="padding:0px;">
                <td style="padding:0px;">
                  25. Tipo de Documento:
                </td>
                <td style="padding:0px;">
                  T.I. <input type="radio" name="tipo_doc" value=""> <br>
                  C.C. <input type="radio" name="tipo_doc" value="">
                </td>
                <td colspan="2" style="padding:0px;">
                  Tarjeta de extranjeria. <input type="radio" name="tipo_doc" value=""> <br>
                  Cedula extranjeria. <input type="radio" name="tipo_doc" value="">
                </td>
                <td style="padding:0px;">
                  NIT. <input type="radio" name="tipo_doc" value=""> <br>
                  Pasaporte <input type="radio" name="tipo_doc" value="">
                </td>
                <td style="padding:0px;">
                  Documento de extranjero <input type="radio" name="tipo_doc" value=""> <br>
                </td>
              </tr>
              <tr style="padding:0px;">
                <td style="padding:0px;">
                  26. N° Documento Identificación:
                </td>
                <td style="padding:0px;">
                  27. Dv
                </td>
                <td style="padding:0px;">
                  28. Primer Apellido
                </td>
                <td style="padding:0px;">
                  29. Segundo Apellido
                </td>
                <td style="padding:0px;">
                  30. Primer Nombre
                </td>
                <td style="padding:0px;">
                  31.Otros Nombres
                </td>
              </tr>
              <tr style="padding:0px;">
                <td id="cedula_cliente_536" style="padding:0px;">01774</td>
                <td style="padding:0px;">
                  <input class="form-control" style="border:0;font-size:x-small" type="text" name="" value="" placeholder="dv">
                </td>
                <td id="primer_apellido_536" style="padding:0px;">

                </td>
                <td id="segundo_appellido_536" style="padding:0px;">

                </td>
                <td id="primer_nombre_536" style="padding:0px;">

                </td>
                <td id="segundo_nombre_536" style="padding:0px;">

                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6">32. Razón Social</td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6" style="padding:0px;"> <input style="border:0;font-size:x-small" class="form-control" type="text" name="" value=""> </td>
              </tr>
              <tr style="padding:0px;">
                <td class="table-success sin_margen" colspan="6"> DATOS DE LA OPERACIÓN</td>
              </tr>
              <tr style="padding:0px;">
                <td style="padding:0px;">33. Moneda negociada</td>
                <td style="padding:0px;">34. Cód</td>
                <td style="padding:0px;">35. Monto de la moneda negociada</td>
                <td colspan="2" style="padding:0px;">36. valor en pesos ($)</td>
                <td style="padding:0px;">
                  36.clase de compra o venta
                </td>
              </tr>
              <tr style="padding:0px;">
                <td id="moneda_negociada_536" style="padding:0px;"></td>
                <td style="padding:0px;"> <input class="form-control" style="border:0;font-size:x-small" type="text" name="" value="USD"> </td>
                <td id="monto_negociado_536" style="padding:0px;"></td>
                <td colspan="2" style="padding:0px;" id="valor_pesos_536"></td>
                <td style="padding:0px;">
                  Efectivo <input type="radio" name="clase" value="true" checked> Cheque Viajero<input type="radio" name="clase" value="">
                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6" style="padding:0px;">
                  <p style="text-align:center;font-size:xx-small">
                    Declaración de Cambio prescrita por la dirección de Impuestos y Aduanas Nacionales de conformidad con lo establecido en la circular Reglamentaria Externa DCIN-83 de Mayo 3 de 2006 del Banco de la República cuando la operación se realice entre dos(2) profesionales del cambio se elaborara una(1) sola declaración decambio, la declaración la debe dilinegiar el profesional que recibe y acepta la oferta del cliente
                  </p>
                  <p style="text-align:justify;font-size:xx-small">
                    Advertencia:Esta declaracion simplificada solo puede ser utilizada, presentada y exigida respecto de operaciones de compra o venta de divisas en efectivo o de cheques de viajero celebradas en zonas de frontera por montos inferiores a quinientos dólares de los Estados Unidos de America (USD 500,00) y superiores a docientos dólares de Estados Unidos de America (USD 200,00) o su equivalente en otras monedas. Operaciones iguales o superiores a quinientos dólares de los Estados Unidos de America (USD 500,00), deben ser declaradas en el formulario "Declaración de cambio por compra y venta de manera profesional de divisas y cheques de viajero" incluido en la circular reglamentaria Externa DCIN-83 de 2004 del Banco de la República y en sus posteriores modificaciones.
                  </p>
                  <p></p>
                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="4" style="padding:0px;">
                  <p style="text-align: lefth">Firma cliente</p>
                </td>
                <td colspan="2" style="padding:0px;">
                  997. Fecha Opercion<br>
                  <p id="fecha_hoy_536"></p>
                </td>
              </tr>
            </tbody>

          </table>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <button type="button" onclick="imprimir_formulario_536();"  name="button"> Imprimir</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_formulario_18_rep" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">formulario 18</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container" id="modal_body_formulario_18_rep" align="center" style="font-size:small">
          <table>
            <tr>
              <td>CONTINENTAL CAMBIOS <br>
              Profesional en Compra y Venta de Divisas y Cheque Viajeros <br>
              Cra 2 #2-81 barrio la laguna Terminal de Ípiales local 47 2do piso
              </td>
              <td>
                <p>formulario N° 18</p>
                <p>Factura N° <input id="numero_factura_formulario_18_rep" value="2"></p>
              </td>
            </tr>
          </table>
          <table>
            <tr>
              <td><img src="<?php echo base_url(); ?>static/images/banco_reoublica _colombia.jpeg" alt=""></td>
              <td>
                <b>Declaración de Compra y Venta de Divisas y Cheque viajeros<br>Formulario N°18<br>
                </b>Circular Regamentaria Externa DCIN-83 DE OCTUBRE DE 2017<br>Original Y Copia
              </td>
            </tr>
          </table>
          <div class="container-fluid">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>1.Ciudad</td>
                  <td>2.Fecha</td>
                </tr>
                <tr>
                  <td>ÍPIALES</td>
                  <td id="fecha_hoy_18_rep">11/12/34</td>
                </tr>
              </tbody>
            </table>
            <b>I. IDENTIFICACIÓN DEL PROFESIONAL DE CAMBIO</b>
            <table class="table table-bordered">
              <tr>
                <td>1. Nombre o Razón Social</td>
                <td>2.Tipo</td>
                <td>3.Número Identificación</td>
              </tr>
              <tr>
                <td>CARLOS EFRAIN REVELO ERIRA</td>
                <td>CC</td>
                <td>1.085.901.197</td>
              </tr>
              <tr>
                <td>4.Número de Factura</td>
                <td colspan="2">5. Matrícula del Establecimiento de Comercio</td>
              </tr>
              <tr>
                <td > <input type="text" id="numero_de_factura_18_rep" value=""> </td>
                <td colspan="2">41074</td>
              </tr>
            </table>
            <b>II. IDENTIFICACIÓN DEL CLIENTE(datos del residente que compra o vende divisas o cheques de viajeros)</b>
            <table class="table table-bordered">
              <tr>
                <td>1.Nombre o Razón social</td>
                <td>2.Tipo</td>
                <td>3.Número de Identificación</td>
              </tr>
              <tr>
                <td id="nombre_cliente_18_rep">Angel Jose Romero Romero</td>
                <td id="tipo_cliente_18_rep"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="C.C"></td>
                <td id="identificacion_cliente_18_rep">123456789</td>
              </tr>
              <tr>
                <td colspan="2">4.Dirección</td>
                <td>5.Ciudad</td>
              </tr>
              <tr>
                <td colspan="2" id="dirreccion_cliente_18_rep"> <input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="CRA 4 13-45 BARRIO BOLIVAR"></td>
                <td id="ciudad_cliente"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="Cali"></td>
              </tr>
              <tr>
                <td>6. Teléfono</td>
                <td colspan="2">7. Actividad Economica</td>
              </tr>
              <tr>
                <td><input id="telefono_cliente_18_rep" type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="307865423"></td>
                <td colspan="2"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="Comerciante"></td>
              </tr>
            </table>
            <b>III. IDENTIFICACIÓN DEL DECLARANTE(datos de la persona natural que siscribe la declaracion, en nombre propio o representacion del cliente)</b>
            <table class="table table-bordered">
              <tr>
                <td>1.Nombre o Razón Social</td>
                <td>2.Tipo</td>
                <td>3.Número Identificación</td>
              </tr>
              <tr>
                <td>Carlos Revelo</td>
                <td>CC</td>
                <td>1.085.901.197</td>
              </tr>
              <tr>
                <td>4.Dirección</td>
                <td>5.Ciudad</td>
                <td>6.Teléfono</td>
              </tr>
              <tr>
                <td>Terminal local 47 2do piso</td>
                <td>Ipiales</td>
                <td>7731277</td>
              </tr>
            </table>
            <b>IV. DESCRIPCIÓN DE LA OPERACIÓN</b><br>
            1.Concepto(seleccione la opcion):<br>
            <input type="radio" name="concepto" value="">Compra Divisas   <input type="radio" name="concepto" value="">Venta Divisas
            <table class="table table-bordered">
              <tr>
                <td>2.Nombre de la moneda negociada</td>
                <td>3.Monto de la moneda negociada</td>
              </tr>
              <tr>
                <td id="moneda_negociada_18_rep">DOLAR</td>
                <td id="monto_negociado_18_rep">2745000</td>
              </tr>
              <tr>
                <td>4.Tasa de cambio</td>
              </tr>
              <tr>
                <td id="tasa_cambio_18_rep">3050</td>
              </tr>
            </table>
            valor en pesos de la operacion
            <table class="table table-bordered">
              <tr>
                <td>5.efectivo</td>
                <td> <input id="input_efectivo_18_rep" type="text" style="borded-style:none;border:0" value=""></td>
              </tr>
              <tr>
                <td>6.cheque de viajero</td>
                <td> <input id="input_cheque_18_rep" type="text" style="borded-style:none;border:0" value="COP$"></td>
              </tr>
              <tr>
                <td>7.pago diferente a efectivo</td>
                <td> <input id="input_diferente_18_rep" type="text" style="borded-style:none;border:0" value="COP$"></td>
              </tr>
              <tr>
                <td>8.TOTAL</td>
                <td id="total_18_rep">900000</td>
              </tr>
            </table>
            <p align="justify" style="font-size:small">
            PARA LOS FINES PREVISTOS EN EL ARTICULO 83 DE LA CONSTITUCIÓN POLITICA DE COLOMBIA,DECLARO BAJO LA GRAVEDAD DE JURAMENTO QUE LOS CONCEPTOS, CANTIDADES Y DEMAS DATOS CONSIGNADOS EN EL PRESENTE FORMULARIO SON CORRECTOS Y FIEL EXPRESIÓN DE LA VERDAD CUANDO LA DECLARACION SE REALICE ENTRE DOS (2) PROFESIONALES DEL CAMBIO SE ELABORA UNA (1) SOLA DECLARACIÓN DE CAMBIO. LA DECLARACIÓN LA DEBE DILIGENCIAR EL BENEFICIARIO O CLIENTE EN LA PAPELERIA SUMINISTRADA POR EL PROFESIONAL QUE RECIBE Y ACEPTA LA OFERTA DEL BENEFICIARIO
            </p>
            <div class="row">
              <div class="col-sm-6">
                <hr style="background:black">
                FIRMA DEL DECLARANTE
              </div>
              <div class="col-sm-6">
                HUELLA INDICE DERECHO
                <div style="height:5em;width:5em;border:solid black">

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <button type="button" onclick="imprimir_formulario_18_rep();"  name="button"> Imprimir</button>
          </div>
          <div class="col-sm-8" id="modal_footer_principal_2">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_formulario_2_rep" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">formulario 536</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container" id="modal_body_formulario_2_rep" align="center" style="font-size:x-small">
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <tr style="padding:0px;">
              <td style="padding:0px;" rowspan="2">
                  <img src="<?php echo base_url(); ?>static/images/dian.jpeg" alt="" height="30px">
              </td>
              <td rowspan="2" style="padding:0px;">
                Declaracion de Cambio simplificada por Compra y Venta<br>Profesional de Divisas en Efectivo y Cheques de Viajero<br> en Zonas de Frontera
              </td>
              <td rowspan="1" style="padding:0px;">
                <img src="<?php echo base_url(); ?>static/images/muisca.jpeg" alt="">
              </td>
              <td style="padding:0px;">
                536
              </td>
            </tr>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <tr style="padding:0px;">
              <td style="padding:0px;">1.Año</td>
              <td id="año_536_rep" style="padding:0px;">2019</td>
              <td style="padding:0px;">2.Concepto: Compra <input type="radio" name="concepto" value="true"> Venta <input type="radio" name="concepto" value=""></td>
              <td style="padding:0px;">24. Número Declaracion</td>
              <td style="padding:0px;"><input type="text" id="numero_factura_536_rep" value="" placeholder="numero factura"></td>
            </tr>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <td rowspan="4" style="padding:0px;">
              Datos<br>Profesional<br>Cambista
            </td>
            <td style="padding:0px;" rowspan="2">
              <div class="container">
                <table class="table table-bordered">
                  <thead>
                    <tr class="table-success sin_margen">
                      <th style="padding:0px;">5.Número de Identificación Tributaria(NIT)</th>
                      <th style="padding:0px;">6.DV</th>
                      <th style="padding:0px;">7.Primer Apellido</th>
                      <th style="padding:0px;">8.Segundo Apellido</th>
                      <th style="padding:0px;">9.Primer Nombre</th>
                      <th style="padding:0px;">10.Otros Nombres</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="padding:0px;">1|0|8|5|9|0|1|1|9|7|-|</td>
                      <td style="padding:0px;">|9|</td>
                      <td style="padding:0px;">REVELO</td>
                      <td style="padding:0px;">ERIRA</td>
                      <td style="padding:0px;">CARLOS</td>
                      <td style="padding:0px;">EFRAIN</td>
                    </tr>
                    <tr class="table-success sin_margen">
                      <th colspan="6" style="padding:0px;">11.Razón Social</th>
                    </tr>
                    <tr style="padding:0px;">
                      <td colspan="6" style="padding:0px;"> <input class="form-control" type="text" style="font-size:x-small;border:0" value="" placeholder="Razón social contenido"> </td>
                    </tr>
                  </tbody>

                </table>
              </div>

            </td>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <thead style="padding:0px;" >
              <tr style="padding:0px;">
                <th  colspan="6" style="padding:0px;" style="background-color:#c2e6cb">Datos del Cliente del Profesional en Compra y Venta de Divisas en Efectivo y Cheques de Viajero</th>
              </tr>
            </thead>
            <tbody>
              <tr style="padding:0px;">
                <td style="padding:0px;">
                  25. Tipo de Documento:
                </td>
                <td style="padding:0px;">
                  T.I. <input type="radio" name="tipo_doc" value=""> <br>
                  C.C. <input type="radio" name="tipo_doc" value="">
                </td>
                <td colspan="2" style="padding:0px;">
                  Tarjeta de extranjeria. <input type="radio" name="tipo_doc" value=""> <br>
                  Cedula extranjeria. <input type="radio" name="tipo_doc" value="">
                </td>
                <td style="padding:0px;">
                  NIT. <input type="radio" name="tipo_doc" value=""> <br>
                  Pasaporte <input type="radio" name="tipo_doc" value="">
                </td>
                <td style="padding:0px;">
                  Documento de extranjero <input type="radio" name="tipo_doc" value=""> <br>
                </td>
              </tr>
              <tr style="padding:0px;">
                <td style="padding:0px;">
                  26. N° Documento Identificación:
                </td>
                <td style="padding:0px;">
                  27. Dv
                </td>
                <td style="padding:0px;">
                  28. Primer Apellido
                </td>
                <td style="padding:0px;">
                  29. Segundo Apellido
                </td>
                <td style="padding:0px;">
                  30. Primer Nombre
                </td>
                <td style="padding:0px;">
                  31.Otros Nombres
                </td>
              </tr>
              <tr style="padding:0px;">
                <td id="cedula_cliente_536_rep" style="padding:0px;">01774</td>
                <td style="padding:0px;">
                  <input class="form-control" style="border:0;font-size:x-small" type="text" name="" value="" placeholder="dv">
                </td>
                <td id="primer_apellido_536_rep" style="padding:0px;">

                </td>
                <td id="segundo_appellido_536_rep" style="padding:0px;">

                </td>
                <td id="primer_nombre_536_rep" style="padding:0px;">

                </td>
                <td id="segundo_nombre_536_rep" style="padding:0px;">

                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6" style="padding:0px;">32. Razón Social</td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6"style="padding:0px;" > <input style="border:0;font-size:x-small" class="form-control" type="text" name="" value=""> </td>
              </tr>
              <tr style="padding:0px;">
                <td class="table-success sin_margen" colspan="6"> DATOS DE LA OPERACIÓN</td>
              </tr>
              <tr style="padding:0px;">
                <td style="padding:0px;">33. Moneda negociada</td>
                <td style="padding:0px;">34. Cód</td>
                <td style="padding:0px;">35. Monto de la moneda negociada</td>
                <td style="padding:0px;" colspan="2">36. valor en pesos ($)</td>
                <td style="padding:0px;">
                  36.clase de compra o venta
                </td>
              </tr>
              <tr style="padding:0px;">
                <td style="padding:0px;" id="moneda_negociada_536_rep"></td>
                <td style="padding:0px;"> <input class="form-control" style="border:0;font-size:x-small" type="text" name="" value="USD"> </td>
                <td id="monto_negociado_536_rep" style="padding:0px;"></td>
                <td colspan="2" id="valor_pesos_536_rep" style="padding:0px;"></td>
                <td style="padding:0px;">
                  Efectivo <input type="radio" name="clase" value="true" checked> Cheque Viajero<input type="radio" name="clase" value="">
                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6" style="padding:0px;">
                  <p style="text-align:center;font-size:xx-small">
                    Declaración de Cambio prescrita por la dirección de Impuestos y Aduanas Nacionales de conformidad con lo establecido en la circular Reglamentaria Externa DCIN-83 de Mayo 3 de 2006 del Banco de la República cuando la operación se realice entre dos(2) profesionales del cambio se elaborara una(1) sola declaración decambio, la declaración la debe dilinegiar el profesional que recibe y acepta la oferta del cliente
                  </p>
                  <p style="text-align:justify;font-size:xx-small">
                    Advertencia:Esta declaracion simplificada solo puede ser utilizada, presentada y exigida respecto de operaciones de compra o venta de divisas en efectivo o de cheques de viajero celebradas en zonas de frontera por montos inferiores a quinientos dólares de los Estados Unidos de America (USD 500,00) y superiores a docientos dólares de Estados Unidos de America (USD 200,00) o su equivalente en otras monedas. Operaciones iguales o superiores a quinientos dólares de los Estados Unidos de America (USD 500,00), deben ser declaradas en el formulario "Declaración de cambio por compra y venta de manera profesional de divisas y cheques de viajero" incluido en la circular reglamentaria Externa DCIN-83 de 2004 del Banco de la República y en sus posteriores modificaciones.
                  </p>
                  <p></p>
                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="4" style="padding:0px;">
                  <p style="text-align: lefth">Firma cliente</p>
                </td>
                <td colspan="2" style="padding:0px;">
                  997. Fecha Opercion<br>
                  <p id="fecha_hoy_536_rep"></p>
                </td>
              </tr>
            </tbody>

          </table>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <button type="button" onclick="imprimir_formulario_536_rep();"  name="button"> Imprimir</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_informe_cajeros_general" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">Informe Cajero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid" id="" style="font-size:large">
          <table class="table table-bordered table-hover">
            <thead class="thead-dark">
              <th>Objeto</th>
              <th>Categoria</th>
              <th>Precio Transaccion</th>
              <th>Cantidad</th>
              <th>Total</th>
            </thead>
            <tbody id="contenedor_informe_cajeros_general">

            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_informe_cajeros_seguros" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">Informe Seguros</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid" id="" style="font-size:large">
          <table class="table table-bordered table-hover">
            <thead class="thead-dark">
              <th>Fecha</th>
              <th>Entidad</th>       
              <th>Seguro</th>
              <th>Precio Base</th>
              <th>Precio Transaccion</th>
              <th>Cantidad</th>
              <th>Total</th>
            </thead>
            <tbody id="contenedor_informe_cajeros_seguros">

            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="modal" id="modal_contador_herramienta" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">Informe Cajero</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid" id="" style="font-size:large">
          <table class="table table-bordered table-hover">
            <thead class="thead-dark">
              <th>Denominacion</th>
              <th>Cantidad <button type="button" name="button" onclick="sumar_herramienta()">Sumar</button></th>
              <th>Sub-total</th>
            </thead>
            <tbody id="contenedor_contador_herramienta">

            </tbody>
          </table>
          <span id="total_contador_herramienta"></span>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>
</div>
