<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>static/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    FACTURACION
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->

  <link href="<?php echo base_url(); ?>static/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>static/css/now-ui-kit.css?v=1.3.0" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>static/css/style.css" rel="stylesheet" />
  <script src="<?php echo base_url(); ?>static/js/core/jquery.min.js"></script>
  <!-- CSS Just for demo purpose, don't include it in your project -->

</head>

<body class="landing-page sidebar-collapse">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400" style="font-size:medium">
    <div class="container">
      <div class="dropdown button-dropdown">
        <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
          <span class="button-bar"></span>
          <span class="button-bar"></span>
          <span class="button-bar"></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-header">Menu Principal</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/index">Caja</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Prestamos/cargar_vista_prestamos">Prestamos</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/inventario">Inventario</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/base">Base Diaria</a>
          <a class="dropdown-item" href="#">Contabilidad</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Reportes">Reportes</a>
          <div class="dropdown-divider"></div>
          <button class="dropdown-item" id="calculadora"><span class="badge badge-primary">Calculadora</span></button>
          <!-- <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">One more separated link</a> -->
        </div>
      </div>
      <div class="navbar-translate">
        <a class="navbar-brand" href="" rel="tooltip" title="Desplegar menu Principal" data-placement="bottom" target="_blank">
          Menu
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar top-bar"></span>
          <span class="navbar-toggler-bar middle-bar"></span>
          <span class="navbar-toggler-bar bottom-bar"></span>
        </button>
      </div>
      <div class="text-center">

      </div>


      <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="<?php echo base_url(); ?>static/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Divisas/inventario">INVENTARIO</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Divisas/index">CAJA</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="" target="_blank">
              <i class="fab fa-twitter"></i>
              <p class="d-lg-none d-xl-none">Twitter</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="" target="_blank">
              <i class="fab fa-facebook-square"></i>
              <p class="d-lg-none d-xl-none">Facebook</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="" target="_blank">
              <i class="fab fa-instagram"></i>
              <p class="d-lg-none d-xl-none">Instagram</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper">
    <div class="page-header page-header-large">
      <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>static/img/bg6.jpg');">
      </div>
      <div class="container">
        <div class="container" id="div_principal">
          <h1 class="title" id="titulo_principal">Continental-Divisas</h1>
          <div class="cols-sm-12 rounded-lg" id='contenedor_principal_divisas' style="font-size:large">

          </div>
          <!-- <div class="row justify-content-center">
            <div class="col-sm-4">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="width:200px" align="center">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item">
                    <img class="d-block" src="<?php echo base_url(); ?>static/images/estado.png" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">

                    </div>
                  </div>
                  <div class="carousel-item active">
                    <img class="d-block" src="<?php echo base_url(); ?>static/images/equidad.png" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">

                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block" src="<?php echo base_url(); ?>static/images/estado.png" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">

                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
              </div>
            </div>
          </div> -->

        </div>
      </div>
    </div>
    <!-- <div class="section section-about-us">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Who we are?</h2>
            <h5 class="description">According to the National Oceanic and Atmospheric Administration, Ted, Scambos, NSIDClead scentist, puts the potentially record low maximum sea ice extent tihs year down to low ice extent in the Pacific and a late drop in ice extent in the Barents Sea.</h5>
          </div>
        </div>
        <div class="separator separator-primary"></div>
        <div class="section-story-overview">
          <div class="row">
            <div class="col-md-6">
              <div class="image-container image-left" style="background-image: url('<?php echo base_url(); ?>static/img/login.jpg')">

                <p class="blockquote blockquote-primary">"Over the span of the satellite record, Arctic sea ice has been declining significantly, while sea ice in the Antarctichas increased very slightly"
                  <br>
                  <br>
                  <small>-NOAA</small>
                </p>
              </div>

              <div class="image-container" style="background-image: url('<?php echo base_url(); ?>static/img/bg3.jpg')"></div>
            </div>
            <div class="col-md-5">



              <div class="image-container image-right" style="background-image: url('<?php echo base_url(); ?>static/img/bg1.jpg')"></div>
              <h3>So what does the new record for the lowest level of winter ice actually mean</h3>
              <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
              </p>
              <p>
                For a start, it does not automatically follow that a record amount of ice will melt this summer. More important for determining the size of the annual thaw is the state of the weather as the midnight sun approaches and temperatures rise. But over the more than 30 years of satellite records, scientists have observed a clear pattern of decline, decade-by-decade.
              </p>
              <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <!-- <div class="section section-team text-center">
      <div class="container">
        <h2 class="title">Here is our team</h2>
        <div class="team">
          <div class="row">
            <div class="col-md-4">
              <div class="team-player">
                <img src="<?php echo base_url(); ?>static/img/avatar.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                <h4 class="title">Romina Hadid</h4>
                <p class="category text-primary">Model</p>
                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                  <a href="#">links</a> for people to be able to follow them outside the site.</p>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-twitter"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-instagram"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-facebook-square"></i></a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="team-player">
                <img src="<?php echo base_url(); ?>static/img/ryan.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                <h4 class="title">Ryan Tompson</h4>
                <p class="category text-primary">Designer</p>
                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                  <a href="#">links</a> for people to be able to follow them outside the site.</p>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-twitter"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-linkedin"></i></a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="team-player">
                <img src="<?php echo base_url(); ?>static/img/eva.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                <h4 class="title">Eva Jenner</h4>
                <p class="category text-primary">Fashion</p>
                <p class="description">You can write here details about one of your team members. You can give more details about what they do. Feel free to add some
                  <a href="#">links</a> for people to be able to follow them outside the site.</p>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-google-plus"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-youtube"></i></a>
                <a href="#pablo" class="btn btn-primary btn-icon btn-round"><i class="fab fa-twitter"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- <div class="section section-contact-us text-center">
      <div class="container">
        <h2 class="title">Want to work with us?</h2>
        <p class="description">Your project is very important to us.</p>
        <div class="row">
          <div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">
            <div class="input-group input-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="now-ui-icons users_circle-08"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="First Name...">
            </div>
            <div class="input-group input-lg">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="now-ui-icons ui-1_email-85"></i>
                </span>
              </div>
              <input type="text" class="form-control" placeholder="Email...">
            </div>
            <div class="textarea-container">
              <textarea class="form-control" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>
            </div>
            <div class="send-button">
              <a href="#pablo" class="btn btn-primary btn-round btn-block btn-lg">Send Message</a>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <footer class="footer footer-default">
      <div class=" container ">
        <nav>
          <ul>
            <li>
              <a href="#">
                <!-- <img src="<?php echo base_url(); ?>static/images/efecty-logo.png" alt=""> -->
              </a>
            </li>
            <li>
              <a href="#">
                Sobre la Empresa
              </a>
            </li>
            <li>
              <a href="#">
                Informacion CONTINENTAL SOFTWARE
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright" id="copyright">
          &copy;
          <script>
            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
          </script><!--, Designed by
          <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
          <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.-->
        </div>
      </div>
    </footer>
  </div>
  <div class="modal" id="modal_calculadora" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Calculadora...</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="row">
              <div class="col-sm-8">
                <input class="col-sm-12" type="numer" id="input_1_calc" value="">
              </div>
              <!-- <div class="col-sm-1">
                <span class="badge badge-info" id="operador">op</span>
              </div>
              <div class="col-sm-3">
                <input class="col-sm-12" type="numer" id="input_2_calc" value="">
              </div> -->
              <div class="col-sm-1">
                =
              </div>
              <div class="col-sm-3">
                <h5><span class="badge badge-success" id="result_calc"></span></h5>
              </div>

            </div>
            <div class="row">

              <div class="col-sm-4" align="center">
                <span class="badge badge-info"> Teclado</span>
                <div class="row">
                  <div class="col-sm-4">
                  <button type="button" name="numero" data-numero='1' onclick="tecla_press(this);"><span class="badge badge-primary">1</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='2' onclick="tecla_press(this);"><span class="badge badge-primary">2</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='3' onclick="tecla_press(this);"><span class="badge badge-primary">3</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='4' onclick="tecla_press(this);"><span class="badge badge-primary">4</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='5' onclick="tecla_press(this);"><span class="badge badge-primary">5</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='6' onclick="tecla_press(this);"><span class="badge badge-primary">6</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='7' onclick="tecla_press(this);"><span class="badge badge-primary">7</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='8' onclick="tecla_press(this);"><span class="badge badge-primary">8</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='9' onclick="tecla_press(this);"><span class="badge badge-primary">9</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='0' onclick="tecla_press(this);"><span class="badge badge-primary">0</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='c' onclick="tecla_press(this);"><span class="badge badge-primary">C</span></button>
                  </div>
                  <div class="col-sm-4">

                  </div>
                </div>
              </div>
              <!-- <div class="col-sm-4">
                <span class="badge badge-info">opciones basicas</span>
                <button style="width:60px" title="SUMA" type="button" name="numero" data-numero='+' onclick="tecla_press(this);"><span class="badge badge-primary">+</span></button>
                <button style="width:60px" title="RESTA" type="button" name="numero" data-numero='-' onclick="tecla_press(this);"><span class="badge badge-primary">-</span></button>
                <button style="width:60px" title="MULTIPLICACIÓN" type="button" name="numero" data-numero='*' onclick="tecla_press(this);"><span class="badge badge-primary">*</span></button>
                <button style="width:60px" title="DIVISIÓN" type="button" name="numero" data-numero='/' onclick="tecla_press(this);"><span class="badge badge-primary">/</span></button>
                <button style="width:120px" title="IGUAL" type="button" name="numero" data-numero='=' onclick="tecla_press(this);"><span class="badge badge-success">=</span></button>
              </div> -->
              <div class="col-sm-4">
                <span class="badge badge-info"> opciones de divisas</span>
                <select class="form-control form-control-sm" id="sel_origen_modal" onchange="cargar_destino();">
                  <option value="">origen</option>
                </select>
                <select class="form-control form-control-sm" id="sel_destino_modal" onchange="opciones_venta_compra();">
                  <option value="">destino</option>
                </select>
                <select class="form-control form-control-sm" id="modal_compra" onchange="calcular_compra();">
                  <option value="">compra</option>
                </select>
                <select class="form-control form-control-sm" id="modal_venta" onchange="calcular_venta();">
                  <option value="">venta</option>
                </select>

              </div>
            </div>



        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>static/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?php echo base_url(); ?>static/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?php echo base_url(); ?>static/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="<?php echo base_url(); ?>static/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url(); ?>static/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/calculadora.js" type="text/javascript"></script>
</body>

</html>
