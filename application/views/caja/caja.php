<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<head>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <script src="<?php echo base_url(); ?>static/js/caja.js"></script>
  <script src="<?php echo base_url(); ?>static/js/printThis.js"></script>
</head>
<!-- <input id="cedula_cajero_login" type="hidden" name="" value="<?php echo $this->session->userdata('cedula');?>">
<input id="nombre_cajero_login" type="hidden" name="" value="<?php echo $this->session->userdata('nombre')." ".$this->session->userdata('apellido');?>"> -->
<div class="container" align="center" style="padding:1em;display:none;font-size:large" id="div_caja">
  <div class="row">
    <div class="col-sm-4" style="display:none" id="contenedor_botones">
    </div>
    <div class="col-sm-8">
      <div class="row">
        <!-- <label><span class="badge badge-info">Doc. Identidad Cliente</span></label> -->
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-user-circle"></i></span>
          </div>
          <input type="number" class="form-control col-sm-4" placeholder="Cedula del cliente" onkeypress="enter_press(event);" id="input_cedula" style="background:white;">
        </div>

          <label>Cajero: <span id="cedula_cajero_login"><?php echo $this->session->userdata('cedula');?></span></label>
          <label>-<span id="nombre_cajero_login"><?php echo $this->session->userdata('nombre').' '.$this->session->userdata('apellido');?></span></label>

      </div>
      <div class="row" id="div_divisas" style="display:none">
        <div class="row">
          <select class="form-control form-control-sm col-sm-4" name="" id="sel_principal" onchange="cargar_segunda_categoria();" style="background:white;">
            <option value="" >Categoria Principal</option>
            <option value="1" >DIVISAS</option>
            <!--<option value="2" >SEGUROS</option>-->
            <option value="3" >GENERAL</option>
          </select>
          <select class="form-control form-control-sm col-sm-4" id="sel_categoria_origen" style="background:white;">
            <option value="" > opcion uno</option>
          </select>
          <!-- <label> <span class="badge badge-info">Cambiar a:</span></label> -->
          <select class="form-control form-control-sm col-sm-4" id="sel_categoria_destino" style="background:white;">
            <option value="" > opcion dos</option>
          </select>
        </div>
        <div class="row">
          <label class="col-sm-4"> <span class="badge badge-info">Cantidad</span></label>
          <input class="form-control col-sm-3"  name="input_5" id="input_cantidad" onkeypress="mostrar_botones_necesarios(event);" onkeyup="format(this)" onchange="format(this)" style="background:white;">
        </div>

      </div>



    </div>
  </div>

  <div class="row" id="cliente_nombre" align="center">

  </div>
   <div class="container table-responsive-sm rounded-lg" style="height:300px">
     <table class="table table-striped rounded-lg" style="background:white">
     <tbody id="div_complemento_caja">

     </tbody>
     </table>
     <!--
      <div class="col">
        <label <span class="badge badge-info">Documento Identificaion</span></label>
        <input type="text" name="input_1" id="input_1">
      </div>
      <div class="col">
        <label <span class="badge badge-info">Nombre</span></label>
        <input type="text" name="input_2" id="input_2">
      </div>
      <div class="col">
        <label <span class="badge badge-info">Apellidos</span></label>
        <input type="text" name="input_3" id="input_3">
      </div>
      <div class="col">
        <label <span class="badge badge-info">Nacionalidad</span></label>
        <input  name="input_4" id="input_4">
      </div>
    -->

  </div>


  <input type="hidden" value="<?php echo base_url(); ?>" id="url">
</div>
<div class="modal" id="modal_principal2">
  <div class="modal-dialog" >
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">

          <label class="modal-title" id="modal_principal_titulo"></label>


        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->

      <div class="modal-body">
          <!-- <div id="btns_head_modal"></div>
          <hr> -->
        <div class="fluid-container" id="modal_body_principal1" style="overflow-y: scroll;" align="center">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <div id="btns_modal_foter"></div>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
      </div>

    </div>
  </div>
</div>
<div class="modal" id="modal_principal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_body_principal_2">
        <!-- <img src="<?php echo base_url(); ?>static/images/logo_carlos_min.jpg" alt=""> -->
        <img src="<?php echo base_url(); ?>static/images/logo_carlos_min.png" alt="">
        <div class="fluid-container" id="modal_body_principal" align="left" style="font-size:medium">
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
          </div>
          <div class="col-sm-8" id="modal_footer_principal">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- fomulario 18 -->
<div class="modal" id="modal_formulario_18" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">formulario 18</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container" id="modal_body_formulario_18" align="center" style="font-size:small">
          <table>
            <tr>
              <td>CONTINENTAL CAMBIOS <br>
              Profesional en Compra y Venta de Divisas y Cheque viajeros <br>
              Cra 2 #2-81 barrio la laguna Terminal de Ípiales local 47 2do piso
              </td>
              <td>
                <p>formulario N° 18</p>
                <p>Factura N° <input id="numero_factura_formulario_18" value="2"></p>
              </td>
            </tr>
          </table>
          <table>
            <tr>
              <td><img src="<?php echo base_url(); ?>static/images/banco_reoublica _colombia.jpeg" alt=""></td>
              <td>
                <b>Declaración de Compra y Venta de Divisas y Cheque viajeros<br>Formulario N°18<br>
                </b>Circular Regamentaria Externa DCIN-83 DE OCTUBRE DE 2017<br>Original Y Copia
              </td>
            </tr>
          </table>
          <div class="container-fluid">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>1.Ciudad</td>
                  <td>2.Fecha</td>
                </tr>
                <tr>
                  <td>ÍPIALES</td>
                  <td id="fecha_hoy_18">11/12/34</td>
                </tr>
              </tbody>
            </table>
            <b>I. IDENTIFICACIÓN DEL PROFESIONAL DE CAMBIO</b>
            <table class="table table-bordered">
              <tr>
                <td>1. Nombre o Razón Social</td>
                <td>2.Tipo</td>
                <td>3.Número Identificación</td>
              </tr>
              <tr>
                <td>CARLOS EFRAIN REVELO ERIRA</td>
                <td>CC</td>
                <td>1.085.901.197</td>
              </tr>
              <tr>
                <td>4.Número de Factura</td>
                <td colspan="2">5. Matrícula del Establecimiento de Comercio</td>
              </tr>
              <tr>
                <td> <input type="text" id="numero_de_factura_18" value="" placeholder="numero factura"> </td>
                <td colspan="2">41074</td>
              </tr>
            </table>
            <b>II. IDENTIFICACIÓN DEL CLIENTE(datos del residente que compra o vende divisas o cheques de viajeros)</b>
            <table class="table table-bordered">
              <tr>
                <td>1.Nombre o Razón social</td>
                <td>2.Tipo</td>
                <td>3.Número de Identificación</td>
              </tr>
              <tr>
                <td id="nombre_cliente_18">Angel Jose Romero Romero</td>
                <td id="tipo_cliente_18"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="C.C"></td>
                <td id="identificacion_cliente_18">123456789</td>
              </tr>
              <tr>
                <td colspan="2">4.Dirección</td>
                <td>5.Ciudad</td>
              </tr>
              <tr>
                <td colspan="2" id="dirreccion_cliente_18"> <input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="CRA 4 13-45 BARRIO BOLIVAR"></td>
                <td id="ciudad_cliente"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="Cali"></td>
              </tr>
              <tr>
                <td>6. Teléfono</td>
                <td colspan="2">7. Actividad Economica</td>
              </tr>
              <tr>
                <td><input id="telefono_cliente_18" type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="307865423"></td>
                <td colspan="2"><input type="text" class="form-control" style="borded-style:none;border:0" value="" placeholder="Comerciante"></td>
              </tr>
            </table>
            <b>III. IDENTIFICACIÓN DEL DECLARANTE(datos de la persona natural que siscribe la declaracion, en nombre propio o representacion del cliente)</b>
            <table class="table table-bordered">
              <tr>
                <td>1.Nombre o Razón Social</td>
                <td>2.Tipo</td>
                <td>3.Número Identificación</td>
              </tr>
              <tr>
                <td>Carlos Revelo</td>
                <td>CC</td>
                <td>1.085.901.197</td>
              </tr>
              <tr>
                <td>4.Dirección</td>
                <td>5.Ciudad</td>
                <td>6.Teléfono</td>
              </tr>
              <tr>
                <td>Terminal local 47 2do piso</td>
                <td>Ipiales</td>
                <td>7731277</td>
              </tr>
            </table>
            <b>IV. DESCRIPCIÓN DE LA OPERACIÓN</b><br>
            1.Concepto(seleccione la opcion):<br>
            <input type="radio" name="concepto" value="">Compra Divisas   <input type="radio" name="concepto" value="">Venta Divisas
            <table class="table table-bordered">
              <tr>
                <td>2.Nombre de la moneda negociada</td>
                <td>3.Monto de la moneda negociada</td>
              </tr>
              <tr>
                <td id="moneda_negociada_18">DOLAR</td>
                <td id="monto_negociado_18">2745000</td>
              </tr>
              <tr>
                <td>4.Tasa de cambio</td>
              </tr>
              <tr>
                <td id="tasa_cambio_18">3050</td>
              </tr>
            </table>
            valor en pesos de la operacion
            <table class="table table-bordered">
              <tr>
                <td>5.efectivo</td>
                <td> <input id="input_efectivo_18" type="text" style="borded-style:none;border:0" value=""></td>
              </tr>
              <tr>
                <td>6.cheque de viajero</td>
                <td> <input id="input_cheque_18" type="text" style="borded-style:none;border:0" value="COP$"></td>
              </tr>
              <tr>
                <td>7.pago diferente a efectivo</td>
                <td> <input id="input_diferente_18" type="text" style="borded-style:none;border:0" value="COP$"></td>
              </tr>
              <tr>
                <td>8.TOTAL</td>
                <td id="total_18">900000</td>
              </tr>
            </table>
            <p align="justify" style="font-size:small">
            PARA LOS FINES PREVISTOS EN EL ARTICULO 83 DE LA CONSTITUCIÓN POLITICA DE COLOMBIA,DECLARO BAJO LA GRAVEDAD DE JURAMENTO QUE LOS CONCEPTOS, CANTIDADES Y DEMAS DATOS CONSIGNADOS EN EL PRESENTE FORMULARIO SON CORRECTOS Y FIEL EXPRESIÓN DE LA VERDAD CUANDO LA DECLARACION SE REALICE ENTRE DOS (2) PROFESIONALES DEL CAMBIO SE ELABORA UNA (1) SOLA DECLARACIÓN DE CAMBIO. LA DECLARACIÓN LA DEBE DILIGENCIAR EL BENEFICIARIO O CLIENTE EN LA PAPELERIA SUMINISTRADA POR EL PROFESIONAL QUE RECIBE Y ACEPTA LA OFERTA DEL BENEFICIARIO
            </p>
            <div class="row">
              <div class="col-sm-6">
                <hr style="background:black">
                FIRMA DEL DECLARANTE
              </div>
              <div class="col-sm-6">
                HUELLA INDICE DERECHO
                <div style="height:7em;width:7em;border:solid black">

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <button type="button" onclick="imprimir_formulario_18();"  name="button"> Imprimir</button>
          </div>
          <div class="col-sm-8" id="modal_footer_principal">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_formulario_2" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">formulario 536</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container" id="modal_body_formulario_2" align="center" style="font-size:x-small">
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <tr>
              <td rowspan="2" style="padding:0px;">
                  <img src="<?php echo base_url(); ?>static/images/dian.jpeg" alt="" height="30px">
              </td>
              <td rowspan="2" style="padding:0px;">
                Declaracion de Cambio simplificada por Compra y Venta<br>Profesional de Divisas en Efectivo y Cheques de Viajero<br> en Zonas de Frontera
              </td>
              <td rowspan="1" style="padding:0px;">
                <img src="<?php echo base_url(); ?>static/images/muisca.jpeg" alt="">
              </td>
              <td style="padding:0px;">
                536
              </td>
            </tr>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <tr>
              <td style="padding:0px;">1.Año</td>
              <td style="padding:0px;" id="año_536">2019</td>
              <td style="padding:0px;">2.Concepto: Compra <input type="radio" name="concepto" value="true"> Venta <input type="radio" name="concepto" value=""></td>
              <td style="padding:0px;">24. Número Declaracion</td>
              <td style="padding:0px;"> <input type="text" id="numero_factura_536" value="" placeholder="numero factura"> </td>
            </tr>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <td rowspan="4" style="padding:0px;">
              Datos<br>Prfesional<br>Cambista
            </td>
            <td rowspan="2" style="padding:0px;">
              <div class="container">
                <table class="table table-bordered">
                  <thead>
                    <tr class="table-success">
                      <th style="padding:0px;">5.Número de Identificación Tributaria(NIT)</th>
                      <th style="padding:0px;">6.DV</th>
                      <th style="padding:0px;">7.Primer Apellido</th>
                      <th style="padding:0px;">8.Segundo Apellido</th>
                      <th style="padding:0px;">9.Primer Nombre</th>
                      <th style="padding:0px;">10.Otros Nombres</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="padding:0px;">1|0|8|5|9|0|1|1|9|7|-|</td>
                      <td style="padding:0px;">|9|</td>
                      <td style="padding:0px;">REVELO</td>
                      <td style="padding:0px;">ERIRA</td>
                      <td style="padding:0px;">CARLOS</td>
                      <td style="padding:0px;">EFRAIN</td>
                    </tr>
                    <tr class="table-success" style="padding:0px;">
                      <th style="padding:0px;" colspan="6">11.Razón Social</th>
                    </tr>
                    <tr>
                      <td style="padding:0px;" colspan="6"> <input class="form-control" type="text" style="font-size:x-small;border:0" value="" placeholder="Razón social contenido"> </td>
                    </tr>
                  </tbody>

                </table>
              </div>

            </td>
          </table>
          <table class="table table-bordered" style="text-align:center;margin-bottom:0px">
            <thead >
              <tr>
                <th  colspan="6" style="background-color:#c2e6cb;padding:0px;">Datos del Cliente del Profesional en Compra y Venta de Divisas en Efectivo y Cheques de Viajero</th>
              </tr>
            </thead>
            <tbody>
              <tr style="padding:0px;">
                <td style="padding:0px;">
                  25. Tipo de Documento:
                </td>
                <td style="padding:0px;">
                  T.I. <input type="radio" name="tipo_doc" value=""> <br>
                  C.C. <input type="radio" name="tipo_doc" value="">
                </td>
                <td style="padding:0px;" colspan="2" >
                  Tarjeta de extranjeria. <input type="radio" name="tipo_doc" value=""> <br>
                  Cédula extranjeria. <input type="radio" name="tipo_doc" value="">
                </td>
                <td style="padding:0px;">
                  NIT. <input type="radio" name="tipo_doc" value=""> <br>
                  Pasaporte <input type="radio" name="tipo_doc" value="">
                </td>
                <td style="padding:0px;">
                  Documento de extranjero <input type="radio" name="tipo_doc" value=""> <br>
                </td>
              </tr>
              <tr style="padding:0px;">
                <td style="padding:0px;">
                  26. N° Documento Identificación:
                </td>
                <td style="padding:0px;">
                  27. Dv
                </td>
                <td style="padding:0px;">
                  28. Primer Apellido
                </td>
                <td style="padding:0px;">
                  29. Segundo Apellido
                </td>
                <td style="padding:0px;">
                  30. Primer Nombre
                </td>
                <td style="padding:0px;">
                  31.Otros Nombres
                </td>
              </tr>
              <tr style="padding:0px;">
                <td id="cedula_cliente_536" style="padding:0px;">01774</td>
                <td style="padding:0px;">
                  <input class="form-control" style="border:0;font-size:x-small" type="text" name="" value="" placeholder="dv">
                </td>
                <td id="primer_apellido_536" style="padding:0px;">

                </td>
                <td id="segundo_appellido_536" style="padding:0px;">

                </td>
                <td id="primer_nombre_536" style="padding:0px;">

                </td>
                <td id="segundo_nombre_536" style="padding:0px;">

                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6" style="padding:0px;">32. Razón Social</td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6" style="padding:0px;"> <input style="border:0;font-size:x-small" class="form-control" type="text" name="" value=""> </td>
              </tr>
              <tr style="padding:0px;">
                <td class="table-success" style="padding:0px;" colspan="6"> DATOS DE LA OPERACIÓN</td>
              </tr>
              <tr style="padding:0px;">
                <td style="padding:0px;">33. Moneda negociada</td>
                <td style="padding:0px;">34. Cód</td>
                <td style="padding:0px;">35. Monto de la moneda negociada</td>
                <td colspan="2" style="padding:0px;">36. valor en pesos ($)</td>
                <td style="padding:0px;">
                  36.clase de compra o venta
                </td>
              </tr>
              <tr>
                <td style="padding:0px;" id="moneda_negociada_536"></td>
                <td style="padding:0px;"> <input class="form-control" style="border:0;font-size:x-small" type="text" name="" value="USD"> </td>
                <td id="monto_negociado_536" style="padding:0px;"></td>
                <td style="padding:0px;" colspan="2" id="valor_pesos_536"></td>
                <td style="padding:0px;">
                  Efectivo <input type="radio" name="clase" value="true" checked> Cheque Viajero<input type="radio" name="clase" value="">
                </td>
              </tr>
              <tr style="padding:0px;">
                <td colspan="6" style="padding:0px;">
                  <p style="text-align:center;font-size:xx-small">
                    Declaración de Cambio prescrita por la dirección de Impuestos y Aduanas Nacionales de conformidad con lo establecido en la circular Reglamentaria Externa DCIN-83 de Mayo 3 de 2006 del Banco de la República cuando la operación se realice entre dos(2) profesionales del cambio se elaborara una(1) sola declaración decambio, la declaración la debe dilinegiar el profesional que recibe y acepta la oferta del cliente
                  </p>
                  <p style="text-align:justify;font-size:xx-small">
                    Advertencia:Esta declaracion simplificada solo puede ser utilizada, presentada y exigida respecto de operaciones de compra o venta de divisas en efectivo o de cheques de viajero celebradas en zonas de frontera por montos inferiores a quinientos dólares de los Estados Unidos de America (USD 500,00) y superiores a docientos dólares de Estados Unidos de America (USD 200,00) o su equivalente en otras monedas. Operaciones iguales o superiores a quinientos dólares de los Estados Unidos de America (USD 500,00), deben ser declaradas en el formulario "Declaración de cambio por compra y venta de manera profesional de divisas y cheques de viajero" incluido en la circular reglamentaria Externa DCIN-83 de 2004 del Banco de la República y en sus posteriores modificaciones.
                  </p>
                  <p></p>

                </td>
              </tr>
              <tr>
                <td style="padding:0px;" colspan="4">
                  <p style="text-align: lefth">Firma cliente</p>
                </td>
                <td colspan="2" style="padding:0px;">
                  997. Fecha Opercion<br>
                  <p id="fecha_hoy_536"></p>
                </td>
              </tr>
            </tbody>

          </table>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <button type="button" onclick="imprimir_formulario_536();"  name="button"> Imprimir</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
