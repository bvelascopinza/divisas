<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<head>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <script src="<?php echo base_url(); ?>static/js/prestamos.js"></script>
</head>
<div class="container" align="center" style="padding:1em;display:none" id="div_prestamos">
  <!-- <button type="button" name="button">
    <i class="fas fa-trash-alt"></i>
    <label>Nuevo prestamo</label>
  </button>
  <button type="button" name="button">
    <i class="fas fa-trash-alt"></i>
    <label>Reportes prestamo</label>
  </button>
  <div class="container"> -->
    <div class="container">
      <div class="row">
        <div class="input-group col-sm-3">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-user-circle"></i></span>
          </div>
          <input type="text" class="form-control" placeholder="Solicitante"  id="input_nombre" style="background:white;font-size:1em">
        </div>
        <div class="input-group col-sm-3">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-user-circle"></i></span>
          </div>
          <input type="number" class="form-control" placeholder="C.C xxxxxxx"  id="input_cedula" style="background:white;font-size:1em">
        </div>
        <div class="input-group col-sm-3">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-user-circle"></i></span>
          </div>
          <input type="number" class="form-control" placeholder="cant:100000"  id="input_cantidad" style="background:white;font-size:1em">
        </div>
        <div class="col-sm-3">
          <select class="form-control" id="sel_categoria_origen" style="background:white;" title="Seleccion la divisa de la transaccion" data-size="20">
              <option value=''>Divisa</option>
          </select>
          <button class="btn btn-success btn-round" type="button" onclick="insertar_prestamo();">
            <i class="fas fa-plus"></i> Agregar Prestamo
          </button>
        </div>

      </div>
      <div class="cols-sm-12 rounded-lg" id='contenedor_principal_reportes' style="background:white;overflow-y:auto;height:400px">
        <table class="table table-hover">
          <thead class="thead-dark" id="thead_tabla">
            <tr>
              <th>Fecha</th>
              <th>Nombre</th>
              <th>Cedula</th>
              <th>Estado</th>
              <th>Cantidad</th>
              <th>Abono</th>
              <th>Acciones</th>
            </tr>
          </thead>
            <tbody id="body_tabla">

            </tbody>

        </table>
      </div>
      <label style="font-size:large">TOTAL:<span class="badge badge-success" id="total_prestamos" style="font-size:large"></span></label>
    </div>
  </div>
</div>
