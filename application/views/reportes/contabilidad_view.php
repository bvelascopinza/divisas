<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>static/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    FACTURACION
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->

  <link href="<?php echo base_url(); ?>static/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>static/css/now-ui-kit.css?v=1.3.0" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>static/css/style.css" rel="stylesheet" />
  <script src="<?php echo base_url(); ?>static/js/core/jquery.min.js"></script>
  <!-- CSS Just for demo purpose, don't include it in your project -->

</head>

<body class="landing-page sidebar-collapse">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
    <div class="container">
      <div class="dropdown button-dropdown">
        <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
          <span class="button-bar"></span>
          <span class="button-bar"></span>
          <span class="button-bar"></span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-header">Menu Principal</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/index">Caja</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Prestamos/cargar_vista_prestamos">Pendientes</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/inventario">Inventario</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Divisas/base">Base Diaria</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Transacciones/contabilidad">Contabilidad</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>Reportes">Reportes</a>
          <a class="dropdown-item" onclick="cajero_modal();">Agregar Cajero</a>
          <div class="dropdown-divider"></div>
          <button class="dropdown-item" id="calculadora"><span class="badge badge-primary">Calculadora</span></button>
          <!-- <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">One more separated link</a> -->
        </div>
      </div>
      <div class="navbar-translate">
        <a class="navbar-brand" href="" rel="tooltip" title="Desplegar menu Principal" data-placement="bottom" target="_blank">
          Menu
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar top-bar"></span>
          <span class="navbar-toggler-bar middle-bar"></span>
          <span class="navbar-toggler-bar bottom-bar"></span>
        </button>
      </div>
      <div class="text-center">

      </div>


      <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="<?php echo base_url(); ?>static/img/blurred-image-1.jpg">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Divisas/inventario">INVENTARIO</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>Divisas/index">CAJA</a>
          </li>

               <div class="dropdown button-dropdown">
              <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                  <img src="<?php echo base_url();?>assets/img/usuario.png" class="rounded-circle img-raised" alt="">
              </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-header">
              Menu Usuario
            </a>
            <a class="dropdown-item">
              <span id="">
              <?php
               echo $this->session->userdata('nombre')." ".$this->session->userdata('apellido');
               ?>
            </span>
            <span id="">
            <?php
             echo $this->session->userdata('cedula');
             ?>
          </span>
            </a>

            <div class="dropdown-divider"></div>
            <button class="dropdown-item" id="calculadora"><span class="badge badge-primary"><a href="<?php echo base_url ();?>login/cerrar">Salir</a></span></button>
            <!-- <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">One more separated link</a> -->
          </div>
        </div>

        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper">
    <div class="page-header page-header-large">
      <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url(); ?>static/img/bg11.jpg');">
      </div>
      <div class="container">
        <div class="container" id="div_principal" style="font-size:large">
          <h1 class="title" id="titulo_principal">CONTINENTAL Contabilidad</h1>
          <div class="container">
            <div class="row">
              <span class="badge badge-success">Fecha inicio:</span>
              <input id="fecha_inicio" type="date" class="form-control col-sm-2" value="" style="background:white">
              <span class="badge badge-success">Fecha fin:</span>
              <input id="fecha_fin" type="date" class="form-control col-sm-2" value="" style="background:white">
              <button class="btn btn-success" type="button" id="contabilidad_button" name="button" onclick="report_cont();">Consultar</button>
              <button type="button" class="btn btn-success" data-tipo='DIVISAS' id="" onclick="seleccion_contabilidad(this)"name="button">Divisas</button>
              <button type="button" class="btn btn-success" data-tipo='GENERAL' onclick="seleccion_contabilidad(this)" name="button">General</button>
              <button type="button" class="btn btn-success" data-tipo='SEGUROS' onclick="seleccion_contabilidad(this)" name="button">Seguros</button>

            </div>

          </div>
          <div class="cols-sm-12 rounded-lg" id='contenedor_principal_contabilidad' style="background:white;overflow-y:auto;height:300px">
            <table class="table table-hover">
              <thead class="thead-dark" id="thead_tabla">
                <tr>
                  <th>FECHA</th>
                  <th>HORA</th>
                  <th>CATEGORIA</th>
                  <th>DEBITOS</th>
                  <th>CREDITOS</th>
                </tr>
              </thead>
                <tbody id="body_tabla">

                </tbody>

            </table>
          </div>
          <div class="container">
            <span>Sumas Iguales</span>
            <span id="sumas_iguales"></span>
            <span>Ganancia</span>
            <span class="badge badge-info" style="font-size:medium" id="ganancia"></span>
          </div>

        </div>
      </div>
    </div>

    <footer class="footer footer-default">
      <div class=" container ">
        <nav>
          <ul>
            <li>
              <a href="#">
                <!-- <img src="<?php echo base_url(); ?>static/images/efecty-logo.png" alt=""> -->
              </a>
            </li>
            <li>
              <a href="#">
                Sobre la Empresa
              </a>
            </li>
            <li>
              <a href="#">
                Informacion CONTINENTAL SOFTWARE
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright" id="copyright">
          &copy;
          <script>
            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
          </script><!--, Designed by
          <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
          <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.-->
        </div>
      </div>
    </footer>
  </div>
  <div class="modal" id="modal_calculadora" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_title">Calculadora...</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="row">
              <div class="col-sm-8">
                <input class="col-sm-12" type="numer" id="input_1_calc" value="">
              </div>
              <!-- <div class="col-sm-1">
                <span class="badge badge-info" id="operador">op</span>
              </div>
              <div class="col-sm-3">
                <input class="col-sm-12" type="numer" id="input_2_calc" value="">
              </div> -->
              <div class="col-sm-1">
                =
              </div>
              <div class="col-sm-3">
                <h5><span class="badge badge-success" id="result_calc"></span></h5>
              </div>

            </div>
            <div class="row">

              <div class="col-sm-4" align="center">
                <span class="badge badge-info"> Teclado</span>
                <div class="row">
                  <div class="col-sm-4">
                  <button type="button" name="numero" data-numero='1' onclick="tecla_press(this);"><span class="badge badge-primary">1</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='2' onclick="tecla_press(this);"><span class="badge badge-primary">2</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='3' onclick="tecla_press(this);"><span class="badge badge-primary">3</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='4' onclick="tecla_press(this);"><span class="badge badge-primary">4</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='5' onclick="tecla_press(this);"><span class="badge badge-primary">5</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='6' onclick="tecla_press(this);"><span class="badge badge-primary">6</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='7' onclick="tecla_press(this);"><span class="badge badge-primary">7</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='8' onclick="tecla_press(this);"><span class="badge badge-primary">8</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='9' onclick="tecla_press(this);"><span class="badge badge-primary">9</span></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='0' onclick="tecla_press(this);"><span class="badge badge-primary">0</span></button>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" name="numero" data-numero='c' onclick="tecla_press(this);"><span class="badge badge-primary">C</span></button>
                  </div>
                  <div class="col-sm-4">

                  </div>
                </div>
              </div>
              <!-- <div class="col-sm-4">
                <span class="badge badge-info">opciones basicas</span>
                <button style="width:60px" title="SUMA" type="button" name="numero" data-numero='+' onclick="tecla_press(this);"><span class="badge badge-primary">+</span></button>
                <button style="width:60px" title="RESTA" type="button" name="numero" data-numero='-' onclick="tecla_press(this);"><span class="badge badge-primary">-</span></button>
                <button style="width:60px" title="MULTIPLICACIÓN" type="button" name="numero" data-numero='*' onclick="tecla_press(this);"><span class="badge badge-primary">*</span></button>
                <button style="width:60px" title="DIVISIÓN" type="button" name="numero" data-numero='/' onclick="tecla_press(this);"><span class="badge badge-primary">/</span></button>
                <button style="width:120px" title="IGUAL" type="button" name="numero" data-numero='=' onclick="tecla_press(this);"><span class="badge badge-success">=</span></button>
              </div> -->
              <div class="col-sm-4">
                <span class="badge badge-info"> opciones de divisas</span>
                <select class="form-control form-control-sm" id="sel_origen_modal" onchange="cargar_destino();">
                  <option value="">origen</option>
                </select>
                <select class="form-control form-control-sm" id="sel_destino_modal" onchange="opciones_venta_compra();">
                  <option value="">destino</option>
                </select>
                <select class="form-control form-control-sm" id="modal_compra" onchange="calcular_compra();">
                  <option value="">compra</option>
                </select>
                <select class="form-control form-control-sm" id="modal_venta" onchange="calcular_venta();">
                  <option value="">venta</option>
                </select>

              </div>
            </div>



        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>static/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?php echo base_url(); ?>static/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?php echo base_url(); ?>static/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="<?php echo base_url(); ?>static/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url(); ?>static/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/calculadora.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>static/js/contabilidad.js" type="text/javascript"></script>
</body>

</html>
