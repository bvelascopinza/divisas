<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<head>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <script src="<?php echo base_url(); ?>static/js/base.js"></script>
  <script src="<?php echo base_url(); ?>static/js/printThis.js"></script>
</head>
<title>
  Base
</title>
<div class="container"  id="div_base" style="display:none">
  <div class="row">
    <select class="form-control form-control-sm col-sm-4" name="" id="sel_divisa_1" style="background:white" onchange="objetos_divisa();">

    </select>
    <select class="form-control form-control-sm col-sm-4" name="" id="sel_cajero" style="background:white">

    </select>
    <button class="btn btn-info" type="button" name="button" onclick="ver_bases_diarias();">Ver</button>
    <button class="btn btn-info" type="button" name="button" onclick="ingresos_egresos();">Ingresos Egresos</button>
    <button class="btn btn-info" type="button" name="button" onclick="ver_solicitud();">
      <label for="">Solicitud</label>
      <span class="badge badge-success" id="span_solicitud"></span>
    </button>
  </div>
  <br>
  <div class="" style="height:300px;overflow:auto;">
    <table class="table" style="background:white">
    <thead>
        <tr>
            <th>Precio</th>
            <th>Cantidad</th>
            <!-- <th>Sub-total</th> -->
        </tr>
    </thead>
    <tbody id="contenido_tabla_base">

    </tbody>
    </table>
  </div>

</div>
<div class="modal" id="modal_base" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title_base"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container"  align="center">
          <div class="container" style="overflow-y:auto;height:300px">
            <table class="table table-hover">
              <thead class="thead-dark">
                <tr>
                  <th>Fecha</th>
                  <!--<th>Hora</th>-->
                  <th>Divisa</th>
                  <!--<th>Precio Base</th>-->
                  <th>Cantidad</th>
                  <th>Cajero</th>
                </tr>
              </thead>
              <tbody id="modal_body_base">

              </tbody>
            </table>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
          </div>
          <div class="col-sm-4" id="modal_footer_base">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_ingresos_egresos" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title">Ingresos Egresos Base Diaria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <select class="col-sm-2" name="" id="sel_cajero_ingresos_egresos">

          </select>
          <select class="col-sm-2" name="" id="sel_divisa_ingresos_egresos">

          </select>
          <select class="col-sm-2" name="" id="sel_precios_ingresos_egresos">

          </select>
          <input class="col-sm-2" type="text" name="" value="" onchange="format(this);" onkeyup="format(this);" id="input_ingreso_egreso">
          <button type="button" name="button" data-tipo="ingreso" data-dismiss="modal" class="col-sm-2 btn btn-info "onclick="guardar_ingreso_egreso(this);">INGRESO</button>
          <button type="button" name="button" data-tipo="egreso" data-dismiss="modal" class="col-sm-2 btn btn-info" onclick="guardar_ingreso_egreso(this);">EGRESO</button>
        </div>


        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_ver_solicitud" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title_base">Solicitud Pendientes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container"  align="center">
          <div class="container" style="overflow-y:auto;height:300px">
            <table class="table table-hover">
              <thead class="thead-dark">
                <tr>
                  <th>Fecha</th>
                  <th>Cajero</th>
                  <th>Divisa</th>
                  <th>Precio</th>
                  <th>Cantidad</th>
                  <th>Tipo</th>
                  <th>Estado</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody id="modal_body_solicitud">

              </tbody>
            </table>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
          </div>
          <div class="col-sm-4" id="modal_footer_base">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="modal" id="modal_impresion_solicitud" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_title_base">Solicitud Reporte</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="fluid-container"  align="left" style="font-size:large" id="modal_body_impresion_solicitud">
          <img src="<?php echo base_url(); ?>static/images/logo_carlos_min.png" alt="">
          <div class="fluid-container" id="modal_body_impresion_solicitud2" align="left" style="font-size:large">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary" id="generar_transaccion">Generar Transaccion</button> -->
        <div class="row justify-content-center align-items-center" >
          <div class="col-sm-4">
            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="base();">Cerrar</button>
            <button type="button" name="button" onclick="imprimir_reporte_solicitud()">Imprimir</button>
          </div>
          <div class="col-sm-4" id="modal_footer_base">

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
