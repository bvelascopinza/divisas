<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisas extends CI_Controller {

	function __construct()
	{

		parent::__construct();
		 $this->load->library('session');
		$this->load->model('Divisas_procesar');
		$this->load->model('Transacciones_procesar');
		if(!$this->session->userdata('nombre')){
			redirect('login');
		}

	}
	public function index()
	{
		$this->load->view('principal/landing-page');
		$this->load->view('caja/caja');
	}

	function informe_cajero_seguros(){
		$cajero = $this->input->post('cajero');
		$fecha = $this->input->post('fecha');
		$resultado = $this->Transacciones_procesar->consultar_seguros($cajero, $fecha);
		echo json_encode($resultado);
	}

	public function verificar_estado(){
		$cajero = $this->input->post('cajero');
		$resultado = $this->Transacciones_procesar->verificar_estado($cajero);
		echo json_encode($resultado);
	}

	public function cajero()
	{
		$this->load->view('principal/cajero_view');
		$this->load->view('caja/caja');
	}
	public function iniciar_turno(){
		$cedula = $this->input->post('cedula');
		$fecha = $this->input->post('fecha');
		$hora = $this->input->post('hora');

		$resultado = $this->Transacciones_procesar->iniciar_turno($cedula, $fecha, $hora);
		echo json_encode($resultado);

	}

	public function terminar_turno(){
		$cajero = $this->input->post('cedula');
		$resultado = $this->Transacciones_procesar->terminar_turno($cajero);
		echo json_encode($resultado);
	}
	public function nueva_divisa(){
		$cat=$this->input->post('divisa');
		$resultado=$this->Divisas_procesar->nueva_divisa($cat);
		echo json_encode($resultado);
	}
	public function inventario()
	{
		$this->load->view('principal/landing-page');
		$this->load->view('inventario/inventario');

	}
	public function categorias_divisas(){
		$categorias=$this->Divisas_procesar->categorias_divisas();
		echo json_encode($categorias);
	}
	public function categorias_seguros(){
		$categorias=$this->Divisas_procesar->categorias_seguros();
		echo json_encode($categorias);
	}
	public function seguros_objetos(){
		$opcion=$this->input->post('categoria');
		$seguros=$this->Divisas_procesar->seguros_objetos($opcion);
		echo json_encode($seguros);
	}
	public function categorias_general(){
		$categorias=$this->Divisas_procesar->categorias_general();
		echo json_encode($categorias);
	}
	public function general_objetos(){
		$opcion=$this->input->post('option');
		$general=$this->Divisas_procesar->general_objetos($opcion);
		echo json_encode($general);
	}
	public function categorias_menos(){
		$opcion=$this->input->post('option');
		$categorias=$this->Divisas_procesar->categorias_menos($opcion);
		echo json_encode($categorias);
	}
	public function cliente_unico(){
		$cedula=$this->input->post('cedula');
		$cliente=$this->Divisas_procesar->cliente_unico($cedula);
		echo json_encode($cliente);
	}
	public function inv_agregar_relacion(){
		$objeto=$this->input->post('objeto');
		$resultado=$this->Divisas_procesar->inv_agregar_relacion($objeto);
		echo json_encode($resultado);
	}
	public function inv_agregar_relacion2(){
		$objeto=$this->input->post('objeto');
		$resultado=$this->Divisas_procesar->inv_agregar_relacion2($objeto);
		echo json_encode($resultado);
	}
	public function agregar_categoria_general(){
		$cat=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->agregar_categoria_general($cat);
		echo json_encode($resultado);
	}
	public function agregar_categoria_seguros(){
		$cat=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->agregar_categoria_seguros($cat);
		echo json_encode($resultado);
	}
	public function consultar_relacion_precios(){
		$div_1=$this->input->post('div1');
		$div_2=$this->input->post('div2');
		//echo $div_1.'compra'.$div_2;
		//echo $fecha;
		$resultado=$this->Divisas_procesar->consultar_relacion_precios($div_1,$div_2);
		echo json_encode($resultado);
	}
	public function consultar_relacion(){
		$div_1=$this->input->post('div1');
		$div_2=$this->input->post('div2');
		$cedula=$this->input->post('cedula');
		$fecha=$this->input->post('fecha');
		//echo $div_1.'compra'.$div_2;
		//echo $fecha;
		$resultado=$this->Divisas_procesar->consultar_relacion($div_1,$div_2,$cedula,$fecha);
		echo json_encode($resultado);
	}
	public function consultar_relacion2(){
		$div_1=$this->input->post('div1');
		$div_2=$this->input->post('div2');
		$cedula=$this->input->post('cedula');
		$fecha=$this->input->post('fecha');
		//echo $div_1.'compra'.$div_2;
		$resultado=$this->Divisas_procesar->consultar_relacion2($div_1,$div_2,$cedula,$fecha);
		echo json_encode($resultado);
	}
	public function consultar_relacion_venta(){
		$div_1=$this->input->post('div1');
		$div_2=$this->input->post('div2');
		echo $div_1.'venta'.$div_2;
		$resultado=$this->Divisas_procesar->consultar_relacion($div_1,$div_2);
		echo json_encode($resultado);
	}
	public function nuevo_cliente(){
		$cliente=$this->input->post('objeto');
		print_r($cliente);
		$resultado=$this->Divisas_procesar->nuevo_cliente($cliente);
		echo json_encode($resultado);
	}
	public function actualizar_relacion(){
		$origen=$this->input->post('original');
		//$compra=$this->input->post('compra');
		$venta=$this->input->post('venta');
		$base=$this->input->post('base');
		$cant_base=$this->input->post('cant_base');
		$cant_base_ant=$this->input->post('cant_base_ant');
		//$resultado=$this->Divisas_procesar->actualizar_relacion($origen,$compra,$venta,$base,$cant_base,$cant_base_ant);
		$resultado=$this->Divisas_procesar->actualizar_relacion($origen,$venta,$base,$cant_base,$cant_base_ant);
		echo json_encode($resultado);
	}
	public function agregar_objeto_divisas(){
		$elements=$this->input->post('elementos');
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->nuevo_objeto($elements,$categoria);
		echo json_encode($resultado);
	}
	public function agregar_objeto_general(){
		$valores=$this->input->post('valores');
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->nuevo_objeto_general($valores,$categoria);
		echo json_encode($resultado);
	}
	public function agregar_objeto_seguros(){
		$valores=$this->input->post('valores');
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->nuevo_objeto_seguros($valores,$categoria);
		echo json_encode($resultado);
	}
	public function obj_general(){
		$categoria=$this->input->post('categoria');
		$categorias=$this->Divisas_procesar->obj_general($categoria);
		echo json_encode($categorias);
	}
	public function modificar_obj_general(){
		$original=$this->input->post('original');
		$valores=$this->input->post('valores');
		//print_r($original['id_objeto_gen']);
		//print_r($valores[0]);
		$resultado=$this->Divisas_procesar->modificar_obj_general($original,$valores);
		echo json_encode($resultado);
	}
	public function modificar_obj_seguros(){
		$original=$this->input->post('original');
		$valores=$this->input->post('valores');
		$resultado=$this->Divisas_procesar->modificar_obj_seguros($original,$valores);
		echo json_encode($resultado);
	}
	public function consultar_objetos_divisas(){
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->consultar_objetos_divisas($categoria);
		echo json_encode($resultado);
	}
	public function base(){
		$this->load->view('principal/landing-page');
		$this->load->view('base/base_diaria_view');
	}
	public function cajeros(){
		$resultado=$this->Divisas_procesar->cajeros();
		echo json_encode($resultado);
	}
	public function insertar_cajero(){
		$cajero=$this->input->post('cajero');
		$resultado=$this->Divisas_procesar->insertar_cajero($cajero);
		echo json_encode($resultado);
	}
	public function actualizar_objetos_divisas(){
		$categoria=$this->input->post('categoria');
		$elements=$this->input->post('elements');
		$total=$this->input->post('total');
		$cajero=$this->input->post('cajero');
		$resultado=$this->Divisas_procesar->actualizar_objetos_divisas($categoria,$elements,$total,$cajero);
		echo json_encode($resultado);
	}
	public function actualizar_objetos_divisas2(){
		$elements=$this->input->post('objetos');
		$resultado=$this->Divisas_procesar->actualizar_objetos_divisas2($elements);
		echo json_encode($resultado);
	}
	public function eliminar_objeto_divisas(){
		$objeto=$this->input->post('objeto');
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->eliminar_objeto_divisas($objeto,$categoria);
		echo json_encode($resultado);
	}
	public function eliminar_objeto_seguros(){
		$objeto=$this->input->post('objeto');
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->eliminar_objeto_seguros($objeto,$categoria);
		echo json_encode($resultado);
	}
	public function eliminar_objeto_general(){
		$objeto=$this->input->post('objeto');
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->eliminar_objeto_general($objeto,$categoria);
		echo json_encode($resultado);
	}
	public function eliminar_categoria_seguros(){
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->eliminar_categoria_seguros($categoria);
		echo json_encode($resultado);
	}
	public function eliminar_categoria_general(){
		$categoria=$this->input->post('categoria');
		$resultado=$this->Divisas_procesar->eliminar_categoria_general($categoria);
		echo json_encode($resultado);
	}
	public function ver_bases_diarias(){
		$fecha=$this->input->post('fecha');
		$resultado=$this->Divisas_procesar->ver_bases_diarias($fecha);
		echo json_encode($resultado);
	}
	public function notificaciones(){
		//$fecha=$this->input->post('fecha');
		//$hora=$this->input->post('hora');
		$resultado=$this->Divisas_procesar->notificaciones();
		echo json_encode($resultado);
	}
	public function divisa_unica(){
		$categoria=$this->input->post('valor');
		$resultado=$this->Divisas_procesar->divisa_unica($categoria);
		echo json_encode($resultado);
	}
	public function informe_cajero(){
		$cajero=$this->input->post('cajero');
		$fecha=$this->input->post('fecha');
		$resultado=$this->Divisas_procesar->informe_cajero($cajero,$fecha);
		echo json_encode($resultado);
	} 
	public function insertar_ingresos_egresos(){
		$obj=$this->input->post('obj');
		$resultado=$this->Divisas_procesar->insertar_ingresos_egresos($obj);
		echo json_encode($resultado);
	}
	public function consultar_transacciones_divisas(){
    $inicio=$this->input->post('inicio');
    $fin=$this->input->post('fin');
    $res=$this->Divisas_procesar->consultar_transaciones_divisas($inicio,$fin);
		echo json_encode($res);
  }
	public function consultar_transacciones_divisas2(){
    $fecha=$this->input->post('fecha');
    $cedula=$this->input->post('cedula');
    $res=$this->Divisas_procesar->consultar_transaciones_divisas2($fecha,$cedula);
		echo json_encode($res);
  }
	function eliminar_transaccion_divisa(){
		$orden=$this->input->post('orden');
		$objeto=$this->input->post('objeto');
		$res=$this->Divisas_procesar->eliminar_transaccion_divisa($orden,$objeto);
		echo json_encode($res);
	}
	function eliminar_cajero(){
		$orden=$this->input->post('cedula');
		$res=$this->Divisas_procesar->eliminar_cajero($orden);
		echo json_encode($res);
	}
	function insertar_solicitud_cajero(){
		$obj=$this->input->post('obj');
		$res=$this->Divisas_procesar->insertar_solicitud_cajero($obj);
		echo json_encode($res);
	}
	function solicitud_pendiente(){
		$res=$this->Divisas_procesar->solicitud_pendiente();
		echo json_encode($res);
	}
	function cambiar_estado_solicitud(){
		$obj=$this->input->post('objeto');
		$res=$this->Divisas_procesar->cambiar_estado_solicitud($obj);
		echo json_encode($res);
	}
	function base_diaria_entrega(){
 	 $objetos=$this->input->post('objs');
 	 $res=$this->Divisas_procesar->base_diaria_entrega($objetos);
 	 echo json_encode($res);
  }
	public function pesos_maximos(){
		$div_1=$this->input->post('div1');
		$div_2=$this->input->post('div2');
		$cedula=$this->input->post('cedula');
		$fecha=$this->input->post('fecha');
		//echo $div_1.'compra'.$div_2;
		//echo $fecha;
		$resultado=$this->Divisas_procesar->pesos_maximos($div_1,$div_2,$cedula,$fecha);
		echo json_encode($resultado);
	}
	public function divisa_maximos(){
		$div_1=$this->input->post('div1');
		$div_2=$this->input->post('div2');
		$cedula=$this->input->post('cedula');
		$fecha=$this->input->post('fecha');
		//echo $div_1.'compra'.$div_2;
		//echo $fecha;
		$resultado=$this->Divisas_procesar->divisa_maximos($div_1,$div_2,$cedula,$fecha);
		echo json_encode($resultado);
	}
	public function cajeros_general_transacciones(){
		$obj=$this->input->post('obj');
		$resultado=$this->Divisas_procesar->cajeros_general_transacciones($obj);
		echo json_encode($resultado);
	}
	public function cajeros_seguros_transacciones(){
		$obj=$this->input->post('obj');
		$resultado=$this->Divisas_procesar->cajeros_seguros_transacciones($obj);
		echo json_encode($resultado);
	}
	public function transacion_inventario_global(){
		$objs=$this->input->post('objs');
		$resultado=$this->Divisas_procesar->transacion_inventario_global($objs);
		echo json_encode($resultado);
	}
	public function reiniciar_base_cajero(){
		$cajero=$this->input->post('cajero');
		$resultado=$this->Divisas_procesar->reiniciar_base_cajero($cajero);
		echo json_encode($resultado);
	}
}
