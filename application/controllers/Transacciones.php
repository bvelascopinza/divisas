<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transacciones extends CI_Controller {

	function __construct()
	{

		parent::__construct();
     $this->load->library('session');
		$this->load->model('Transacciones_procesar');

	}
  public function consultar_transacciones_divisas(){
    $inicio=$this->input->post('inicio');
    $fin=$this->input->post('fin');
    $res=$this->Transacciones_procesar->consultar_transaciones_divisas($inicio,$fin);
		echo json_encode($res);
  }
  public function consultar_transacciones_divisas_dia(){
    $hoy=$this->input->post('hoy');
	$tipo=$this->input->post('tipo');     
    $res=$this->Transacciones_procesar->consultar_transacciones_divisas_dia($hoy,$tipo);
		echo json_encode($res);
  }
  public function transaciones_divisas(){
    $objeto=$this->input->post('obj');
		$opcion=$this->input->post('opcion');
    $res=$this->Transacciones_procesar->insertar_transaccion_divisas($objeto,$opcion);
		echo json_encode($res);
  }
  public function transaciones_seguros(){
    $objeto=$this->input->post('obj');
		//print_r($objeto);
    $res=$this->Transacciones_procesar->insertar_transaccion_seguros($objeto);
		echo json_encode($res);
  }
	public function consultar_transacciones_seguros(){
    $inicio=$this->input->post('inicio');
    $fin=$this->input->post('fin');
    $res=$this->Transacciones_procesar->consultar_transaciones_seguros($inicio,$fin);
		echo json_encode($res);
  }
  public function transaciones_general(){
    $objeto=$this->input->post('obj');
		$total=$this->input->post('total');
    $res=$this->Transacciones_procesar->insertar_transaccion_general($objeto,$total);
		echo json_encode($res);
  }
	public function consultar_transacciones_general(){
    $inicio=$this->input->post('inicio');
    $fin=$this->input->post('fin');
    $res=$this->Transacciones_procesar->consultar_transaciones_general($inicio,$fin);
		echo json_encode($res);
  }
	public function contabilidad()
	{
		$this->load->view('reportes/contabilidad_view');

	}
	public function caja_general(){
		$res=$this->Transacciones_procesar->caja_general();
		echo json_encode($res);
	}
	public function consultar_conta(){
    $inicio=$this->input->post('inicio');
    $fin=$this->input->post('fin');
    $res=$this->Transacciones_procesar->consultar_conta($inicio,$fin);
		echo json_encode($res);
  }
	function informe_global_divisas(){
		//$fecha=$this->input->post('fecha');
		//$fecha2=$this->input->post('fecha2');
    //$res=$this->Transacciones_procesar->informe_global_divisas($fecha,$fecha2);
		$res=$this->Transacciones_procesar->informe_global_divisas();
		echo json_encode($res);
	}
	function informe_global_divisas_pesos(){
		$res=$this->Transacciones_procesar->informe_global_divisas_pesos();
		echo json_encode($res);
	}
	function cargar_bancos(){
		$res=$this->Transacciones_procesar->cargar_bancos();
		echo json_encode($res);
	}
	function actualizar_bancos(){
		$valor=$this->input->post('valor');
		$res=$this->Transacciones_procesar->actualizar_bancos($valor);
		echo json_encode($res);
	}
	function eliminar_transaccion_divisa(){
		$orden=$this->input->post('orden');
		$objeto=$this->input->post('objeto');
		$res=$this->Transacciones_procesar->eliminar_transaccion_divisa($orden,$objeto);
	}
	public function seleccion_contabilidad(){
		$tipo=$this->input->post('tipo');
		$fecha_inicio=$this->input->post('fecha_inicio');
		$fecha_fin=$this->input->post('fecha_fin');
  	$res=$this->Transacciones_procesar->seleccion_contabilidad($tipo,$fecha_inicio,$fecha_fin);
  	echo json_encode($res);
	}
	public function revertir_venta_general(){
		$obj=$this->input->post('obj');
  	$res=$this->Transacciones_procesar->revertir_venta_general($obj);
  	echo json_encode($res);
	}
	public function cajeros_general_transacciones(){
		$obj=$this->input->post('obj');
		$resultado=$this->Divisas_procesar->cajeros_general_transacciones($obj);
		echo json_encode($resultado);
	}
	public function eliminar_transaccion_seguros(){
		$obj=$this->input->post('objeto');
  	$res=$this->Transacciones_procesar->eliminar_transaccion_seguros($obj);
  	echo json_encode($res);
	}
}
