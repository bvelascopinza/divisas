<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestamos extends CI_Controller {

	function __construct()
	{

		parent::__construct();
		 $this->load->library('session');
		$this->load->model('Prestamos_procesar');
		if(!$this->session->userdata('nombre')){
			redirect('login');
		}

	}
	public function cargar_vista_prestamos(){
    $this->load->view('principal/landing-page');
    $this->load->view('prestamos/prestamos_view');
  }
  public function insertar_prestamo(){
    $objeto=$this->input->post('objeto');
    $resultado=$this->Prestamos_procesar->insertar_prestamo($objeto);
		echo json_encode($resultado);
  }
  public function actualizar_prestamo(){
    $objeto=$this->input->post('objeto');
    $resultado=$this->Prestamos_procesar->actualizar_prestamo($objeto);
		echo json_encode($resultado);
  }
  public function categorias_divisas(){
		$categorias=$this->Prestamos_procesar->categorias_divisas();
		echo json_encode($categorias);
	}
  public function cargar_prestamos(){
    $prestamos=$this->Prestamos_procesar->cargar_prestamos();
		echo json_encode($prestamos);
  }
	public function actualizar_abono(){
    $objeto=$this->input->post('objeto');
    $resultado=$this->Prestamos_procesar->actualizar_abono($objeto);
		echo json_encode($resultado);
  }
}
