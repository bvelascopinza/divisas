<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguros extends CI_Controller {

	function __construct()
	{

    parent::__construct();
    $this->load->model('Transacciones_procesar');
		if(!$this->session->userdata('nombre')){
			redirect('login');
    }
    
    function informe_cajero_seguros(){
      $cajero = $this->input->post('cajero');
      $fecha = $this->input->post('fecha');
      $resultado = $this->Transacciones_procesar->consultar_seguros($cajero, $fecha);
      echo json_encode($resultado);
    }

  }
}
