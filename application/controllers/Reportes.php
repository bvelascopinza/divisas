<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	function __construct()
	{

		parent::__construct();
		 $this->load->library('session');
		//$this->load->model('Reportes_procesar');

		if(!$this->session->userdata('nombre')){
			redirect('login');
		}

	}
  public function index()
	{
		$this->load->view('reportes/reportes_view');
	}
	public function reportes_cajero_vista()
	{
		$this->load->view('reportes/reportes_view_cajero');
	}
}
